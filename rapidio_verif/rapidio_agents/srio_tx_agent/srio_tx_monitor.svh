//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

// -----------------------------------------------------------------------------------------------
// srio_tx_monitor
// -----------------------------------------------------------------------------------------------
class srio_tx_monitor extends uvm_monitor;

	// uvm factory registation
	`uvm_component_utils_begin(srio_tx_monitor)
	`uvm_component_utils_end

  // data members

	// component members
  virtual srio_tx_if state_if;
  uvm_analysis_port #(srio_tx_seq_item) srio_tx_port; 
	// uvm methods
	extern function new(string name="srio_tx_monitor", uvm_component parent);
	extern virtual function void build_phase(uvm_phase phase);
	extern virtual function void connect_phase(uvm_phase phase);
	extern virtual task run_phase(uvm_phase phase);
endclass : srio_tx_monitor

// -----------------------------------------------------------------------------------------------
// new
// -----------------------------------------------------------------------------------------------
function srio_tx_monitor::new(string name="srio_tx_monitor", uvm_component parent);
	super.new(name,parent);
endfunction : new

// -----------------------------------------------------------------------------------------------
// build
// -----------------------------------------------------------------------------------------------
function void srio_tx_monitor::build_phase(uvm_phase phase);
	super.build_phase(phase);
  srio_tx_port = new("srio_tx_port", this);
  if (!uvm_config_db #(virtual srio_tx_if)::get(uvm_root::get(),"*", "state_if",state_if)) begin 
    `uvm_fatal(get_full_name(), $sformatf("Failed to get inst ")) 
  end 

endfunction : build_phase

// -----------------------------------------------------------------------------------------------
// connect
// -----------------------------------------------------------------------------------------------
function void srio_tx_monitor::connect_phase(uvm_phase phase);
  super.connect_phase(phase);
endfunction : connect_phase

// -----------------------------------------------------------------------------------------------
// run
// -----------------------------------------------------------------------------------------------
task srio_tx_monitor::run_phase(uvm_phase phase);
  
  srio_tx_seq_item state_pkt;

  forever @(posedge state_if.clk) begin
    //`uvm_info("MYINFO1", $sformatf("instr retire: %0h",state_if.instr_retire), UVM_LOW)
    //`uvm_info("MYINFO1", $sformatf("x5: %0h pc:%0h memdata:%h%h ",state_if.x5, state_if.pc, state_if.data_msb,state_if.data_lsb), UVM_LOW)
    if (state_if.instr_retire == 'b1) begin
      state_pkt = srio_tx_seq_item::type_id::create("state_pkt",this); 
      state_pkt.int_regs.push_back(state_if.x0);
      state_pkt.int_regs.push_back(state_if.x1);
      state_pkt.int_regs.push_back(state_if.x2);
      state_pkt.int_regs.push_back(state_if.x3);
      state_pkt.int_regs.push_back(state_if.x4);
      state_pkt.int_regs.push_back(state_if.x5);
      state_pkt.int_regs.push_back(state_if.x6);
      state_pkt.int_regs.push_back(state_if.x7);
      state_pkt.int_regs.push_back(state_if.x8);
      state_pkt.int_regs.push_back(state_if.x9);
      state_pkt.int_regs.push_back(state_if.x10);
      state_pkt.int_regs.push_back(state_if.x11);
      state_pkt.int_regs.push_back(state_if.x12);
      state_pkt.int_regs.push_back(state_if.x13);
      state_pkt.int_regs.push_back(state_if.x14);
      state_pkt.int_regs.push_back(state_if.x15);
      state_pkt.int_regs.push_back(state_if.x16);
      state_pkt.int_regs.push_back(state_if.x17);
      state_pkt.int_regs.push_back(state_if.x18);
      state_pkt.int_regs.push_back(state_if.x19);
      state_pkt.int_regs.push_back(state_if.x20);
      state_pkt.int_regs.push_back(state_if.x21);
      state_pkt.int_regs.push_back(state_if.x22);
      state_pkt.int_regs.push_back(state_if.x23);
      state_pkt.int_regs.push_back(state_if.x24);
      state_pkt.int_regs.push_back(state_if.x25);
      state_pkt.int_regs.push_back(state_if.x26);
      state_pkt.int_regs.push_back(state_if.x27);
      state_pkt.int_regs.push_back(state_if.x28);
      state_pkt.int_regs.push_back(state_if.x29);
      state_pkt.int_regs.push_back(state_if.x30);
      state_pkt.int_regs.push_back(state_if.x31);
      //state_pkt.csr[`CSR_MSTATUS] = state_if.csr_mstatus;
      //state_pkt.csr[`CSR_MEDELEG] = state_if.csr_medeleg;
      //state_pkt.csr[`CSR_MIDELEG] = state_if.csr_mideleg;
      //state_pkt.csr[`CSR_MIE] = state_if.csr_mie;
      //state_pkt.csr[`CSR_MTVEC] = state_if.csr_mtvec;
      //state_pkt.csr[`CSR_MEPC] = state_if.csr_mepc;
      //state_pkt.csr[`CSR_MCAUSE] = state_if.csr_mcause;
      //state_pkt.csr[`CSR_MTVAL] = state_if.csr_mtval;
      //state_pkt.csr[`CSR_MIP] = state_if.csr_mip;
      state_pkt.csr[`CSR_CYCLE      ] = state_if.csr_cycle - 209;
      state_pkt.csr[`CSR_TIME       ] = state_if.csr_time;
      state_pkt.csr[`CSR_INSTRET    ] = state_if.csr_instret;
      state_pkt.csr[`CSR_SSTATUS    ] = state_if.csr_sstatus;
      state_pkt.csr[`CSR_SIE        ] = state_if.csr_sie;
      state_pkt.csr[`CSR_STVEC      ] = state_if.csr_stvec;
      state_pkt.csr[`CSR_SCOUNTEREN ] = state_if.csr_scounteren;
      state_pkt.csr[`CSR_SSCRATCH   ] = state_if.csr_sscratch;
      state_pkt.csr[`CSR_SEPC       ] = state_if.csr_sepc;
      state_pkt.csr[`CSR_SCAUSE     ] = state_if.csr_scause;
      state_pkt.csr[`CSR_STVAL      ] = state_if.csr_stval;
      state_pkt.csr[`CSR_SIP        ] = state_if.csr_sip;
      state_pkt.csr[`CSR_SATP       ] = state_if.csr_satp;
      state_pkt.csr[`CSR_MSTATUS    ] = state_if.csr_mstatus;
      state_pkt.csr[`CSR_MISA       ] = state_if.csr_misa;
      state_pkt.csr[`CSR_MEDELEG    ] = state_if.csr_medeleg;
      state_pkt.csr[`CSR_MIDELEG    ] = state_if.csr_mideleg;
      state_pkt.csr[`CSR_MIE        ] = state_if.csr_mie;
      state_pkt.csr[`CSR_MTVEC      ] = state_if.csr_mtvec;
      state_pkt.csr[`CSR_MCOUNTEREN ] = state_if.csr_mcounteren;
      state_pkt.csr[`CSR_MSCRATCH   ] = state_if.csr_mscratch;
      state_pkt.csr[`CSR_MEPC       ] = state_if.csr_mepc;
      state_pkt.csr[`CSR_MCAUSE     ] = state_if.csr_mcause;
      state_pkt.csr[`CSR_MTVAL      ] = state_if.csr_mtval;
      state_pkt.csr[`CSR_MIP        ] = state_if.csr_mip;
      state_pkt.csr[`CSR_TSELECT    ] = state_if.csr_tselect;
      state_pkt.csr[`CSR_TDATA1     ] = state_if.csr_tdata1;
      state_pkt.csr[`CSR_TDATA2     ] = state_if.csr_tdata2;
      state_pkt.csr[`CSR_TDATA3     ] = state_if.csr_tdata3;
      state_pkt.csr[`CSR_DCSR       ] = state_if.csr_dcsr;
      state_pkt.csr[`CSR_DPC        ] = state_if.csr_dpc;
      state_pkt.csr[`CSR_DSCRATCH   ] = state_if.csr_dscratch;
      state_pkt.csr[`CSR_MCYCLE     ] = state_if.csr_mcycle;
      state_pkt.csr[`CSR_MINSTRET   ] = state_if.csr_minstret;
      state_pkt.csr[`CSR_MVENDORID  ] = state_if.csr_mvendorid;
      state_pkt.csr[`CSR_MARCHID    ] = state_if.csr_marchid;
      state_pkt.csr[`CSR_MIMPID     ] = state_if.csr_mimpid;
      state_pkt.csr[`CSR_MHARTID    ] = state_if.csr_mhartid;
      state_pkt.csr[`CSR_CYCLEH     ] = state_if.csr_cycleh;
      state_pkt.csr[`CSR_TIMEH      ] = state_if.csr_timeh;
      state_pkt.csr[`CSR_INSTRETH   ] = state_if.csr_instreth;
      state_pkt.csr[`CSR_MCYCLEH    ] = state_if.csr_mcycleh;
      state_pkt.csr[`CSR_MINSTRETH  ] = state_if.csr_minstreth;
//      `uvm_info("MYINFO1", $sformatf("x5: %0h pc:%0h mem:%0h ",state_if.x5, state_if.pc, state_if.data_msb), UVM_LOW)
    state_pkt.pc = state_if.pc;
    srio_tx_port.write(state_pkt);
    //  `uvm_info("SHAKTI", $sformatf("-----------------"), UVM_LOW)
      //state_pkt.print_srio_tx();
    end
  end
endtask : run_phase

