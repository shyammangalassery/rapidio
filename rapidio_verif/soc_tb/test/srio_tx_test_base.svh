//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

// -----------------------------------------------------------------------------------------------
// srio_tx_test_base
// -----------------------------------------------------------------------------------------------
class srio_tx_test_base extends uvm_test;

	// uvm factory registation
	`uvm_component_utils_begin(srio_tx_test_base)
	`uvm_component_utils_end
	
  // data members

	// component members
  soc_env m_env;
  soc_env_config m_env_cfg;
  srio_tx_agent_config m_state_cfg;
  virtual srio_tx_if state_if;
	// uvm methods
	extern function new(string name="srio_tx_test_base",uvm_component parent=null);
	extern function void build_phase(uvm_phase phase);
	extern task run_phase(uvm_phase phase);
  extern function void start_of_simulation();
endclass : srio_tx_test_base

// -----------------------------------------------------------------------------------------------
// new
// -----------------------------------------------------------------------------------------------
function srio_tx_test_base::new(string name="srio_tx_test_base", uvm_component parent=null);
	super.new(name, parent);
endfunction : new

function void srio_tx_test_base::start_of_simulation();
//  srio_tx_report_server report_server = new;
//  uvm_report_server::set_server( report_server );
endfunction: start_of_simulation

// -----------------------------------------------------------------------------------------------
// build
// -----------------------------------------------------------------------------------------------
function void srio_tx_test_base::build_phase(uvm_phase phase);
	//super.build(phase);
  m_env_cfg = soc_env_config::type_id::create("m_env_cfg");
  uvm_config_db #(soc_env_config)::set(this,"*", "soc_env_config", m_env_cfg);
  uvm_config_db #(srio_tx_agent_config)::set(this,"*", "srio_tx_state_agent_config", m_state_cfg);
  if (!uvm_config_db #(virtual srio_tx_if)::get(uvm_root::get(),"*", "state_if",state_if)) begin 
    `uvm_fatal(get_full_name(), $sformatf("Failed to get inst ")) 
  end 

//  uvm_config_db #(srio_tx_if)::set(this, "*", "srio_tx_state_if",state_if);
  //m_state_cfg = srio_tx_agent_config::type_id::create("m_state_cfg");
  
//  if(!uvm_config_db #(srio_tx_if)::get(this, "", "srio_tx_state_if", 
//                                        m_env_cfg.state_if)) 
//    `uvm_error("Config Error", 
//               "uvm_config_db #(bfm_type)::get cannot find resource APB3_IF" )
//
//  `get_intf_object(srio_tx_if, m_state_cfg.state_if)
//  m_env_cfg.m_state_cfg = m_state_cfg;
  m_env = soc_env::type_id::create("m_env",this);
endfunction : build_phase

// -----------------------------------------------------------------------------------------------
// connect
// -----------------------------------------------------------------------------------------------
//function void srio_tx_test_base::connect_phase(uvm_phase phase);
//endfunction : connect_phase
task srio_tx_test_base::run_phase(uvm_phase phase);
  phase.raise_objection(this, "Starting sequence"); 
  state_if.rst_n = 0;
  #10000;
  state_if.rst_n = 1;
  #60000000;
  phase.drop_objection(this, "Completed sequence"); 
endtask
