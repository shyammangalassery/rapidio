/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: M Gopinathan
Email id: gopinathan18@gmail.com
Details: This file contains all Data types, Structs and Interfaces used in any of the modules

--------------------------------------------------------------------------------------------------
*/
package srio_txrx_dtypes;
import DefaultValue ::*;

//`define ADDR	34    // 34-, 50-, and 66-bit addresses to be specified in a packet
//`define DATA  64    // Payload data

////////////////////////////////////////////////////////////////////////////////
/// Data Structures
////////////////////////////////////////////////////////////////////////////////

  typedef Bit#(50)	Addr; // Physical address 34-, 50-, and 66-bit addresses to be specified in a packet
  typedef Bit#(2)   Xmsb; // Extended address most significant bits
  typedef Bit#(128)	Data; // Payload data
  typedef Bit#(4)		Type; // Format Type and Transaction Type 
  typedef Bit#(8)		TranId; // Transaction ID
  typedef Bit#(32)	DestId; // 32 bits specified in spec 3.0, 16 bits for Large System, 8 bits (right justified) for Small System
  typedef Bit#(32)	SourceId; // 32 bits specified in spec 3.0, 16 bits for Large System, 8 bits (right justified) for Small System
  typedef Bit#(2)		Prio; // Priority Field used in PHY
  typedef Bit#(2)		TT; // 2'b00 for Small System, 2'b01 for Large System and 2'b10 for Dev32 system 
  typedef Bit#(8) 	ByteEn; //  Packet Byte Count. Indicates the number of valid bytes in the packet. Maximum value allowed is 256 bytes.
  typedef Bit#(9)		ByteCount; // Frame Byte Count. Indicates the number of valid bytes for the current packet beat.
   
 typedef Bit#(4)		Size; 
  typedef Bit#(4)		RdSize; //  Data size for read transactions, used in conjunction with the word pointer (wdptr)
  typedef Bit#(4)		WrSize; //  Write data size for sub-double-word transactions, used in conjunction with the word pointer (wdptr) 
  typedef Bit#(1)		WdPointer; //  Word pointer, used in conjunction with the data size (rdsize and wrsize) fields
  
  typedef Bit#(16)	DoorBell; // Doorbell Info. Optional port carries doorbell information
  typedef Bit#(4)		MsgLen; // Total number of packets comprising the message.
  typedef Bit#(4) 	MsgSeg; // Defines the segment of message supplied by this packet
  typedef Bit#(21)	Offset; // ndicates the double-word offset into theCAR/CSR register block for reads and writes  
  typedef Bit#(4)		Status; // Indicates packet status, if the transaction was successfully completed or if there was an error.
  typedef Bit#(2)		Mletter; //  indicates the desired slot within a mailbox

////////////////////////////////////////////////////////////////////////////////
/// Struct Definition for interface
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////LOGICAL LAYER ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////// INITIATOR REQUEST /////////////

/// Initiator request control signals
/*typedef struct {Bool srio_tx_ireq_sof_n;
  Bool srio_tx_ireq_eof_n;
  Bool srio_tx_ireq_vld_n;
  Bool srio_tx_ireq_dsc_n;
} Ireq_ctrl deriving (Bits, Eq, Bounded);

instance DefaultValue#(Ireq_ctrl);
  defaultValue = Ireq_ctrl {srio_tx_ireq_sof_n:False, srio_tx_ireq_eof_n:False,
  srio_tx_ireq_vld_n:False, srio_tx_ireq_dsc_n:False};
endinstance*/

/// Initiator request data signals
typedef struct {Data srio_tx_ireq_data;
  Bool srio_tx_ireq_crf;
  Prio srio_tx_ireq_prio;
  Type srio_tx_ireq_ftype; 
  TranId srio_tx_ireq_tid;
  Type srio_tx_ireq_ttype;
  Addr srio_tx_ireq_addr;
  Bit#(8) srio_tx_ireq_hopcount;
  ByteCount srio_tx_ireq_byte_count;
  ByteEn srio_tx_ireq_byte_en_n;
  Bool srio_tx_ireq_local;
} Ireq_data deriving (Bits, Eq, Bounded);

instance DefaultValue#(Ireq_data);
  defaultValue = Ireq_data {srio_tx_ireq_data:0, srio_tx_ireq_crf:False, srio_tx_ireq_prio:0,
  srio_tx_ireq_ftype:0, srio_tx_ireq_tid:0, srio_tx_ireq_ttype:0, srio_tx_ireq_addr:0,
  srio_tx_ireq_hopcount:0, srio_tx_ireq_byte_count:0, srio_tx_ireq_byte_en_n:0,
  srio_tx_ireq_local:False};
endinstance

/// Initiator request message signals
typedef struct {DoorBell srio_tx_ireq_db_info;
  MsgLen srio_tx_ireq_msg_len;
  MsgSeg srio_tx_ireq_msg_seg;
  Bit#(6) srio_tx_ireq_mbox;
  Mletter srio_tx_ireq_letter;
} Ireq_msg deriving (Bits, Eq, Bounded);

instance DefaultValue#(Ireq_msg);
  defaultValue = Ireq_msg {srio_tx_ireq_db_info:0, 
  srio_tx_ireq_msg_len:0, srio_tx_ireq_msg_seg:0, srio_tx_ireq_mbox:0, srio_tx_ireq_letter:0};
endinstance

// Initiator request packet
typedef struct {Ireq_data ireqdata;
  Ireq_msg ireqmsg;
} Ireq_pkt deriving (Bits, Eq, Bounded);

instance DefaultValue#(Ireq_pkt);
  defaultValue = Ireq_pkt {ireqdata:defaultValue, ireqmsg:defaultValue};
endinstance

////////////////// INITIATOR RESPONSE /////////////////////

//Initiator response signals


/// Initiator response data signals
typedef struct {Data srio_rx_iresp_data;
  Bool srio_rx_iresp_crf;
  Prio srio_rx_iresp_prio;
  Type srio_rx_iresp_ftype; 
  Type srio_rx_iresp_ttype;
  Status srio_rx_iresp_status;
  TranId srio_rx_iresp_tid;
  Bool srio_rx_iresp_local;
} Iresp_data deriving (Bits, Eq, Bounded);

instance DefaultValue#(Iresp_data);
  defaultValue = Iresp_data {srio_rx_iresp_data:0, srio_rx_iresp_crf:False, srio_rx_iresp_prio:0,
  srio_rx_iresp_ftype:0, srio_rx_iresp_tid:0, srio_rx_iresp_ttype:0,
  srio_rx_iresp_status:0, srio_rx_iresp_local:False};
endinstance

/// Initiator response message signals
typedef struct {MsgSeg srio_rx_iresp_msg_seg;
  Bit#(6) srio_rx_iresp_mbox;
  Mletter srio_rx_iresp_letter;
} Iresp_msg deriving (Bits, Eq, Bounded);

instance DefaultValue#(Iresp_msg);
  defaultValue = Iresp_msg {srio_rx_iresp_msg_seg:0, srio_rx_iresp_mbox:0, srio_rx_iresp_letter:0};
endinstance
// Initiator response packet
typedef struct {Iresp_data irespdata;
  Iresp_msg irespmsg;
} Iresp_pkt deriving (Bits, Eq, Bounded);

instance DefaultValue#(Iresp_pkt);
  defaultValue = Iresp_pkt {irespdata:defaultValue, irespmsg:defaultValue};
endinstance

//////////////////////  TARGET REQUEST  //////////////////


/// Target request control signals
typedef struct {Bool srio_rx_treq_sof_n;
  Bool srio_rx_treq_eof_n;
  Bool srio_rx_treq_vld_n;
} Treq_ctrl deriving (Bits, Eq, Bounded);

instance DefaultValue#(Treq_ctrl);
  defaultValue = Treq_ctrl {srio_rx_treq_sof_n:False, srio_rx_treq_eof_n:False,
  srio_rx_treq_vld_n:False};
endinstance

/// Target request data signals
typedef struct {Data srio_rx_treq_data;
  Bool srio_rx_treq_crf;
  Prio srio_rx_treq_prio;
  Type srio_rx_treq_ftype; 
  TranId srio_rx_treq_tid;
  Type srio_rx_treq_ttype;
  Addr srio_rx_treq_addr;
  ByteCount srio_rx_treq_byte_count;
  ByteEn srio_rx_treq_byte_en_n;
} Treq_data deriving (Bits, Eq, Bounded);

instance DefaultValue#(Treq_data);
  defaultValue = Treq_data {srio_rx_treq_data:0, srio_rx_treq_crf:False, srio_rx_treq_prio:0,
  srio_rx_treq_ftype:0, srio_rx_treq_tid:0, srio_rx_treq_ttype:0, srio_rx_treq_addr:0,
  srio_rx_treq_byte_count:0, srio_rx_treq_byte_en_n:0};
endinstance

/// Target request message signals
typedef struct {DoorBell srio_rx_treq_db_info;
  MsgLen srio_rx_treq_msg_len;
  MsgSeg srio_rx_treq_msg_seg;
  Bit#(6) srio_rx_treq_mbox;
  Mletter srio_rx_treq_letter;
} Treq_msg deriving (Bits, Eq, Bounded);

instance DefaultValue#(Treq_msg);
  defaultValue = Treq_msg {srio_rx_treq_db_info:0, 
  srio_rx_treq_msg_len:0, srio_rx_treq_msg_seg:0, srio_rx_treq_mbox:0, srio_rx_treq_letter:0};
endinstance
// Target request packet
typedef struct {Treq_ctrl treqcntrl;
  Treq_data treqdata;
  Treq_msg treqmsg;
} Treq_pkt deriving (Bits, Eq, Bounded);

instance DefaultValue#(Treq_pkt);
  defaultValue = Treq_pkt {treqcntrl:defaultValue, treqdata:defaultValue, treqmsg:defaultValue};
endinstance


////////////////////////// TARGET RESPONSE ///////////////////////////////


//Target response signals

typedef struct {Bool srio_tx_tresp_sof_n;
  Bool srio_tx_tresp_eof_n;
  Bool srio_tx_tresp_vld_n;
  Bool srio_tx_tresp_dsc_n;
} Tresp_ctrl deriving (Bits, Eq, Bounded);

instance DefaultValue#(Tresp_ctrl);
  defaultValue = Tresp_ctrl {srio_tx_tresp_sof_n:False, srio_tx_tresp_eof_n:False,
  srio_tx_tresp_vld_n:False, srio_tx_tresp_dsc_n:False};
endinstance

/// Target response data signals
typedef struct {Data srio_tx_tresp_data;
  Bool srio_tx_tresp_crf;
  Prio srio_tx_tresp_prio;
  Type srio_tx_tresp_ftype; 
  Type srio_tx_tresp_ttype;
  Status srio_tx_tresp_status;
  TranId srio_tx_tresp_tid;
  Bool srio_tx_tresp_no_data;
} Tresp_data deriving (Bits, Eq, Bounded);

instance DefaultValue#(Tresp_data);
  defaultValue = Tresp_data {srio_tx_tresp_data:0, srio_tx_tresp_crf:False, srio_tx_tresp_prio:0,
  srio_tx_tresp_ftype:0, srio_tx_tresp_tid:0, srio_tx_tresp_ttype:0, 
  srio_tx_tresp_status:0, srio_tx_tresp_no_data:True};
endinstance

/// Target response message signals
typedef struct {MsgSeg srio_tx_tresp_msg_seg;
  Bit#(6) srio_tx_tresp_mbox;
  Mletter srio_tx_tresp_letter;
} Tresp_msg deriving (Bits, Eq, Bounded);

instance DefaultValue#(Tresp_msg);
  defaultValue = Tresp_msg {srio_tx_tresp_msg_seg:0, srio_tx_tresp_mbox:0, srio_tx_tresp_letter:0};
endinstance
// Target response packet
typedef struct {Tresp_ctrl trespcntrl;
  Tresp_data trespdata;
  Tresp_data trespmsg;
} Tresp_pkt deriving (Bits, Eq, Bounded);

instance DefaultValue#(Tresp_pkt);
  defaultValue = Tresp_pkt {trespcntrl:defaultValue, trespdata:defaultValue, trespmsg:defaultValue};
endinstance


//////////////////////  MAINTENANCE REQUEST //////////////////////

/// Maintenance request control signals
typedef struct {Bool srio_rx_mreq_sof_n;
  Bool srio_rx_mreq_eof_n;
  Bool srio_rx_mreq_vld_n;
} Mreq_ctrl deriving (Bits, Eq, Bounded);

instance DefaultValue#(Mreq_ctrl);
  defaultValue = Mreq_ctrl {srio_rx_mreq_sof_n:False, srio_rx_mreq_eof_n:False,
  srio_rx_mreq_vld_n:False};
endinstance


/// Maintenance request data signals
typedef struct {Data srio_rx_mreq_data;
  Bool srio_rx_mreq_crf;
  Prio srio_rx_mreq_prio;
  Type srio_rx_mreq_ftype; 
  Type srio_rx_mreq_ttype;
  TranId srio_rx_mreq_tid;
  Offset srio_rx_mreq_offset;
  ByteCount srio_rx_mreq_byte_count;
  ByteEn srio_rx_mreq_byte_en_n;
  Bool srio_rx_mreq_local;
} Mreq_data deriving (Bits, Eq, Bounded);

instance DefaultValue#(Mreq_data);
  defaultValue = Mreq_data {srio_rx_mreq_data:0, srio_rx_mreq_crf:False, srio_rx_mreq_prio:0,
  srio_rx_mreq_ftype:0, srio_rx_mreq_ttype:0, srio_rx_mreq_tid:0,srio_rx_mreq_offset:0,
  srio_rx_mreq_byte_count:0, srio_rx_mreq_byte_en_n:0, srio_rx_mreq_local:False};
endinstance

// Maintenance request packet
typedef struct {Mreq_ctrl mreqcntrl;
  Mreq_data mreqdata;
} Mreq_pkt deriving (Bits, Eq, Bounded);

instance DefaultValue#(Mreq_pkt);
  defaultValue = Mreq_pkt {mreqcntrl:defaultValue, mreqdata:defaultValue};
endinstance


///////////////////  MAINTENANCE RESPONSE ////////////////////


//Maintenance response signals

typedef struct {Bool srio_tx_mresp_sof_n;
  Bool srio_tx_mresp_eof_n;
  Bool srio_tx_mresp_vld_n;
} Mresp_ctrl deriving (Bits, Eq, Bounded);

instance DefaultValue#(Mresp_ctrl);
  defaultValue = Mresp_ctrl {srio_tx_mresp_sof_n:False, srio_tx_mresp_eof_n:False,
  srio_tx_mresp_vld_n:False};
endinstance

/// Maintenance response data signals
typedef struct {Data srio_tx_mresp_data;
  Bool srio_tx_mresp_crf;
  Prio srio_tx_mresp_prio;
  Type srio_tx_mresp_ftype; 
  Type srio_tx_mresp_ttype;
  Bit#(8) srio_tx_mresp_hop_count;
  TranId srio_tx_mresp_tid;
  Bool srio_tx_mresp_local;
  Status srio_tx_mresp_status;
} Mresp_data deriving (Bits, Eq, Bounded);

instance DefaultValue#(Mresp_data);
  defaultValue = Mresp_data {srio_tx_mresp_data:0, srio_tx_mresp_crf:False, srio_tx_mresp_prio:0,
  srio_tx_mresp_ftype:0, srio_tx_mresp_ttype:0, srio_tx_mresp_tid:0, srio_tx_mresp_hop_count:0,
  srio_tx_mresp_status:0, srio_tx_mresp_local:False};
endinstance

// Maintenance response packet
typedef struct {Mresp_ctrl mrespcntrl;
  Mresp_data mrespdata;
} Mresp_pkt deriving (Bits, Eq, Bounded);

instance DefaultValue#(Mresp_pkt);
  defaultValue= Mresp_pkt {mrespcntrl:defaultValue, mrespdata:defaultValue};
endinstance

//////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////TRANSPORT LAYER ///////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

// -- Initiator request 
typedef struct {TT srio_tx_ireq_tt;
  DestId srio_tx_ireq_dest_id;
  SourceId srio_tx_ireq_source_id;} Ireq_tt deriving (Bits, Eq, Bounded);

instance DefaultValue# (Ireq_tt);
  defaultValue = Ireq_tt {srio_tx_ireq_tt:0, srio_tx_ireq_dest_id:0,
  srio_tx_ireq_source_id:0};
endinstance


// -- Initiator response

typedef struct {TT srio_rx_iresp_tt;
  DestId srio_rx_iresp_dest_id;
  SourceId srio_rx_iresp_source_id;} Iresp_tt deriving (Bits, Eq, Bounded);

instance DefaultValue# (Iresp_tt);
  defaultValue = Iresp_tt {srio_rx_iresp_tt:0, srio_rx_iresp_dest_id:0,
  srio_rx_iresp_source_id:0};
endinstance

// -- Target request

typedef struct {TT srio_rx_treq_tt;
  DestId srio_rx_treq_dest_id;
  SourceId srio_rx_treq_source_id;} Treq_tt deriving (Bits, Eq, Bounded);

instance DefaultValue# (Treq_tt);
  defaultValue = Treq_tt {srio_rx_treq_tt:0, srio_rx_treq_dest_id:0,
  srio_rx_treq_source_id:0};
endinstance

// -- Target response

typedef struct {TT srio_tx_tresp_tt;
  DestId srio_tx_tresp_dest_id;
  SourceId srio_tx_tresp_source_id;} Tresp_tt deriving (Bits, Eq, Bounded);

instance DefaultValue# (Tresp_tt);
  defaultValue = Tresp_tt {srio_tx_tresp_tt:0, srio_tx_tresp_dest_id:0,
  srio_tx_tresp_source_id:0};
endinstance

// -- Maintenance request

typedef struct {TT srio_tx_Mreq_tt;
  DestId srio_tx_Mreq_dest_id;
  SourceId srio_tx_Mreq_source_id;} Mreq_tt deriving (Bits, Eq, Bounded);

instance DefaultValue# (Mreq_tt);
  defaultValue = Mreq_tt {srio_tx_Mreq_tt:0, srio_tx_Mreq_dest_id:0,
  srio_tx_Mreq_source_id:0};
endinstance

typedef struct {TT srio_tx_Mresp_tt;
  DestId srio_tx_Mresp_dest_id;
  SourceId srio_tx_Mresp_source_id;} Mresp_tt deriving (Bits, Eq, Bounded);

instance DefaultValue# (Mresp_tt);
  defaultValue = Mresp_tt {srio_tx_Mresp_tt:0, srio_tx_Mresp_dest_id:0,
  srio_tx_Mresp_source_id:0};
endinstance

typedef struct {
	Data data;
	Bit#(4) txrem;
	Bool crf; 
} Transmit_pkt deriving (Bits, Eq);

instance DefaultValue#(Transmit_pkt); // Assigning Default value for the Tranmit_Pkt 
	defaultValue = Transmit_pkt {data: 0, txrem: 0, crf: False};
endinstance 

endpackage : srio_txrx_dtypes
