/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: M Gopinathan
Email id: gopinathan18@gmail.com
Details: AXI slave transactor - yet to be updated

--------------------------------------------------------------------------------------------------
*/
package srio_axi4_slave;
`include "defines_srio.bsv"
import Semi_FIFOF::*;
import AXI4_Types::*;
import BUtils ::*;
import srio_txrx_dtypes ::*;
import srio_txrx_txblock_ll_ireq ::*;
import srio_txrx_txblock_tl_ireq ::*;
import srio_txrx_txblock_ll_iresp ::*;
import srio_txrx_txblock_tl_iresp ::*;
import DefaultValue ::*;

interface Ifc_srio_axi4_slave;
 interface AXI4_Slave_IFC#(`ADDR, `DATA,`USERSPACE) srio_axi4_slave;
endinterface

typedef enum{Send_init_req,Get_init_resp} Init_Mem_state deriving(Bits,Eq);
module mksrio_axi4_slave(Ifc_srio_axi4_slave);

AXI4_Slave_Xactor_IFC #(`ADDR, `DATA, `USERSPACE) initiator_slave_xactor <- mkAXI4_Slave_Xactor;
Ifc_txblock_ll_ireq srio_ireq_ll <- mksrio_txrx_txblock_ll_ireq;
Ifc_txblock_tl_ireq srio_ireq_tl <- mksrio_txrx_txblock_tl_ireq;

Ifc_txblock_ll_iresp srio_iresp_ll <- mksrio_txrx_txblock_ll_iresp;
Ifc_txblock_tl_iresp srio_iresp_tl <- mksrio_txrx_txblock_tl_iresp;


Reg#(Bit#(`ADDR)) rg_ireq_address <- mkReg(0);
Reg#(Bit#(3)) rg_ireq_transfer_size <- mkReg(0);
Reg#(Bit#(4)) rg_ireq_id <-mkReg(0);
Reg#(Bit#(8)) rg_ireq_readburst_value <-mkReg(0);
Reg#(Init_Mem_state) rg_init_read_state[2] <- mkCReg(2, Send_init_req);
Reg#(Init_Mem_state) rg_init_write_state[2] <- mkCReg(2, Send_init_req);


rule rl_slave_initiator_read_request(rg_init_read_state[1] == Send_init_req);

let lv_irq_ar <- pop_o(initiator_slave_xactor.o_rd_addr);
$display($stime(),": received read request from host aruser %d",lv_irq_ar.aruser);
rg_ireq_address <= lv_irq_ar.araddr;
rg_ireq_transfer_size <= lv_irq_ar.arsize; // Burst size - maximum number of bytes to transfer in each
                                    //data transfer, or beat, in a burst
rg_ireq_readburst_value <= lv_irq_ar.arlen; // Burst length
rg_ireq_id <= lv_irq_ar.arid; // Transaction ID

let lv_init_req_data  =  Ireq_data {srio_tx_ireq_data : 128'b0, // To come from AXI
                      srio_tx_ireq_crf : False, // To be implemented later
                      srio_tx_ireq_prio : 2'b00, // To be implemented later
                      srio_tx_ireq_ftype : 4'b0010, // Hard coded for Read
                      srio_tx_ireq_tid : 8'hFF, // Generate Internally
                      srio_tx_ireq_ttype : 4'b0100, // To come from AXI                        
                      srio_tx_ireq_addr : lv_irq_ar.araddr[49:0],
                      srio_tx_ireq_hopcount : 8'h00, // To be implemented later
                      srio_tx_ireq_byte_count : 9'b0, // To come from AXI
                      srio_tx_ireq_byte_en_n : 8'b0, // To come from AXI
                      srio_tx_ireq_local: False}; // To be implemented later



let lv_tl_pkt = Ireq_tt  {srio_tx_ireq_tt : 2'b01 , // To come from AXI
                       srio_tx_ireq_dest_id : 32'h000000FF, // To come from AXI
                       srio_tx_ireq_source_id : 32'h0000000F}; // To come from AXI

let lv_init_req_msg = Ireq_msg {srio_tx_ireq_db_info : 16'b0, // To be implemented later
                      srio_tx_ireq_msg_len : 4'b0, // To be implemented later
                      srio_tx_ireq_msg_seg : 4'b0, // To be implemented later
                      srio_tx_ireq_mbox : 6'b0, //  To be implemented later
                      srio_tx_ireq_letter : 2'b0}; // To be implemented later

srio_ireq_ll.data_signals(lv_init_req_data);
srio_ireq_ll.msg_signals(lv_init_req_msg);
srio_ireq_tl.transport_signals(lv_tl_pkt);

rg_init_read_state[1] <= Get_init_resp;

endrule 

rule rl_slave_initiator_read_response (rg_init_read_state[0] == Get_init_resp);

let lv_iresp_data_ll = srio_iresp_ll.data_signals;
let lv_iresp_msg_ll = srio_ireq_ll.msg_signals;
let lv_iresp_tl = srio_iresp_tl.transport_signals;


let lv_r = AXI4_Rd_Data {rresp: AXI4_OKAY, rdata:
lv_iresp_data_ll.srio_rx_iresp_data, rlast: True, ruser:0, rid: ?};

initiator_slave_xactor.i_rd_data.enq(lv_r);
rg_init_read_state[0] <= Send_init_req;
endrule

rule rl_slave_initiator_write_request (rg_init_write_state[1] == Send_init_req);

let lv_irq_aw <- pop_o(initiator_slave_xactor.o_wr_addr);                                               
let lv_irq_w  <- pop_o(initiator_slave_xactor.o_wr_data); 

let lv_init_req_data  =  Ireq_data {srio_tx_ireq_data : lv_irq_w.wdata, // To come from AXI
                      srio_tx_ireq_crf : False, // To be implemented later
                      srio_tx_ireq_prio : 2'b00, // To be implemented later
                      srio_tx_ireq_ftype : 4'b0101, // Hard coded for Read
                      srio_tx_ireq_tid : 8'hFF, // Generate Internally
                      srio_tx_ireq_ttype : 4'b0100, // To come from AXI                        
                      srio_tx_ireq_addr : lv_irq_aw.awaddr[49:0],
                      srio_tx_ireq_hopcount : 8'h00, // To be implemented later
                      srio_tx_ireq_byte_count : 9'b0, // To come from AXI
                      srio_tx_ireq_byte_en_n : 8'b0, // To come from AXI
                      srio_tx_ireq_local: False}; // To be implemented later



let lv_tl_pkt = Ireq_tt  {srio_tx_ireq_tt : 2'b01 , // To come from AXI
                       srio_tx_ireq_dest_id : 32'h000000FF, // To come from AXI
                       srio_tx_ireq_source_id : 32'h0000000F}; // To come from AXI

let lv_init_req_msg = Ireq_msg {srio_tx_ireq_db_info : 16'b0, // To be implemented later
                      srio_tx_ireq_msg_len : 4'b0, // To be implemented later
                      srio_tx_ireq_msg_seg : 4'b0, // To be implemented later
                      srio_tx_ireq_mbox : 6'b0, //  To be implemented later
                      srio_tx_ireq_letter : 2'b0}; // To be implemented later

srio_ireq_ll.data_signals(lv_init_req_data);
srio_ireq_ll.msg_signals(lv_init_req_msg);
srio_ireq_tl.transport_signals(lv_tl_pkt);

rg_init_write_state[1] <= Get_init_resp;

endrule

rule rl_slave_initiator_write_response (rg_init_write_state[0] == Get_init_resp);
let lv_iresp_data_ll = srio_iresp_ll.data_signals;
let lv_iresp_msg_ll = srio_ireq_ll.msg_signals;
let lv_iresp_tl = srio_iresp_tl.transport_signals;


let lv_r = AXI4_Wr_Resp {bresp: AXI4_OKAY, buser:0, bid: ?};

initiator_slave_xactor.i_wr_resp.enq(lv_r);
rg_init_write_state[0] <= Send_init_req;
endrule


interface srio_axi4_slave= initiator_slave_xactor.axi_side;
endmodule : mksrio_axi4_slave
endpackage : srio_axi4_slave
