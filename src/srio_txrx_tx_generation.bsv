package srio_txrx_tx_generation;

import DefaultValue ::*;
import srio_txrx_dtypes ::*;
import srio_txrx_ftypes ::*;
import srio_txrx_txblock_ll_ireq ::*;
import srio_txrx_txblock_tl_ireq ::*;
import srio_txrx_txdev8_functions ::*;
import srio_txrx_txdev16_functions ::*;
import srio_txrx_init_encoder ::*;
import FIFO ::*;
import FIFOF ::*;
import SpecialFIFOs ::*;

interface Ifc_srio_txrx_tx_generation;
  method Action ma_init_packet_ll (Ireq_pkt pkt); 
  method Action ma_packet_tl (Ireq_tt pkt);
  method Transmit_pkt ma_tx_pkt_ll_tl ();

  method Action ma_pkgen_rdy_n (Bool value);

endinterface : Ifc_srio_txrx_tx_generation

module mksrio_txrx_tx_generation (Ifc_srio_txrx_tx_generation);

Wire#(Ireq_pkt) wr_init_pkt <- mkDWire(defaultValue);
Wire#(Ireq_tt) wr_tt_pkt <- mkDWire(defaultValue);
Wire#(Bool) wr_ready <- mkDWire(False);
Wire#(Transmit_pkt) wr_pktgen_transmit_pkt <- mkDWire(defaultValue);
FIFOF#(Transmit_pkt) ff_transmit_pkt <- mkSizedBypassFIFOF (8);

Wire#(Data) wr_ftype2_out_pkt <- mkDWire(0);
Wire#(Bool) wr_pktgen_dsc <- mkDWire(False);
Wire#(Bit#(4)) wr_ftype2_txrem <- mkDWire(0);
Reg#(Ftype2_read_request) rg_ftype2_request_pkt <- mkReg(defaultValue);

rule rl_ftype2_pkt_concatination;
  if (wr_init_pkt.ireqdata.srio_tx_ireq_ftype == 4'b0010)
    begin
      let {sz,ptr} <- fn_init_encoder (True, wr_init_pkt.ireqdata.srio_tx_ireq_byte_count,
                                      wr_init_pkt.ireqdata.srio_tx_ireq_byte_en_n);
  
      rg_ftype2_request_pkt <= Ftype2_read_request {prio: wr_init_pkt.ireqdata.srio_tx_ireq_prio,
                              ftype: wr_init_pkt.ireqdata.srio_tx_ireq_ftype,
                              ttype: wr_init_pkt.ireqdata.srio_tx_ireq_ttype,
                              rdsize: sz, 
                              srctid: wr_init_pkt.ireqdata.srio_tx_ireq_tid,
                              addr: wr_init_pkt.ireqdata.srio_tx_ireq_addr[47:3],
                              wdptr: ptr,
                              xamsbs: wr_init_pkt.ireqdata.srio_tx_ireq_addr[49:48]};
    end
    else
      rg_ftype2_request_pkt <= defaultValue;
endrule

// will be merged in one rule changing the register to wire

rule rl_ftype2_pkt_generation(rg_ftype2_request_pkt.ftype == 4'b0010);
  case (wr_tt_pkt.srio_tx_ireq_tt)matches
      'b00  : begin // 8bit device ID
              wr_ftype2_out_pkt <=
              fn_dev8_ftype2_header(rg_ftype2_request_pkt,wr_tt_pkt.srio_tx_ireq_dest_id[7:0],
              wr_tt_pkt.srio_tx_ireq_source_id[7:0], wr_tt_pkt.srio_tx_ireq_tt);
              wr_ftype2_txrem <= 4'b1011;
              end

      'b01  : begin // 16bit device ID
              wr_ftype2_out_pkt <= 
              fn_dev16_ftype2_header(rg_ftype2_request_pkt,wr_tt_pkt.srio_tx_ireq_dest_id[15:0],
              wr_tt_pkt.srio_tx_ireq_source_id[15:0], wr_tt_pkt.srio_tx_ireq_tt);
              wr_ftype2_txrem <= 4'b1011;
              end
  endcase
endrule

rule rl_datapkt_valid;
  Data lv_output_data_pkt = wr_ftype2_out_pkt; // or with other ftypes in future
 Bit#(4) lv_pkt_gen_txrem = wr_ftype2_txrem;

// store it in the FIFO
if (lv_pkt_gen_vld == True)
    begin
      Transmit_pkt lv_Transmit_pkt = Transmit_pkt {data:lv_output_data_pkt, txrem:lv_pkt_gen_txrem,
      crf:False};
      ff_transmit_pkt.enq(lv_Transmit_pkt);
    end
endrule


rule rl_fifo_first;
  wr_pktgen_transmit_pkt <= ff_transmit_pkt.first();
endrule

rule rl_fifo_clear(wr_init_pkt.ireqcntrl.srio_tx_ireq_dsc_n == True);
wr_pktgen_dsc <= True;
ff_transmit_pkt.clear();
endrule
  

rule rl_fifo_dequeue(wr_ready == False && ff_transmit_pkt.first.vld == True );
  ff_transmit_pkt.deq();
endrule

method Action ma_init_packet_ll (Ireq_pkt pkt);
  wr_init_pkt <= pkt;
endmethod

method Action ma_packet_tl (Ireq_tt pkt);
  wr_tt_pkt <= pkt;
endmethod

method Action ma_pkgen_rdy_n (Bool value);
  wr_ready <= value;
endmethod

method Transmit_pkt ma_tx_pkt_ll_tl ();
  return Transmit_pkt {data: wr_pktgen_transmit_pkt.data,
                       txrem: wr_pktgen_transmit_pkt.txrem,
                       crf: False};
          
endmethod

endmodule : mksrio_txrx_tx_generation

endpackage : srio_txrx_tx_generation
