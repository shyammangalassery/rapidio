/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: M Gopinathan
Email id: gopinathan18@gmail.com
Details: This file contains all packet format types

--------------------------------------------------------------------------------------------------
*/

package srio_txrx_ftypes;
import DefaultValue ::*;
import srio_txrx_dtypes ::*;

////////////////////////////////////////////////////////////////////////////////
/// Struct Definition for Ftypes Packet requirements
////////////////////////////////////////////////////////////////////////////////

// Ftype0 - Implementation Defined (User)

// Ftype1 - Reserved Intervention-Request Class
//typedef struct {
//} FType1_InterventionRequest;

// Ftype2 - Request Class
typedef struct {Prio prio;
  Type ftype;
  Type ttype;
  RdSize rdsize;
  TranId srctid;
  Bit#(45) addr;
  WdPointer wdptr;
  Xmsb xamsbs;
} Ftype2_read_request deriving (Bits, Eq);

instance DefaultValue#(Ftype2_read_request); // Ftype 2 Default Value
    defaultValue = Ftype2_read_request {prio:0, ftype:0, ttype:0, rdsize:0, srctid:0, addr:0, wdptr:0,
    xamsbs:0};
endinstance

// Ftype 3 & 4 - Reserved

// Ftype 5 - Write Class
typedef struct {Type ftype;
  Type ttype;
  WrSize wrsize;
  TranId srctid;
  Bit#(45) addr;
  WdPointer wdptr;
  Xmsb xamsbs;
  Data data;
} Ftype5_write_request deriving (Bits, Eq);

instance DefaultValue#(Ftype5_write_request); // Ftype 5 Default Value
    defaultValue = Ftype5_write_request {ftype:0, ttype:0, wrsize:0, srctid:0, addr:0, wdptr:0, 
    xamsbs:0, data:0};
endinstance

// Ftype 6 - Streaming Write

typedef struct {Type ftype;
  Bit#(45) addr;
  Xmsb xamsbs;
  Data data;
} Ftype6_streaming_write deriving (Bits, Eq);

instance DefaultValue#(Ftype6_streaming_write); // Ftype 6 Default value
  defaultValue = Ftype6_streaming_write {ftype:0, addr:0, xamsbs:0, data:0};
endinstance

// Ftype 7 -Flow Control                                                                           
typedef struct {                                                                           
} FType7_flowcntl deriving (Bits, Eq, Bounded); // Yet to implement  

// Ftype 8 - Maintenance

endpackage : srio_txrx_ftypes
