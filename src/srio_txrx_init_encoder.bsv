package srio_txrx_init_encoder;

import Assert::*;
import srio_txrx_dtypes ::*;

function ActionValue#(Tuple2#(Size, WdPointer)) fn_init_encoder (Bool read, ByteCount byte_count,
ByteEn byte_en);
  return (
    actionvalue
    Size size =0;
    WdPointer wdpointer =0;
    case (byte_count) matches
    'd256   : begin
                size = 4'b1111;
                wdpointer = 1'b1;
    	        end
    'd224  :  begin
                if (read == True) begin
                  size = 4'b1111;
                  wdpointer = 1'b0;
    	        end
                else begin
                  dynamicAssert(True, "Oops!!!, Cannot Perform Write Operation using this ByteCount\n");
                end
              end
    'd192  :  begin
                if (read == True)	begin
                  size = 4'b1110;
                  wdpointer = 1'b1;
              end
                else begin
                  dynamicAssert(True, "Oops!!!, Cannot Perform Write Operation using this ByteCount\n");
                end
              end
    'd160   : begin
              if (read == True)	begin
                  size = 4'b1110;
                  wdpointer = 1'b0;
              end
                else begin
                  dynamicAssert(True, "Oops!!!, Cannot Perform Write Operation using this ByteCount\n");
                end
              end
    'd128   : begin
              size = 4'b1101;
              wdpointer = 1'b1;
              end
    'd96    : begin
              if (read == True)	begin
                  size = 4'b1101;
                  wdpointer = 1'b0;
              end
                else begin
                  dynamicAssert(True, "Oops!!!, Cannot Perform Write Operation using this ByteCount\n");
                end
              end
    'd64    : begin
                size = 4'b1100;
                wdpointer = 1'b1;
              end
    'd32    : begin
                size = 4'b1100;
                wdpointer = 1'b0;
              end
    'd16    : begin
                size = 4'b1011;
                wdpointer = 1'b1;
              end
    'd8     : begin
                if (byte_en == 8'b11111111) begin
                  size = 4'b1011;
                  wdpointer = 1'b0;
                end 
                else begin
                  dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                end
              end
    'd7     : begin
                if (byte_en == 8'b11111110) begin
                  size = 4'b1010;
                  wdpointer = 1'b0;
                end else if (byte_en == 8'b01111111) begin
                  size = 4'b1010;
                  wdpointer = 1'b1;
                end
                else begin
                  dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                end
              end
    'd6     : begin
                if (byte_en == 8'b11111100) begin
                  size = 4'b1001;
                  wdpointer = 1'b0;
                end else if (byte_en == 8'b00111111) begin
                  size = 4'b1001;
                  wdpointer = 1'b1;
                end
                  else begin
                    dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                  end
              end
    'd5     : begin
                if (byte_en == 8'b11111000) begin
                  size = 4'b0111;
                  wdpointer = 1'b0;
                end else if (byte_en == 8'b00011111) begin
                  size = 4'b0111;
                  wdpointer = 1'b1;
                end
                  else begin
                    dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                  end
              end
    'd4     : begin
                if (byte_en == 8'b11110000) begin
                  size = 4'b1000;
                  wdpointer = 1'b0;
                end else if (byte_en == 8'b00001111) begin
                  size = 4'b1000;
                  wdpointer = 1'b1;
                end
                  else begin
                    dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                  end
              end
    'd3     : begin
                if (byte_en == 8'b11100000) begin
                  size = 4'b0101;
                  wdpointer = 1'b0;
                end else if (byte_en == 8'b00000111) begin
                  size = 4'b0101;
                  wdpointer = 1'b1;
                end 
                  else begin
                    dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                  end
              end
    'd2     : begin
                if (byte_en == 8'b11000000) begin
                  size = 4'b0100;
                  wdpointer = 1'b0;
                end else if (byte_en == 8'b00110000) begin
                  size = 4'b0110;
                  wdpointer = 1'b0;
                end else if (byte_en == 8'b00001100) begin
                  size = 4'b0100;
                  wdpointer = 1'b1;
                end else if (byte_en == 8'b00000011) begin
                  size = 4'b0110;
                  wdpointer = 1'b1;
                end
                  else begin
                    dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                  end
              end
    'd1     : begin
              if (byte_en == 8'b10000000) begin
                size = 4'b0000;
                wdpointer = 1'b0;
              end else if (byte_en == 8'b01000000) begin
                size = 4'b0001;
                wdpointer = 1'b0;
              end else if (byte_en == 8'b00100000) begin
                size = 4'b0010;
                wdpointer = 1'b0;
              end else if (byte_en == 8'b00010000) begin
                size = 4'b0011;
                wdpointer = 1'b0;
              end else if (byte_en == 8'b00001000) begin
                size = 4'b0000;
                wdpointer = 1'b1;
              end else if (byte_en == 8'b00000100) begin
                size = 4'b0001;
                wdpointer = 1'b1;
              end else if (byte_en == 8'b00000010) begin
                size = 4'b0010;
                wdpointer = 1'b1;
              end else if (byte_en == 8'b00000001) begin
                size = 4'b0011;
                wdpointer = 1'b1;
              end
                else begin 
                  dynamicAssert(True, "Oops!!!, This Byte Enable field not Supported\n");
                end  
             end
    
      default : begin
                  dynamicAssert(True, "NOTE : Value of ByteCount is Not Matched with the case. So Default value is Used\n ");
        size = 4'b1111;
        wdpointer = 1'b1;
      end  
    endcase
    return tuple2(size, wdpointer);
    endactionvalue );
    
  endfunction

endpackage : srio_txrx_init_encoder 
