/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO_TargetRespIFC Testbench
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO_TargetRespIFC. 
-- Generation of Target Response signals for 32 bit device, 16 bit device and 8 bit device
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_TargetRespIFC;

import DefaultValue ::*;
import RapidIO_DTypes::*;
import RapidIO_InitEncoder_WdPtr_Size::*;
import RapidIO_TargetRespIFC::*;

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkTb_for_RapidIO_TargetRespIFC(Empty);

// Interface

Ifc_TargetRespSignals  targetrespsignals <- mkRapidIO_TargetRespIFC();


Wire#(Bool) wr_ready <- mkDWire (False); 
Wire#(TargetRespIfcPkt) wr_target_resp_sig <- mkDWire (defaultValue);
Wire#(TargetRespIfcCntrl) wr_control <- mkDWire (defaultValue);
Wire#(TargetRespIfcData) wr_data <- mkDWire (defaultValue);
Wire#(TargetRespIfcMsg) wr_msg <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_packet <- mkDWire (defaultValue);

// Clock Declaration

Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);		

//  clock

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 8)
	$finish (0);
endrule


// The device type is selected w.r.t. tt

rule rl_input0(reg_ref_clk == 0);
	$display ("\n \n For 32 bit device " );
	targetrespsignals._TgtRespIfc._tresp_sof_n(False);
	targetrespsignals._TgtRespIfc._tresp_eof_n(True);
	targetrespsignals._TgtRespIfc._tresp_vld_n(False);
	targetrespsignals._TgtRespIfc._tresp_dsc_n(True);
	targetrespsignals._TgtRespIfc._tresp_tt(2'b10);
	targetrespsignals._TgtRespIfc._tresp_data(64'h9999999999999999);
	targetrespsignals._TgtRespIfc._tresp_crf(False);
	targetrespsignals._TgtRespIfc._tresp_prio(2'b01);
	targetrespsignals._TgtRespIfc._tresp_ftype(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_dest_id(32'hda34568c);
	targetrespsignals._TgtRespIfc._tresp_status(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_tid(8'hbf);
	targetrespsignals._TgtRespIfc._tresp_ttype(4'b0100);
	targetrespsignals._TgtRespIfc._tresp_no_data(False);
	targetrespsignals._TgtRespIfc._tresp_msg_seg(4'b0010);
	targetrespsignals._TgtRespIfc._tresp_mbox(2'b10);
	targetrespsignals._TgtRespIfc._tresp_letter(2'b10);
	targetrespsignals._inputs_TRespRDYIn_From_Concat(False);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 271)

rule rl_in0(reg_ref_clk == 0);
	wr_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_data <= TargetRespIfcData {tresp_tt:2'b10, tresp_data:64'h9999999999999999, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b0101, 			   	              tresp_dest_id:32'hda34568c,tresp_status:4'b0101,tresp_tid:8'hbf,tresp_ttype:4'b0100, tresp_no_data:False};
	wr_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Eof is made active low so that packet is not generated 
	
rule rl_input1(reg_ref_clk == 1);
	targetrespsignals._TgtRespIfc._tresp_sof_n(True);
	targetrespsignals._TgtRespIfc._tresp_eof_n(False);
	targetrespsignals._TgtRespIfc._tresp_vld_n(True);
	targetrespsignals._TgtRespIfc._tresp_dsc_n(False);
	targetrespsignals._TgtRespIfc._tresp_tt(2'b10);
	targetrespsignals._TgtRespIfc._tresp_data(64'h9999888899999999);
	targetrespsignals._TgtRespIfc._tresp_crf(False);
	targetrespsignals._TgtRespIfc._tresp_prio(2'b01);			
	targetrespsignals._TgtRespIfc._tresp_ftype(4'b0101); 
	targetrespsignals._TgtRespIfc._tresp_dest_id(32'hda34568c);
	targetrespsignals._TgtRespIfc._tresp_status(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_tid(8'hbf);			 
	targetrespsignals._TgtRespIfc._tresp_ttype(4'b0100);
	targetrespsignals._TgtRespIfc._tresp_no_data(True);
	targetrespsignals._TgtRespIfc._tresp_msg_seg(4'b0010);
	targetrespsignals._TgtRespIfc._tresp_mbox(2'b10);
	targetrespsignals._TgtRespIfc._tresp_letter(2'b10);
	targetrespsignals._inputs_TRespRDYIn_From_Concat(False);
endrule

// All values are made zero(default) since eof is asserted

rule rl_in1(reg_ref_clk == 1);
	wr_control <= TargetRespIfcCntrl {tresp_sof:False, tresp_eof:True, tresp_vld:False, tresp_dsc:True};	
	wr_data <= TargetRespIfcData {tresp_tt:0, tresp_data:0, tresp_crf:False, tresp_prio:0, tresp_ftype:0, tresp_dest_id:0, tresp_status:0, 			   			   tresp_tid:0,tresp_ttype:0,tresp_no_data:True };
	wr_msg <= TargetRespIfcMsg {tresp_msg_seg:0, tresp_mbox:0, tresp_letter:0};
endrule

// 16 Bit Device 

rule rl_input2(reg_ref_clk == 2);
	$display ("\n \n For 16 bit device " );
	targetrespsignals._TgtRespIfc._tresp_sof_n(False);
	targetrespsignals._TgtRespIfc._tresp_eof_n(True);
	targetrespsignals._TgtRespIfc._tresp_vld_n(False);
	targetrespsignals._TgtRespIfc._tresp_dsc_n(True);
	targetrespsignals._TgtRespIfc._tresp_tt(2'b01);
	targetrespsignals._TgtRespIfc._tresp_data(64'h9999999999999999);
	targetrespsignals._TgtRespIfc._tresp_crf(False);
	targetrespsignals._TgtRespIfc._tresp_prio(2'b01);
	targetrespsignals._TgtRespIfc._tresp_ftype(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_dest_id(32'hda8c0000);
	targetrespsignals._TgtRespIfc._tresp_status(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_tid(8'hbf);
	targetrespsignals._TgtRespIfc._tresp_ttype(4'b0100);
	targetrespsignals._TgtRespIfc._tresp_no_data(False);
	targetrespsignals._TgtRespIfc._tresp_msg_seg(4'b0010);
	targetrespsignals._TgtRespIfc._tresp_mbox(2'b10);
	targetrespsignals._TgtRespIfc._tresp_letter(2'b10);
	targetrespsignals._inputs_TRespRDYIn_From_Concat(False);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 271)

rule rl_in2(reg_ref_clk == 2);
	wr_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_data <= TargetRespIfcData {tresp_tt:2'b01, tresp_data:64'h9999999999999999, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b0101, 			     	                tresp_dest_id:32'hda8c0000,tresp_status:4'b0101, tresp_tid:8'hbf,tresp_ttype:4'b0100,tresp_no_data:False};
	wr_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Eof is made active low so that packet is not generated 

rule rl_input3(reg_ref_clk == 3);
	targetrespsignals._TgtRespIfc._tresp_sof_n(True);
	targetrespsignals._TgtRespIfc._tresp_eof_n(False);
	targetrespsignals._TgtRespIfc._tresp_vld_n(True);
	targetrespsignals._TgtRespIfc._tresp_dsc_n(False);
	targetrespsignals._TgtRespIfc._tresp_tt(2'b01);
	targetrespsignals._TgtRespIfc._tresp_data(64'h9999888899999999);
	targetrespsignals._TgtRespIfc._tresp_crf(False);
	targetrespsignals._TgtRespIfc._tresp_prio(2'b01);			
	targetrespsignals._TgtRespIfc._tresp_ftype(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_dest_id(32'hda8c0000);
	targetrespsignals._TgtRespIfc._tresp_status(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_tid(8'hbf);
	targetrespsignals._TgtRespIfc._tresp_ttype(4'b0100);
	targetrespsignals._TgtRespIfc._tresp_no_data(True);
	targetrespsignals._TgtRespIfc._tresp_msg_seg(4'b0010);
	targetrespsignals._TgtRespIfc._tresp_mbox(2'b10);
	targetrespsignals._TgtRespIfc._tresp_letter(2'b10);
	targetrespsignals._inputs_TRespRDYIn_From_Concat(False);
endrule

// All values are made zero(default) since eof is asserted

rule rl_in3(reg_ref_clk == 3);
	wr_control <= TargetRespIfcCntrl {tresp_sof:False, tresp_eof:True, tresp_vld:False, tresp_dsc:True};	
	wr_data <= TargetRespIfcData {tresp_tt:0, tresp_data:0, tresp_crf:False, tresp_prio:0, tresp_ftype:0, tresp_dest_id:0, tresp_status:0, 			   			   tresp_tid:0,tresp_ttype:0,tresp_no_data:True };
	wr_msg <= TargetRespIfcMsg {tresp_msg_seg:0, tresp_mbox:0, tresp_letter:0};
endrule

// For 8 Bit Device 

rule rl_input4(reg_ref_clk == 4);
	$display ("\n \n For 8 bit device " );
	targetrespsignals._TgtRespIfc._tresp_sof_n(False);
	targetrespsignals._TgtRespIfc._tresp_eof_n(True);
	targetrespsignals._TgtRespIfc._tresp_vld_n(False);
	targetrespsignals._TgtRespIfc._tresp_dsc_n(True);
	targetrespsignals._TgtRespIfc._tresp_tt(2'b00);
	targetrespsignals._TgtRespIfc._tresp_data(64'h9999999999999999);
	targetrespsignals._TgtRespIfc._tresp_crf(False);
	targetrespsignals._TgtRespIfc._tresp_prio(2'b01);
	targetrespsignals._TgtRespIfc._tresp_ftype(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_dest_id(32'hda000000);
	targetrespsignals._TgtRespIfc._tresp_status(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_tid(8'hbf);
	targetrespsignals._TgtRespIfc._tresp_ttype(4'b0100);
	targetrespsignals._TgtRespIfc._tresp_no_data(False);
	targetrespsignals._TgtRespIfc._tresp_msg_seg(4'b0010);
	targetrespsignals._TgtRespIfc._tresp_mbox(2'b10);
	targetrespsignals._TgtRespIfc._tresp_letter(2'b10);
	targetrespsignals._inputs_TRespRDYIn_From_Concat(False);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 271)

rule rl_in4(reg_ref_clk == 4);
	wr_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_data <= TargetRespIfcData {tresp_tt:2'b00, tresp_data:64'h9999999999999999, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b0101, 			   	              tresp_dest_id:32'hda000000,tresp_status:4'b0101, tresp_tid:8'hbf,tresp_ttype:4'b0100,tresp_no_data:False};
	wr_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Eof is made active low so that packet is not generated

rule rl_input5(reg_ref_clk == 5);
	targetrespsignals._TgtRespIfc._tresp_sof_n(True);
	targetrespsignals._TgtRespIfc._tresp_eof_n(False);
	targetrespsignals._TgtRespIfc._tresp_vld_n(True);
	targetrespsignals._TgtRespIfc._tresp_dsc_n(False);
	targetrespsignals._TgtRespIfc._tresp_tt(2'b00);
	targetrespsignals._TgtRespIfc._tresp_data(64'h9999888899999999);
	targetrespsignals._TgtRespIfc._tresp_crf(False);
	targetrespsignals._TgtRespIfc._tresp_prio(2'b01);			
	targetrespsignals._TgtRespIfc._tresp_ftype(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_dest_id(32'hda000000);
	targetrespsignals._TgtRespIfc._tresp_status(4'b0101);
	targetrespsignals._TgtRespIfc._tresp_tid(8'hbf);
	targetrespsignals._TgtRespIfc._tresp_ttype(4'b0100);
	targetrespsignals._TgtRespIfc._tresp_no_data(True);
	targetrespsignals._TgtRespIfc._tresp_msg_seg(4'b0010);
	targetrespsignals._TgtRespIfc._tresp_mbox(2'b10);
	targetrespsignals._TgtRespIfc._tresp_letter(2'b10);
	targetrespsignals._inputs_TRespRDYIn_From_Concat(False);
endrule

// All values are made zero(default)  since eof is asserted

rule rl_in5(reg_ref_clk == 5);
	wr_control <= TargetRespIfcCntrl {tresp_sof:False, tresp_eof:True, tresp_vld:False, tresp_dsc:True};	
	wr_data <= TargetRespIfcData {tresp_tt:0, tresp_data:0, tresp_crf:False, tresp_prio:0, tresp_ftype:0, tresp_dest_id:0, tresp_status:0, 			   			   tresp_tid:0,tresp_ttype:0,tresp_no_data:True };
	wr_msg <= TargetRespIfcMsg {tresp_msg_seg:0, tresp_mbox:0, tresp_letter:0};
endrule

rule rl_output;
     	wr_ready <= targetrespsignals._TgtRespIfc.tresp_rdy_n_ ();		//output from target response
     	wr_target_resp_sig <= targetrespsignals.outputs_TgtRespIfcPkt_ ();	//output from target response
	wr_packet <= TargetRespIfcPkt {trespcntrl:wr_control, trespdata:wr_data, trespmsg:wr_msg};	//reference packet
endrule

rule rl_disp;
     $display("\n  Ready = %b , \n reference packet =  %b \n  Target Response Packet =     %b ",wr_ready,wr_packet,wr_target_resp_sig);
       
endrule

rule rl_compare;
	if(wr_packet == wr_target_resp_sig)
	$display("\n  Target Response packet is equal to reference packet ");
	else
	$display("\n  Target Response packet is NOT equal to reference packet ");	

endrule


endmodule

endpackage
