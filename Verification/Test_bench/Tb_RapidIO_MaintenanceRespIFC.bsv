/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO_MaintenanceRespIFC Testbench
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO_MaintenanceRespIFC. 
-- Generation of Maintenance Response signals 32 bit device, 16 bit device and 8 bit device
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_MaintenanceRespIFC;

import DefaultValue ::*;
import RapidIO_DTypes::*;
import RapidIO_InitEncoder_WdPtr_Size::*;
import RapidIO_MaintenanceRespIFC::*;

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkTb_for_RapidIO_MaintenanceRespIFC(Empty);

// Interface 

Ifc_MaintenanceRespSignals  maintenancerespsignals <- mkRapidIO_MaintenanceRespIFC();


Wire#(Bool) wr_ready <- mkDWire (False); 
Wire#(MaintenanceRespIfcPkt) wr_maintain_resp_sig <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcCntrl) wr_control <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcData) wr_data <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_packet <- mkDWire (defaultValue);

// Clock Declaration 

Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);		


//  Clock  

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 8)
	$finish (0);
endrule

// The device type is selected w.r.t. tt

rule rl_input0(reg_ref_clk == 0);
	$display ("\n \n For 32 bit device " );
	maintenancerespsignals._MaintainRespIFC._mresp_sof_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_eof_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_vld_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_tt(2'b10);
	maintenancerespsignals._MaintainRespIFC._mresp_data(64'h9999999999999999);
	maintenancerespsignals._MaintainRespIFC._mresp_crf(False);
	maintenancerespsignals._MaintainRespIFC._mresp_prio(2'b01);
	maintenancerespsignals._MaintainRespIFC._mresp_ftype(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_dest_id(32'hda34568c);
	maintenancerespsignals._MaintainRespIFC._mresp_status(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_tid(8'hbf);
	maintenancerespsignals._MaintainRespIFC._mresp_ttype(4'b0100);
	maintenancerespsignals._MaintainRespIFC._mresp_hop_count(8'hbb);
	maintenancerespsignals._MaintainRespIFC._mresp_local(True);
	maintenancerespsignals._inputs_MRespRDYIn_From_Concat(True);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 247)

rule rl_in0(reg_ref_clk == 0);
	wr_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_data <= MaintenanceRespIfcData {mresp_tt:2'b10, mresp_data:64'h9999999999999999, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b0101, 			   	              mresp_dest_id:32'hda34568c,mresp_status:4'b0101,mresp_tid:8'hbf,mresp_ttype:4'b0100, mresp_hop_count:8'hbb,mresp_local:True};
endrule

// Eof is made active low so that packet is not generated //

rule rl_input1(reg_ref_clk == 1);
	maintenancerespsignals._MaintainRespIFC._mresp_sof_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_eof_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_vld_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_tt(2'b10);
	maintenancerespsignals._MaintainRespIFC._mresp_data(64'h9999888899999999);
	maintenancerespsignals._MaintainRespIFC._mresp_crf(False);
	maintenancerespsignals._MaintainRespIFC._mresp_prio(0);			
	maintenancerespsignals._MaintainRespIFC._mresp_ftype(4'b0101); 
	maintenancerespsignals._MaintainRespIFC._mresp_dest_id(32'hda34568c);
	maintenancerespsignals._MaintainRespIFC._mresp_status(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_tid(8'hbf);			 
	maintenancerespsignals._MaintainRespIFC._mresp_ttype(4'b0100);
	maintenancerespsignals._MaintainRespIFC._mresp_hop_count(8'hbb);
	maintenancerespsignals._MaintainRespIFC._mresp_local(False);
	maintenancerespsignals._inputs_MRespRDYIn_From_Concat(True);
endrule

// All values are made zero(default) since eof is asserted

rule rl_in1(reg_ref_clk == 1);
	wr_control <= MaintenanceRespIfcCntrl {mresp_sof:False, mresp_eof:True, mresp_vld:False};	
	wr_data <= MaintenanceRespIfcData {mresp_tt:0, mresp_data:0, mresp_crf:False, mresp_prio:0, mresp_ftype:0, mresp_dest_id:0, mresp_status:0, 			   			   mresp_tid:0,mresp_ttype:0,mresp_hop_count:0,mresp_local:False };
endrule

// 16 Bit Device 

rule rl_input2(reg_ref_clk == 2);
	$display ("\n \n For 16 bit device " );
	maintenancerespsignals._MaintainRespIFC._mresp_sof_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_eof_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_vld_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_tt(2'b01);
	maintenancerespsignals._MaintainRespIFC._mresp_data(64'h9999999999999999);
	maintenancerespsignals._MaintainRespIFC._mresp_crf(False);
	maintenancerespsignals._MaintainRespIFC._mresp_prio(2'b01);
	maintenancerespsignals._MaintainRespIFC._mresp_ftype(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_dest_id(32'hda8c0000);
	maintenancerespsignals._MaintainRespIFC._mresp_status(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_tid(8'hbf);
	maintenancerespsignals._MaintainRespIFC._mresp_ttype(4'b0100);
	maintenancerespsignals._MaintainRespIFC._mresp_hop_count(8'hbb);
	maintenancerespsignals._MaintainRespIFC._mresp_local(False);
	maintenancerespsignals._inputs_MRespRDYIn_From_Concat(True);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 247)

rule rl_in2(reg_ref_clk == 2);
	wr_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_data <= MaintenanceRespIfcData {mresp_tt:2'b01, mresp_data:64'h9999999999999999, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b0101, 			     	                mresp_dest_id:32'hda8c0000,mresp_status:4'b0101, mresp_tid:8'hbf,mresp_ttype:4'b0100,mresp_hop_count:8'hbb, 						mresp_local:False};
endrule

// Eof is made active low so that packet is not generated

rule rl_input3(reg_ref_clk == 3);
	maintenancerespsignals._MaintainRespIFC._mresp_sof_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_eof_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_vld_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_tt(2'b01);
	maintenancerespsignals._MaintainRespIFC._mresp_data(64'h9999888899999999);
	maintenancerespsignals._MaintainRespIFC._mresp_crf(False);
	maintenancerespsignals._MaintainRespIFC._mresp_prio(0);			
	maintenancerespsignals._MaintainRespIFC._mresp_ftype(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_dest_id(32'hda8c0000);
	maintenancerespsignals._MaintainRespIFC._mresp_status(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_tid(8'hbf);
	maintenancerespsignals._MaintainRespIFC._mresp_ttype(4'b0100);
	maintenancerespsignals._MaintainRespIFC._mresp_hop_count(8'hbb);
	maintenancerespsignals._MaintainRespIFC._mresp_local(False);
endrule

// All values are made zero(default) since eof is asserted

rule rl_in3(reg_ref_clk == 3);
	wr_control <= MaintenanceRespIfcCntrl {mresp_sof:False, mresp_eof:True, mresp_vld:False};	
	wr_data <= MaintenanceRespIfcData {mresp_tt:0, mresp_data:0, mresp_crf:False, mresp_prio:0, mresp_ftype:0, mresp_dest_id:0, mresp_status:0, 			   			   mresp_tid:0,mresp_ttype:0,mresp_hop_count:0,mresp_local:False};
endrule

// For 8 Bit Device 

rule rl_input4(reg_ref_clk == 4);
	$display ("\n \n For 8 bit device " );
	maintenancerespsignals._MaintainRespIFC._mresp_sof_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_eof_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_vld_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_tt(2'b00);
	maintenancerespsignals._MaintainRespIFC._mresp_data(64'h9999999999999999);
	maintenancerespsignals._MaintainRespIFC._mresp_crf(False);
	maintenancerespsignals._MaintainRespIFC._mresp_prio(2'b01);
	maintenancerespsignals._MaintainRespIFC._mresp_ftype(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_dest_id(32'hda000000);
	maintenancerespsignals._MaintainRespIFC._mresp_status(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_tid(8'hbf);
	maintenancerespsignals._MaintainRespIFC._mresp_ttype(4'b0100);
	maintenancerespsignals._MaintainRespIFC._mresp_hop_count(8'hbb);
	maintenancerespsignals._MaintainRespIFC._mresp_local(False);
	maintenancerespsignals._inputs_MRespRDYIn_From_Concat(True);
endrule

// This rule passes the corresponding values to wires for generating reference packet (line 247)

rule rl_in4(reg_ref_clk == 4);
	wr_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_data <= MaintenanceRespIfcData {mresp_tt:2'b00, mresp_data:64'h9999999999999999, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b0101, 			   	              mresp_dest_id:32'hda000000,mresp_status:4'b0101, mresp_tid:8'hbf,mresp_ttype:4'b0100,mresp_hop_count:8'hbb,mresp_local:False };
endrule

// Eof is made active low so that packet is not generated

rule rl_input5(reg_ref_clk == 5);
	maintenancerespsignals._MaintainRespIFC._mresp_sof_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_eof_n(False);
	maintenancerespsignals._MaintainRespIFC._mresp_vld_n(True);
	maintenancerespsignals._MaintainRespIFC._mresp_tt(2'b00);
	maintenancerespsignals._MaintainRespIFC._mresp_data(64'h9999888899999999);
	maintenancerespsignals._MaintainRespIFC._mresp_crf(False);
	maintenancerespsignals._MaintainRespIFC._mresp_prio(0);			
	maintenancerespsignals._MaintainRespIFC._mresp_ftype(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_dest_id(32'hda000000);
	maintenancerespsignals._MaintainRespIFC._mresp_status(4'b0101);
	maintenancerespsignals._MaintainRespIFC._mresp_tid(8'hbf);
	maintenancerespsignals._MaintainRespIFC._mresp_ttype(4'b0100);
	maintenancerespsignals._MaintainRespIFC._mresp_hop_count(8'hbb);
	maintenancerespsignals._MaintainRespIFC._mresp_local(False);
	maintenancerespsignals._inputs_MRespRDYIn_From_Concat(True);
endrule

// All values are made zero(default)  since eof is asserted

rule rl_in5(reg_ref_clk == 5);
	wr_control <= MaintenanceRespIfcCntrl {mresp_sof:False, mresp_eof:True, mresp_vld:False};	
	wr_data <= MaintenanceRespIfcData {mresp_tt:0, mresp_data:0, mresp_crf:False, mresp_prio:0, mresp_ftype:0, mresp_dest_id:0, mresp_status:0, 			   			   mresp_tid:0,mresp_ttype:0,mresp_hop_count:0,mresp_local:False};
endrule


rule rl_output;
     	wr_ready <= maintenancerespsignals._MaintainRespIFC.mresp_rdy_n_ ();			// Output from Maintenance Response
     	wr_maintain_resp_sig <= maintenancerespsignals.outputs_MaintainRespIfcPkt_ ();		// Output from Maintenance Response
	wr_packet <= MaintenanceRespIfcPkt {mrespcntrl:wr_control, mrespdata:wr_data};		//Reference packet
endrule

rule rl_disp;
     $display("\n  Ready = %b , \n Reference packet =  %b \n  Maintenance Response Packet =     %b ",wr_ready,wr_packet,wr_maintain_resp_sig);
       
endrule

rule rl_compare;
	if(wr_packet == wr_maintain_resp_sig)
	$display("\n  Maintenance Response packet is equal to reference packet ");
	else
	$display("\n  Maintenance Response packet is NOT equal to reference packet ");	

endrule


endmodule

endpackage
