/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Test Bench For RapidIO_MaintenanceReqIFC

-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of both the RapidIO_IOPkt_concatenation module, RapidIO_IOPkt_Generation module, RapidIO_PktSeparation Module, Transport Parse Module, RapidIO_RxPktFTypeAnalyse and RapidIO_MaintenanceReqIFC
-- 1. Initiator Request packet is provided as input to concatenation module.
-- 2. Output SOF, EOF, Vld, Data (Header or Data), TxRem and Crf are obtained fron generation module.
-- 3. The output from generation module is given to Transport Parse module.
-- 4. The output from Transport Parse module is given to RapidIO_RxPktFTypeAnalyse.
-- 5. The output from RapidIO_RxPktFTypeAnalyse module is given to RapidIO_MaintenanceReqIFC Module.
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_Maintenance_Request;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_PktTransportParse::*;
import RapidIO_RxPktFTypeAnalyse::*;
import RapidIO_MaintenanceReqIFC::*;


module mkTb_Maintenance_Request(Empty);

// Interface

Ifc_RapidIO_IOPktConcatenation ifc_con <- mkRapidIO_IOPktConcatenation;
Ifc_RapidIO_IOPkt_Generation ifc_gen <- mkRapidIO_IOPkt_Generation;
Ifc_RapidIO_InComingPkt_Separation ifc_sep <- mkRapidIO_InComingPkt_Separation;
Ifc_RapidIO_PktTransportParse ifc_parse <- mkRapidIO_PktTransportParse;
Ifc_RapidIO_RxPktFTypeAnalyse ifc_analyse <- mkRapidIO_RxPktFTypeAnalyse;
Ifc_MaintenanceReq ifc_main_req <- mkRapidIO_MaintenanceReqIFC;

// For Concatenation Module
Wire#(InitReqIfcCntrl) wr_ireq_control <- mkDWire (defaultValue);
Wire#(InitReqIfcData) wr_ireq_data <- mkDWire (defaultValue);
Wire#(InitReqIfcMsg) wr_ireq_msg <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet_con <- mkDWire (defaultValue);

Wire#(MaintenanceRespIfcPkt) wr_main_packet_con <- mkDWire (defaultValue);

Wire#(FType8_MaintenanceClass) wr_main_FType8_MaintenanceClass <- mkDWire (defaultValue);


Wire#(InitReqDataInput) wr_data_count <- mkDWire (defaultValue); 
Wire#(Bool) wr_ready_concatenation <- mkDWire (False); 

// For Generation Module
Wire#(Bool) wr_gen_SOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_EOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_DSC_n <- mkDWire (False);
Wire#(Bool) wr_gen_VLD_n <- mkDWire (False);
Wire#(DataPkt) wr_gen_Data <- mkDWire (defaultValue); 
Wire#(Bit#(4)) wr_gen_TxRem <- mkDWire (0);
Wire#(Bool) wr_gen_Crf <- mkDWire (False); 
Wire#(Bool) wr_ready_from_dest <- mkDWire (False); 

// For Parse Module
Wire#(Maybe#(FType8_MaintenanceClass)) wr_parse_Ftype8_MaintenanceClass <- mkDWire(tagged Invalid);
Wire#(Maybe#(Data)) wr_parse_MaintenanceWrData <- mkDWire(tagged Invalid);

Wire#(ReceivedPktsInfo) wr_ReceivedPktsInfo <- mkDWire (defaultValue);
Wire#(TT) wr_tt <- mkDWire (0); 
Wire#(DestId) wr_Destid <- mkDWire (0); 
Wire#(SourceId) wr_Sourceid <- mkDWire (0);
Wire#(Prio) wr_Prio <- mkDWire (0);
Wire#(Bit#(4)) wr_max_Pkt_Count <- mkDWire (0);

// For Analyse Module
Wire#(Maybe#(MaintenanceReqIfcPkt)) wr_req_main_packet <- mkDWire (tagged Invalid);
Wire#(MaintenanceReqIfcPkt) wr_mreq <- mkDWire (defaultValue);
Wire#(Bool) wr_ready_from_analyse <- mkDWire (False); 

// For Maintenance Request Module
Wire#(Bool) wr_ready_from_mreq <- mkDWire (False); 

Wire#(Bool) wr_mreq_sof <- mkDWire(False);
Wire#(Bool) wr_mreq_eof <- mkDWire (False);
Wire#(Bool) wr_mreq_vld <- mkDWire (False);
Wire#(TT) wr_mreq_tt <- mkDWire (0); 
Wire#(Data) wr_mreq_data <- mkDWire (0);
Wire#(Bool) wr_mreq_crf <- mkDWire (False);
Wire#(Prio) wr_mreq_prio <- mkDWire (0);
Wire#(Type) wr_mreq_ftype <- mkDWire (0);
Wire#(DestId) wr_mreq_dest_id <- mkDWire (0);
Wire#(SourceId) wr_mreq_source_id <- mkDWire (0);
Wire#(TranId) wr_mreq_tid <- mkDWire (0);
Wire#(Type) wr_mreq_ttype <- mkDWire (0);
Wire#(ByteCount) wr_mreq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_mreq_byte_en <- mkDWire (0);
Wire#(Offset) wr_mreq_offset <- mkDWire (0);
Wire#(Bool) wr_mreq_local <-  mkDWire (False); 

// Clock Declaration 
Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);	
  
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n [  ------------------------------- CLOCK == %d -----------------------------------------  ]", reg_ref_clk);
	if (reg_ref_clk == 8)
	$finish (0);
endrule

// ------------------ Maintenance Response ---------------- Ftype 8 --------------------- //

// Response Class - Read Request

//  Control, Data and Msg Signals are provied to create reference packet 
rule rl_init_F8_Rd_req(reg_ref_clk == 0);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:8'h2f, ireq_tid:8'hbf,ireq_ttype:4'b0000, 						ireq_byte_count:'d4,ireq_byte_en:8'b11110000, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Rd_req(reg_ref_clk == 0);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Rd_req(reg_ref_clk == 0);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet);
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Rd_req(reg_ref_clk == 0);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Rd_req(reg_ref_clk == 1);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule
// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Rd_req(reg_ref_clk == 0);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ---------- Read Request ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b     					            			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_main_FType8_MaintenanceClass ,    			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Rd_req(reg_ref_clk == 1); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Rd_req(reg_ref_clk == 0);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Rd_req(reg_ref_clk == 1);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F8_Rd_req(reg_ref_clk == 1);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Rd_req(reg_ref_clk == 1);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b \n\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf,wr_ready_from_dest);     
endrule

// Input to Transport Parse Module
rule rl_input_parse_F8_Rd_req(reg_ref_clk == 1);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Transport Parse Module
rule rl_output_parse_F8_Rd_req(reg_ref_clk == 3); 
	wr_parse_Ftype8_MaintenanceClass <= ifc_parse.outputs_RxFtype8MainReqClass_ ();
	wr_parse_MaintenanceWrData <= ifc_parse.outputs_RxFtype8MaintainData_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport Parse module
rule rl_disp_parse_F8_Rd_req(reg_ref_clk == 3); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n data                = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	   			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_Ftype8_MaintenanceClass, 			wr_parse_MaintenanceWrData,wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F8_Rd_req(reg_ref_clk == 3); 
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse._inputs_RxFtype8MainReqClass (wr_parse_Ftype8_MaintenanceClass);
	ifc_analyse._inputs_RxFtype8MaintainData (wr_parse_MaintenanceWrData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_MReq (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F8_Rd_req(reg_ref_clk == 3);  
	wr_req_main_packet <=ifc_analyse.outputs_MaintainReqIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module 
rule rl_disp_analyse_F8_Rd_req(reg_ref_clk == 3); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_req_main_packet);
     if (wr_req_main_packet matches tagged Valid .data_mreq) begin
     wr_mreq <= data_mreq;  
     end  
endrule

//Input to Maintenance Request Module
rule rl_input_mreq_F8_Rd_req(reg_ref_clk == 3);
	ifc_main_req._inputs_MaintenanceReqIfcPkt (wr_mreq);
	ifc_main_req._MaintenanceReqIfc._mreq_rdy_n(False);
endrule

// Output from Maintenance Request Module
rule rl_output_mreq_F8_Rd_req(reg_ref_clk == 3 );
	wr_ready_from_mreq <=ifc_main_req.outputs_TxReady_From_MReq_ ();
	wr_mreq_sof <=ifc_main_req._MaintenanceReqIfc.mreq_sof_n_ ();
	wr_mreq_eof <=ifc_main_req._MaintenanceReqIfc.mreq_eof_n_ ();
	wr_mreq_vld <=ifc_main_req._MaintenanceReqIfc.mreq_vld_n_ ();
	wr_mreq_tt <=ifc_main_req._MaintenanceReqIfc.mreq_tt_ ();
	wr_mreq_data <=ifc_main_req._MaintenanceReqIfc.mreq_data_ ();
	wr_mreq_crf <=ifc_main_req._MaintenanceReqIfc.mreq_crf_ ();
	wr_mreq_prio <=ifc_main_req._MaintenanceReqIfc.mreq_prio_ ();
	wr_mreq_ftype <=ifc_main_req._MaintenanceReqIfc.mreq_ftype_ ();
	wr_mreq_dest_id <=ifc_main_req._MaintenanceReqIfc.mreq_dest_id_ ();
	wr_mreq_source_id <=ifc_main_req._MaintenanceReqIfc.mreq_source_id_ ();
	wr_mreq_tid <=ifc_main_req._MaintenanceReqIfc.mreq_tid_ ();
	wr_mreq_ttype <=ifc_main_req._MaintenanceReqIfc.mreq_ttype_ ();	
	wr_mreq_offset <=ifc_main_req._MaintenanceReqIfc.mreq_offset_ ();
	wr_mreq_byte_en <=ifc_main_req._MaintenanceReqIfc.mreq_byte_en_ ();
	wr_mreq_byte_count <=ifc_main_req._MaintenanceReqIfc.mreq_byte_count_ ();
	wr_mreq_local <=ifc_main_req._MaintenanceReqIfc.mreq_local_ ();
endrule

// To display output from Maintenance Request Module
rule rl_disp_mreq_F8_Rd_req(reg_ref_clk == 3 );
     $display(" \n # ------- Maintenance Request------------- #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b	\n offset		= %b \n byte_en		= %b \n byte_count		= %b \n Local		= %b 	    			\n Rdymreqmodule	= %b",wr_mreq_sof, wr_mreq_eof,wr_mreq_vld,wr_mreq_tt,wr_mreq_data, wr_mreq_crf, wr_mreq_prio,wr_mreq_ftype,wr_mreq_dest_id, 			wr_mreq_source_id,wr_mreq_tid,wr_mreq_ttype,wr_mreq_offset,wr_mreq_byte_en,wr_mreq_byte_count,wr_mreq_local, wr_ready_from_mreq);
endrule


// Response Class - Write Request

//  Control, Data and Msg Signals are provied to create reference packet 
rule rl_init_F8_Wr_req(reg_ref_clk == 4);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h7777777777777777, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:8'h77, ireq_tid:8'hbf,ireq_ttype:4'b0001, 						ireq_byte_count:'d8,ireq_byte_en:8'b11111111, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Wr_req(reg_ref_clk == 4);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Wr_req(reg_ref_clk == 4);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet);
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Wr_req(reg_ref_clk == 4);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Wr_req(reg_ref_clk == 5);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Wr_req(reg_ref_clk == 4);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ----------Write Request---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b  						    			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_main_FType8_MaintenanceClass,         			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Wr_req(reg_ref_clk == 5); 
     $display("   \n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Wr_req(reg_ref_clk == 4);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Wr_req(reg_ref_clk == 5);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule

// output from Generation Module 
rule rl_init_outputs_gen_F8_Wr_req(reg_ref_clk == 5 || reg_ref_clk == 6);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Wr_req(reg_ref_clk == 5 || reg_ref_clk == 6);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Transport Parse Module
rule rl_input_parse_F8_Wr_req(reg_ref_clk == 5 || reg_ref_clk == 6);
	ifc_parse._PktParseRx_SOF_n (wr_gen_SOF_n);
	ifc_parse._PktParseRx_EOF_n (wr_gen_EOF_n);
	ifc_parse._PktParseRx_VLD_n (wr_gen_VLD_n);
	ifc_parse._PktParseRx_data (wr_gen_Data);
	ifc_parse._PktParseRx_rem (wr_gen_TxRem);
	ifc_parse._PktParseRx_crf (wr_gen_Crf);
	ifc_parse._inputs_TxReadyIn_From_Analyze (False);
endrule

// Output from Transport Parse Module
rule rl_output_parse_F8_Wr_req(reg_ref_clk == 7 || reg_ref_clk == 8); 
	wr_parse_Ftype8_MaintenanceClass <= ifc_parse.outputs_RxFtype8MainReqClass_ ();
	wr_parse_MaintenanceWrData <= ifc_parse.outputs_RxFtype8MaintainData_ ();
	wr_ReceivedPktsInfo <= ifc_parse.outputs_ReceivedPkts_ ();
	wr_tt <= ifc_parse.outputs_TTReceived_ ();
	wr_Destid <= ifc_parse.outputs_RxDestId_ ();
	wr_Prio <= ifc_parse.outputs_RxPrioField_ ();
	wr_max_Pkt_Count <= ifc_parse.outputs_MaxPktCount_ ();
	wr_Sourceid <= ifc_parse.outputs_RxSourceId_ ();
endrule

// To display output from Transport Parse module
rule rl_disp_parse_F8_Wr_req(reg_ref_clk == 7 || reg_ref_clk == 8); 
     $display(" \n # -------Tarnsport Parse Module ------------- #");
     $display(" \n Ftype               = %b \n data                = %b \n Output Packet       = %b \n tt                  = %b \n Destid              = %b 	   			\n Sourceid            = %b	   			\n Prio                = %b \n Max Packet Count    = %b",wr_parse_Ftype8_MaintenanceClass, 			wr_parse_MaintenanceWrData,wr_ReceivedPktsInfo,wr_tt,wr_Destid,wr_Sourceid,wr_Prio,wr_max_Pkt_Count);     
endrule

//Input to Analyse Module
rule rl_input_analyse_F8_Wr_req(reg_ref_clk == 7 || reg_ref_clk == 8); 
	ifc_analyse._inputs_ReceivedPkts (wr_ReceivedPktsInfo);
	ifc_analyse._inputs_RxFtype8MainReqClass (wr_parse_Ftype8_MaintenanceClass);
	ifc_analyse._inputs_RxFtype8MaintainData (wr_parse_MaintenanceWrData);
	ifc_analyse._inputs_TTReceived (wr_tt);
	ifc_analyse._inputs_RxDestId(wr_Destid);
	ifc_analyse._inputs_RxSourceId (wr_Sourceid);
	ifc_analyse._inputs_RxPrioField (wr_Prio);
	ifc_analyse._inputs_MaxPktCount (wr_max_Pkt_Count);
	ifc_analyse._inputs_TxReady_From_MReq (False);
endrule

// Output from Analyse Module
rule rl_output_analyse_F8_Wr_req(reg_ref_clk == 8);  
	wr_req_main_packet <=ifc_analyse.outputs_MaintainReqIfcPkt_ ();
	wr_ready_from_analyse <= ifc_analyse.outputs_TxReadyOut_From_Analyze_ ();
endrule

// To display output from Analyse module 
rule rl_disp_analyse_F8_Wr_req(reg_ref_clk == 8); 
     $display(" \n # -------Analyse Module ------------- #");
     $display("\n Output Packet       = %b ",wr_req_main_packet);
     if (wr_req_main_packet matches tagged Valid .data_mreq) begin
     wr_mreq <= data_mreq;  
     end  
endrule

//Input to Maintenance Request Module
rule rl_input_mreq_F8_Wr_req(reg_ref_clk == 8);
	ifc_main_req._inputs_MaintenanceReqIfcPkt (wr_mreq);
	ifc_main_req._MaintenanceReqIfc._mreq_rdy_n(False);
endrule

// Output from Maintenance Request Module
rule rl_output_mreq_F8_Wr_req(reg_ref_clk == 8);
	wr_ready_from_mreq <=ifc_main_req.outputs_TxReady_From_MReq_ ();
	wr_mreq_sof <=ifc_main_req._MaintenanceReqIfc.mreq_sof_n_ ();
	wr_mreq_eof <=ifc_main_req._MaintenanceReqIfc.mreq_eof_n_ ();
	wr_mreq_vld <=ifc_main_req._MaintenanceReqIfc.mreq_vld_n_ ();
	wr_mreq_tt <=ifc_main_req._MaintenanceReqIfc.mreq_tt_ ();
	wr_mreq_data <=ifc_main_req._MaintenanceReqIfc.mreq_data_ ();
	wr_mreq_crf <=ifc_main_req._MaintenanceReqIfc.mreq_crf_ ();
	wr_mreq_prio <=ifc_main_req._MaintenanceReqIfc.mreq_prio_ ();
	wr_mreq_ftype <=ifc_main_req._MaintenanceReqIfc.mreq_ftype_ ();
	wr_mreq_dest_id <=ifc_main_req._MaintenanceReqIfc.mreq_dest_id_ ();
	wr_mreq_source_id <=ifc_main_req._MaintenanceReqIfc.mreq_source_id_ ();
	wr_mreq_tid <=ifc_main_req._MaintenanceReqIfc.mreq_tid_ ();
	wr_mreq_ttype <=ifc_main_req._MaintenanceReqIfc.mreq_ttype_ ();	
	wr_mreq_offset <=ifc_main_req._MaintenanceReqIfc.mreq_offset_ ();
	wr_mreq_byte_en <=ifc_main_req._MaintenanceReqIfc.mreq_byte_en_ ();
	wr_mreq_byte_count <=ifc_main_req._MaintenanceReqIfc.mreq_byte_count_ ();
	wr_mreq_local <=ifc_main_req._MaintenanceReqIfc.mreq_local_ ();
endrule

// To display output from Maintenance Request Module
rule rl_disp_mreq_F8_Wr_req(reg_ref_clk == 8);
     $display(" \n # ------ Maintenance Request ------------ #");
     $display("\n Sof		= %b \n Eof		= %b \n Vld		= %b \n TT		= %b \n Data		= %b  \n Crf		= %b 	    		        \n Prio		= %b \n Ftype		= %b \n DestId		= %b \n SoureceId		= %b \n TId 		= %b  			   			\n Ttype		= %b	\n offset		= %b \n byte_en		= %b \n byte_count		= %b \n Local		= %b 	    			\n Rdymreqmodule	= %b",wr_mreq_sof, wr_mreq_eof,wr_mreq_vld,wr_mreq_tt,wr_mreq_data, wr_mreq_crf, wr_mreq_prio,wr_mreq_ftype,wr_mreq_dest_id, 			wr_mreq_source_id,wr_mreq_tid,wr_mreq_ttype,wr_mreq_offset,wr_mreq_byte_en,wr_mreq_byte_count,wr_mreq_local, wr_ready_from_mreq);
endrule

endmodule
endpackage
