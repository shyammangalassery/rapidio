/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_B2B_RD_WR_TEST_SVH
`define RAPIDIO_B2B_RD_WR_TEST_SVH

class test_b2b_nread extends rapidio_b2b_base_test;

  // Sequence handle
  rapidio_nread_sequence   rio_nread_seq;
  rapidio_response_sequence       rio_response_seq; 
  `uvm_component_utils(test_b2b_nread)

  // FUNC : Constructor
  function new(string name = "test_b2b_nread", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  // FUNC : Build Phase
  virtual function void build_phase(uvm_phase phase);
    
    // Control the number of RMW loops
    // Define the initiator sequence to run in the run phase
    rio_nread_seq = rapidio_nread_sequence::type_id::create(); 
    rio_response_seq = rapidio_response_sequence::type_id::create(); 

    // Overriding base scoreboard with memory_scoreboard 


    super.build_phase(phase);
  endfunction : build_phase

  // TASK : Run Phase
  task run_phase(uvm_phase phase);
    super.run_phase(phase);    
    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("RAISING OBJECTION"), UVM_MEDIUM);
    uvm_test_done.raise_objection(this);

    fork
      rio_nread_seq.start(rapidio_env0.v_rio_sqncr);
      rio_response_seq.start(rapidio_env1.v_rio_sqncr);
    //  rio_nread_nwrite_seq.start(rapidio_env1.v_rio_sqncr);
    join

    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("DROPPING OBJECTION"), UVM_MEDIUM);
    #10 uvm_test_done.drop_objection(this);
  endtask : run_phase

endclass : test_b2b_nread


class test_b2b_nwrite extends rapidio_b2b_base_test;

  // Sequence handle
  rapidio_nwrite_sequence   rio_nwrite_seq;
  rapidio_response_sequence       rio_response_seq; 
  `uvm_component_utils(test_b2b_nwrite)

  // FUNC : Constructor
  function new(string name = "test_b2b_nwrite", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  // FUNC : Build Phase
  virtual function void build_phase(uvm_phase phase);
    
    // Control the number of RMW loops
    // Define the initiator sequence to run in the run phase
    rio_nwrite_seq = rapidio_nwrite_sequence::type_id::create(); 
    rio_response_seq = rapidio_response_sequence::type_id::create(); 

    // Overriding base scoreboard with memory_scoreboard 


    super.build_phase(phase);
  endfunction : build_phase

  // TASK : Run Phase
  task run_phase(uvm_phase phase);
    super.run_phase(phase);    
    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("RAISING OBJECTION"), UVM_MEDIUM);
    uvm_test_done.raise_objection(this);

    fork
      rio_nwrite_seq.start(rapidio_env0.v_rio_sqncr);
    join

    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("DROPPING OBJECTION"), UVM_MEDIUM);
    #10 uvm_test_done.drop_objection(this);
  endtask : run_phase

endclass : test_b2b_nwrite


class test_b2b_nwrite_resp extends rapidio_b2b_base_test;

  // Sequence handle
  rapidio_nwrite_r_sequence   rio_nwrite_seq;
  rapidio_response_sequence       rio_response_seq; 
  `uvm_component_utils(test_b2b_nwrite_resp)

  // FUNC : Constructor
  function new(string name = "test_b2b_nwrite_resp", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  // FUNC : Build Phase
  virtual function void build_phase(uvm_phase phase);
    
    // Control the number of RMW loops
    // Define the initiator sequence to run in the run phase
    rio_nwrite_seq = rapidio_nwrite_r_sequence::type_id::create(); 
    rio_response_seq = rapidio_response_sequence::type_id::create(); 

    // Overriding base scoreboard with memory_scoreboard 


    super.build_phase(phase);
  endfunction : build_phase

  // TASK : Run Phase
  task run_phase(uvm_phase phase);
    super.run_phase(phase);    
    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("RAISING OBJECTION"), UVM_MEDIUM);
    uvm_test_done.raise_objection(this);

    fork
      rio_nwrite_seq.start(rapidio_env0.v_rio_sqncr);
      rio_response_seq.start(rapidio_env1.v_rio_sqncr);
    join

    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("DROPPING OBJECTION"), UVM_MEDIUM);
    #10 uvm_test_done.drop_objection(this);
  endtask : run_phase

endclass : test_b2b_nwrite_resp


class test_b2b_nread_err_resp extends rapidio_b2b_base_test;

  // Sequence handle
  rapidio_nread_sequence   rio_nread_seq;
  rapidio_err_response_sequence       rio_err_response_seq; 
  `uvm_component_utils(test_b2b_nread_err_resp)

  // FUNC : Constructor
  function new(string name = "test_b2b_nread_err_resp", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  // FUNC : Build Phase
  virtual function void build_phase(uvm_phase phase);
    
    // Control the number of RMW loops
    // Define the initiator sequence to run in the run phase
    rio_nread_seq = rapidio_nread_sequence::type_id::create(); 
    rio_err_response_seq = rapidio_err_response_sequence::type_id::create(); 

    // Overriding base scoreboard with memory_scoreboard 


    super.build_phase(phase);
  endfunction : build_phase

  // TASK : Run Phase
  task run_phase(uvm_phase phase);
    super.run_phase(phase);    
    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("RAISING OBJECTION"), UVM_MEDIUM);
    uvm_test_done.raise_objection(this);

    fork
      rio_nread_seq.start(rapidio_env0.v_rio_sqncr);
      rio_err_response_seq.start(rapidio_env1.v_rio_sqncr);
    //  rio_nread_nwrite_seq.start(rapidio_env1.v_rio_sqncr);
    join

    `uvm_info("RAPIDIO_B2B_RD_WR_TEST_INFO", $sformatf("DROPPING OBJECTION"), UVM_MEDIUM);
    #10 uvm_test_done.drop_objection(this);
  endtask : run_phase

endclass : test_b2b_nread_err_resp


`endif
