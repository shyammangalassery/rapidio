/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- ------------------------------------------------------------------------------------------------------------------------------------------*/

`timescale 1ns/1ns

// To insert reset in middle. Further dependent on +define+RESET to be passed from CMD line
`define START_RESET     6255
`define END_RESET       100

import uvm_pkg::*;
`include "uvm_macros.svh"
`include "rapidio_interface.sv"  
`include "rapidio_pkg.sv"
import RAPIDIO_PKG::*;



// Top Module
module rapidio_tb_top;

// Include all the files //

 

`include "rapidio_b2b_base_test.sv"
`include "rapidio_b2b_rd_wr_test_lib.sv"


  parameter RIO_CLK_PERIOD =  10;  // 100 Mhz clk 
  parameter RESET_FOR   = 10; 
  parameter RIO_CLK_PHASE  =  0; 
  bit       rio_resetn, rio_clk;
  rapidio_interface rio_if();

  assign rio_if.rio_clk = rio_clk;
  assign rio_if.rio_resetn = rio_resetn;

  initial begin
    uvm_config_db#(virtual rapidio_interface)::set(uvm_root::get(), "*rapidio_env0.rio_ll_agent*", "vif", rio_if);
    uvm_config_db#(virtual rapidio_interface)::set(uvm_root::get(), "*rapidio_env0.rio_tl_agent*", "vif", rio_if);
    uvm_config_db#(virtual rapidio_interface)::set(uvm_root::get(), "*rapidio_env1.rio_ll_agent*", "vif", rio_if);
    uvm_config_db#(virtual rapidio_interface)::set(uvm_root::get(), "*rapidio_env1.rio_tl_agent*", "vif", rio_if);
     uvm_config_db#(virtual rapidio_interface)::set(uvm_root::get(), "*", "vif", rio_if);

    run_test();
  end


initial begin
    rio_resetn = 1'b0;
    rio_clk    = RIO_CLK_PHASE;
  end
  
  initial begin
    forever #(RIO_CLK_PERIOD/2) rio_clk = ~rio_clk ;
  end
  initial begin
    repeat(RESET_FOR) @(negedge rio_clk);
      rio_resetn = 1'b1;
  end



endmodule :rapidio_tb_top

