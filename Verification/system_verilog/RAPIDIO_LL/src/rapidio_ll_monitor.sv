/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_LL_MONITOR_SVH
`define RAPIDIO_LL_MONITOR_SVH

typedef class rapidio_ll_mon;

`uvm_blocking_peek_imp_decl(_response)
//`uvm_blocking_peek_imp_decl(_target)

// Callback class declaration for monitor 
class rapidio_ll_monitor_cbs extends uvm_callback;
  virtual function void trans_observed(rapidio_ll_mon xactor, rapidio_ll_sequence_item cycle);
  endfunction:trans_observed
endclass: rapidio_ll_monitor_cbs


// CLASS: rapidio_ll_mon
class rapidio_ll_mon extends uvm_monitor;

  // Virtual Interface declaration
 virtual interface         rapidio_interface  vif;


//SB ANALYSIS PORT ADDED HERE 
  uvm_analysis_port #(rapidio_ll_sequence_item)mon2sb_ll_port;
  uvm_analysis_port #(rapidio_ll_sequence_item)mon2sb_ll_dut_port;
  uvm_analysis_port #(rapidio_ll_sequence_item)mon2cov_ll_port;
  uvm_analysis_port #(rapidio_ll_sequence_item)mon_ll_ral_port;
  event packet2receive_ll_monitor;

  // Analysis & peek port declarations
  uvm_analysis_port#(rapidio_ll_sequence_item) cov_port;
  uvm_analysis_port#(rapidio_ll_sequence_item) sb_port;
  uvm_blocking_peek_imp_response#(rapidio_ll_sequence_item, rapidio_ll_mon) response_ph_imp_port;
  uvm_blocking_get_port #( byte unsigned) get_pkt_port;
  uvm_blocking_get_port #( int) get_cntrl_port;
  
//Variable declarations
  rapidio_ll_agent_config            rapidio_ll_cfg;

  protected rapidio_ll_sequence_item trans_collected;
  bit                            item_available = 0;    //Used to check if item is active
  bit                            initiator_enable;
  bit                            target_enable;



  // Monitor events declarations
  event rapidio_ll_trans_captured;

  // UVM automation macros
  `uvm_component_utils_begin(rapidio_ll_mon)
    `uvm_field_object(rapidio_ll_cfg, UVM_ALL_ON)
    `uvm_field_object(trans_collected, UVM_ALL_ON)
    `uvm_field_int(item_available,UVM_ALL_ON)
    `uvm_field_int(initiator_enable,UVM_ALL_ON)
    `uvm_field_int(target_enable,UVM_ALL_ON)
  `uvm_component_utils_end

  `uvm_register_cb(rapidio_ll_mon, rapidio_ll_monitor_cbs)

  extern function new (string name, uvm_component parent);
  extern function void build_phase(uvm_phase phase);
  extern virtual task run_phase(uvm_phase phase);   
  extern virtual task collect_transactions();
  extern virtual task check_resetn_assertion();

  // TASK : Peek task for Master
  task peek_response(output rapidio_ll_sequence_item trans);
    @this.rapidio_ll_trans_captured;
    trans = trans_collected;
    //`uvm_info("RAPIDIO_LL_MON_INFO",$sformatf("RECEIVED iPEEKPKT =%p\n",trans), UVM_LOW);
  endtask


endclass : rapidio_ll_mon

// FUNC : Constructor
function rapidio_ll_mon::new(string name, uvm_component parent);
  super.new(name, parent);
  cov_port = new("cov_port", this);
  mon2sb_ll_port=new("mon2sb_ll_port",this);
  mon2sb_ll_dut_port=new("mon2sb_ll_dut_port",this);
  mon2cov_ll_port=new("mon2cov_ll_port",this);
  mon_ll_ral_port=new("mon_ll_ral_port",this);
  response_ph_imp_port = new("response_ph_imp_port", this);
  trans_collected = new();
  get_pkt_port = new("get_pkt_port", this); 
  get_cntrl_port = new("get_cntrl_port", this); 
endfunction : new

// FUNC : Build Phase
function void rapidio_ll_mon::build_phase(uvm_phase phase);
  super.build_phase(phase);
  if(!uvm_config_db#(virtual rapidio_interface)::get(this, "", "vif", vif))
    `uvm_fatal("RAPIDIO_LL_MON_FATAL",{"VIRTUAL INTERFACE MUST BE SET FOR: ",get_full_name(),".vif"});
  void'(uvm_config_db#(rapidio_ll_agent_config)::get(this,"","rapidio_ll_agent_config",rapidio_ll_cfg));
endfunction : build_phase

// TASK : Run phase
task rapidio_ll_mon::run_phase(uvm_phase phase);
  super.run_phase(phase);
  `uvm_info("RAPIDIO_MON_INFO",$sformatf("RAPIDIO_MONITOR STARTED\n"),UVM_LOW)

  forever begin
   wait(vif.rio_resetn);
   //`uvm_info("RAPIDIO_MON_INFO",$sformatf("RAPIDIO_MONITOR iN FOREVER\n"),UVM_LOW)
    item_available = 1;
    fork
      collect_transactions();
    join_any
    disable fork;
  end
endtask : run_phase

// TASK : check_presetn_assertion - Check for presetn assertion while in run phase
task rapidio_ll_mon::check_resetn_assertion();
  wait(vif.rio_resetn !== 1'b1 || item_available === 1'b0);
  if(item_available) begin            item_available = 0;
    -> rapidio_ll_trans_captured;
 end
endtask : check_resetn_assertion
 
// TASK : collect_transactions - Collect transaction task
task rapidio_ll_mon::collect_transactions();

  int pkt_len;
  byte unsigned bytes[];
     get_cntrl_port.get(pkt_len);
     bytes =new[pkt_len];
  //`uvm_info("RAPIDIO_LL_MON_INFO",$sformatf("RECEIVED PKT WITH BYTE COUNT =%d\n",pkt_len), UVM_LOW);
  for(int i=0; i< pkt_len; i++) begin 
    get_pkt_port.get(bytes[i]);
  
trans_collected.dword_length=pkt_len;
    //`uvm_info("RAPIDIO_LL_MON_INFO",$sformatf("RECEIVED PKT WITH BYTE T =%h, i= %0d\n",bytes[i],i), UVM_LOW);
  ->packet2receive_ll_monitor;

  end 
  void'(trans_collected.unpack_bytes(bytes)); 
  `uvm_do_callbacks(rapidio_ll_mon,rapidio_ll_monitor_cbs,trans_observed(this,trans_collected))
  -> rapidio_ll_trans_captured;

  cov_port.write(trans_collected);
 mon2cov_ll_port.write(trans_collected);
  mon2sb_ll_port.write(trans_collected);
  mon2sb_ll_dut_port.write(trans_collected);
  mon_ll_ral_port.write(trans_collected);
  item_available  = 0 ;

endtask : collect_transactions
  
`endif
