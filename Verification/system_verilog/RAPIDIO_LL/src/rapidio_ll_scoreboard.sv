/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_SCOREBOARD_SVH
`define RAPIDIO_SCOREBOARD_SVH

//CLASS : rapidio_ll_scoreboard
class rapidio_ll_scoreboard extends uvm_scoreboard;

  // Config parameter handle declarations


  // Analysis port declarations
  uvm_analysis_imp #(rapidio_ll_sequence_item,rapidio_ll_scoreboard) sb_export;

  // UVM automation macros
  `uvm_component_utils_begin(rapidio_ll_scoreboard)
  `uvm_component_utils_end

  // FUNC : Constructor
  function new(string name, uvm_component parent);
    super.new(name, parent);
  endfunction : new

  // FUNC : Build Phase
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    sb_export = new("sb_export", this);
  endfunction : build_phase

  // FUNC : write
  virtual function void write(input rapidio_ll_sequence_item trans);
  endfunction : write

endclass : rapidio_ll_scoreboard

`endif
