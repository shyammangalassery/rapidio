/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_TL_SEQUENCE_ITEM_SVH
`define RAPIDIO_TL_SEQUENCE_ITEM_SVH

class rapidio_tl_sequence_item extends uvm_sequence_item;

// Rand declaration for TL fields
rand bit [1:0]   tt;
rand bit [31:0]  src_id;
rand bit [31:0]  dest_id;
rand bit [7:0]   hop_count;

// Dynamic array declaration for LL bytes
bit [7:0] ll_pkt_bytes [];
bit [7:0] tl_pkt_bytes [];
bit [7:0] tl_pkt_bytes_mon [];
int sz, resp_sz,pl_size, pl_sz;

//constraints
  constraint valid_tt {tt inside{0,1,2};}
  constraint valid_id {src_id != dest_id;}
  constraint valid_src_id {
  (tt == 0) -> src_id inside {[1:127]};
  (tt == 1) -> src_id inside {[1:32768]};
  }  
  constraint valid_dest_id 
  {
  (tt == 0) -> dest_id inside {[1:127]};
  (tt == 1) -> dest_id inside {[1:32768]};
  }  

// Automation macros
  `uvm_object_utils_begin(rapidio_tl_sequence_item)
    `uvm_field_array_int (ll_pkt_bytes, UVM_ALL_ON | UVM_NOPACK)
    `uvm_field_int (tt, UVM_ALL_ON | UVM_NOPACK)
    `uvm_field_int (src_id, UVM_ALL_ON | UVM_NOPACK)
    `uvm_field_int (dest_id, UVM_ALL_ON | UVM_NOPACK)
    `uvm_field_int (hop_count, UVM_ALL_ON | UVM_NOPACK)
  `uvm_object_utils_end

// FUNC : Constructor
  function new (string name = "rapidio_tl_sequence_item");
    super.new(name);
  endfunction : new

// Add do_pack method
// Packing for maintenance packets
  function void do_pack(uvm_packer packer);
    bit [5:0] bit_padding = 6'b000000;
    super.do_pack(packer);
    sz=ll_pkt_bytes.size;
    // For Maintenance packet
   // `uvm_info("RAPIDIO_TL_SEQ_ITEM_INFO",$sformatf("RAPIDIO_TL_SEQ_ITEM  tl_pkt_bytes = %d \n", ll_pkt_bytes[0][3:0]),UVM_LOW)
    if(ll_pkt_bytes[0][7:4] == 'b1000)
      begin
       // For maintenance request packets
       if((ll_pkt_bytes[0][3:0] == 4'h0) || (ll_pkt_bytes[0][3:0] == 4'h1) || (ll_pkt_bytes[0][3:0] == 4'b0100)) begin 
        packer.pack_field_int(tt, $bits(tt)); 
        packer.pack_field_int(ll_pkt_bytes[0][7:4], $bits(ll_pkt_bytes[0][7:4]));
       if(tt == 2'b00)
         begin
           packer.pack_field_int(dest_id[7:0], 8);
           packer.pack_field_int(src_id[7:0], 8); 
         end
       else if (tt == 2'b01)
         begin
           packer.pack_field_int(src_id[15:0], $bits(src_id[15:0])); 
           packer.pack_field_int(dest_id[15:0], $bits(dest_id[15:0]));
         end
       else
         begin
           packer.pack_field_int(src_id, $bits(src_id)); 
           packer.pack_field_int(dest_id, $bits(dest_id));
         end
        packer.pack_field_int(ll_pkt_bytes[0][3:0], $bits(ll_pkt_bytes[0][3:0]));
        packer.pack_field_int(ll_pkt_bytes[1][7:4], $bits(ll_pkt_bytes[1][7:4]));
        packer.pack_field_int(ll_pkt_bytes[1][3:0], $bits(ll_pkt_bytes[1][3:0]));
        packer.pack_field_int(ll_pkt_bytes[2][7:4], $bits(ll_pkt_bytes[2][7:4]));
        packer.pack_field_int(hop_count, $bits(hop_count)); 
        packer.pack_field_int(ll_pkt_bytes[2][3:0], $bits(ll_pkt_bytes[2][3:0]));
        for(int i = 3; i < sz; i++) begin
          packer.pack_field_int(ll_pkt_bytes[i], $bits(ll_pkt_bytes[i]));
        end
       end
      end

// Packing for non-maintenance packets
    else 
      begin 
       `uvm_info("RAPIDIO_TL_SEQ_ITEM_INFO",$sformatf("RAPIDIO_TL_SEQ_ITEM  info  \n"),UVM_LOW)
        packer.pack_field_int(tt, $bits(tt)); 
        packer.pack_field_int(ll_pkt_bytes[0][7:4], $bits(ll_pkt_bytes[0][7:4]));
       if(tt == 2'b00)
         begin
           packer.pack_field_int(dest_id[7:0], 8);
           packer.pack_field_int(src_id[7:0], 8); 
         end
       else if (tt == 2'b01)
         begin
           packer.pack_field_int(src_id[15:0], $bits(src_id[15:0])); 
           packer.pack_field_int(dest_id[15:0], $bits(dest_id[15:0]));
         end
       else
         begin
           packer.pack_field_int(src_id, $bits(src_id)); 
           packer.pack_field_int(dest_id, $bits(dest_id));
         end
        packer.pack_field_int(ll_pkt_bytes[0][3:0], $bits(ll_pkt_bytes[0][3:0]));
        for(int i = 1; i < sz; i++) begin
          packer.pack_field_int(ll_pkt_bytes[i], $bits(ll_pkt_bytes[i]));
        end  
      end 
        packer.pack_field_int(bit_padding, $bits(bit_padding));
  endfunction : do_pack
      

// Add do_unpack method
function  void do_unpack( uvm_packer packer);
  bit [1:0] bit_padding ;
  super.do_unpack(packer);
  ll_pkt_bytes.delete();
  if (tt == 2'b00) pl_sz = pl_size -3;
  else if (tt == 2'b01) pl_sz = pl_size -5;  
  else  pl_sz = pl_size -9;
  ll_pkt_bytes = new[pl_sz];
  
  tt = packer.unpack_field_int($bits(tt)); 
  ll_pkt_bytes[0][7:4] = packer.unpack_field_int(4);
  if(tt == 2'b00) begin 
    dest_id[7:0] = packer.unpack_field_int($bits(dest_id[7:0]));
    src_id[7:0] = packer.unpack_field_int($bits(src_id[7:0])); 
  end
  else if (tt == 2'b01) begin
    src_id[15:0] = packer.unpack_field_int($bits(src_id[15:0])); 
    dest_id[15:0] = packer.unpack_field_int($bits(dest_id[15:0]));
  end
  else begin 
    src_id = packer.unpack_field_int($bits(src_id)); 
    dest_id = packer.unpack_field_int($bits(dest_id));
  end
  ll_pkt_bytes[0][3:0] = packer.unpack_field_int(4);
  // For Maintenance Response
  if(ll_pkt_bytes[0][7:4] == 4'b1000)
    begin
      ll_pkt_bytes[1] = packer.unpack_field_int(8);
      ll_pkt_bytes[2][7:4] = packer.unpack_field_int(4);
      ll_pkt_bytes[2][3:0] = packer.unpack_field_int(4);
      foreach(ll_pkt_bytes[i]) 
        begin
          if (i>2) ll_pkt_bytes[i] = packer.unpack_field_int(8);
        end
    end 
  // For Non-Maintenance Response
  else
    begin
       foreach(ll_pkt_bytes[i]) 
         begin
           if ( i!= 0) ll_pkt_bytes[i] = packer.unpack_field_int(8);
         end
    end
endfunction : do_unpack
 
endclass: rapidio_tl_sequence_item

`endif
