/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

class rapidio_reg_block extends uvm_reg_block;

 // Register the object in the factory //
  `uvm_object_utils(rapidio_reg_block)

  //Declare the registers with rand keyword for randomly choosing //
  rand DEVICE_IDENTITY_CAR          				dev_iden_car;
  rand DEVICE_INFORMATION_CAR       				dev_info_car;
  rand ASSEMBLY_IDENTITY_CAR        				ass_iden_car;
  rand ASSEMBLY_INFORMATION_CAR       				ass_info_car;
  rand PROCESSING_ELEMENT_FEATURE  				prs_ele_ftr;
  rand SWITCH_PORT_INFORMATION      				swth_prt_info;
  rand SOURCE_OPERATIONS_CAR         				src_oprt_car;
  rand DESTINATION_OPERATIONS_CAR  				dest_oprt_car;
  rand PROCESSING_ELEMENT_LOGICAL_LAYER_CONTROL_CSR 		psll_csr;
  rand LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_0_CSR 		lcsba0_csr;
  rand LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_1_CSR 		lcsba1_csr;

  uvm_reg_map RIO_MAP;             // This is for creating block of map ;

   // Construct the object for the base class //
  function new(string name="rapidio_reg_block");
   super.new(name,UVM_NO_COVERAGE);
  endfunction 

  // Build the packets //
  virtual function void build();

   dev_iden_car	= DEVICE_IDENTITY_CAR::type_id::create("dev_iden_car");
   dev_iden_car.configure(this,null,"");
   dev_iden_car.build();

   
   dev_info_car	=  DEVICE_INFORMATION_CAR::type_id::create("dev_info_car");
   dev_info_car.configure(this,null,"");
   dev_info_car.build();

   ass_iden_car	= ASSEMBLY_IDENTITY_CAR ::type_id::create("ass_iden_car");
   ass_iden_car.configure(this,null,"");
   ass_iden_car.build();

   ass_info_car	=  ASSEMBLY_INFORMATION_CAR::type_id::create("ass_info_car");
   ass_info_car.configure(this,null,"");
   ass_info_car.build();

   prs_ele_ftr	=PROCESSING_ELEMENT_FEATURE ::type_id::create("prs_ele_ftr");
   prs_ele_ftr.configure(this,null,"");
   prs_ele_ftr.build();

   swth_prt_info = SWITCH_PORT_INFORMATION ::type_id::create("swth_prt_info");
   swth_prt_info.configure(this,null,"");
   swth_prt_info.build();

   src_oprt_car = SOURCE_OPERATIONS_CAR ::type_id::create("src_oprt_car");
   src_oprt_car.configure(this,null,"");
   src_oprt_car.build();
   
   dest_oprt_car = DESTINATION_OPERATIONS_CAR ::type_id::create("dest_oprt_car");
   dest_oprt_car.configure(this,null,"");
   dest_oprt_car.build();

   psll_csr =PROCESSING_ELEMENT_LOGICAL_LAYER_CONTROL_CSR::type_id::create("psll_csr");
   psll_csr.configure(this,null,"");
   psll_csr.build();

   lcsba0_csr =LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_0_CSR::type_id::create("lcsba0_csr");
   lcsba0_csr.configure(this,null,"");
   lcsba0_csr.build();

   lcsba1_csr = LOCAL_CONFIGURATION_SPACE_BASE_ADDRESS_1_CSR ::type_id::create("lcsba1_csr");
   lcsba1_csr.configure(this,null,"");
   lcsba1_csr.build();

   this.RIO_MAP=new();
   RIO_MAP = create_map("RIO_MAP",'h0,4,UVM_LITTLE_ENDIAN);

   RIO_MAP.add_reg(dev_iden_car,32'h00000000,"RW");
   RIO_MAP.add_reg(dev_info_car,32'h00000004,"RW");
   RIO_MAP.add_reg(ass_iden_car,32'h00000008,"RW");
   RIO_MAP.add_reg(ass_info_car,32'h0000000C,"RW");
   RIO_MAP.add_reg(prs_ele_ftr,32'h00000010,"RW");
   RIO_MAP.add_reg(swth_prt_info,32'h00000014,"RW");
   RIO_MAP.add_reg(src_oprt_car,32'h00000018,"RW");
  
   RIO_MAP.add_reg(dest_oprt_car,32'h00000018,"RW");
   RIO_MAP.add_reg(psll_csr,32'h0000004C,"RW");
   RIO_MAP.add_reg(lcsba0_csr,32'h00000058,"RW");
   RIO_MAP.add_reg(lcsba1_csr,32'h0000005C,"RW");
   
  lock_model();
 
  endfunction : build

  

endclass
   

   


 



   
