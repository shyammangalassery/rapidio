/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- ------------------------------------------------------------------------------------------------------------------------------------------*/

`uvm_analysis_imp_decl(_rcvd_ll_pkt)
`uvm_analysis_imp_decl(_sent_ll_pkt)
`uvm_analysis_imp_decl(_rcvd_ll_dut_pkt)
`uvm_analysis_imp_decl(_sent_ll_dut_pkt)
`uvm_analysis_imp_decl(_rcvd_tl_pkt)
`uvm_analysis_imp_decl(_sent_tl_pkt)
`uvm_analysis_imp_decl(_rcvd_tl_dut_pkt)
`uvm_analysis_imp_decl(_sent_tl_dut_pkt)


class rapidio_scoreboard extends uvm_scoreboard;


`uvm_component_utils(rapidio_scoreboard)


//Queue declaration:
rapidio_ll_sequence_item 		exp_ll_pkt_q[$];
rapidio_ll_sequence_item 		exp_ll_dut_pkt_q[$];
rapidio_tl_sequence_item 		exp_tl_pkt_q[$];
rapidio_tl_sequence_item 		exp_tl_dut_pkt_q[$];


uvm_analysis_imp_sent_ll_pkt#(rapidio_ll_sequence_item,rapidio_scoreboard)	dr2sb_ll_export;
uvm_analysis_imp_rcvd_ll_dut_pkt#(rapidio_ll_sequence_item,rapidio_scoreboard)	mon2sb_ll_dut_export;
uvm_analysis_imp_sent_tl_pkt#(rapidio_tl_sequence_item,rapidio_scoreboard)		dr2sb_tl_export;
uvm_analysis_imp_rcvd_tl_dut_pkt#(rapidio_tl_sequence_item,rapidio_scoreboard)	mon2sb_tl_dut_export;

uvm_analysis_imp_rcvd_ll_pkt#(rapidio_ll_sequence_item,rapidio_scoreboard)	mon2sb_ll_export;
uvm_analysis_imp_sent_ll_dut_pkt#(rapidio_ll_sequence_item,rapidio_scoreboard)	dr2sb_ll_dut_export;
uvm_analysis_imp_rcvd_tl_pkt#(rapidio_tl_sequence_item,rapidio_scoreboard)	        mon2sb_tl_export;
uvm_analysis_imp_sent_tl_dut_pkt#(rapidio_tl_sequence_item,rapidio_scoreboard)       dr2sb_tl_dut_export;




//CONSTRUCTOR:
function new(string name,uvm_component parent);
super.new(name,parent);

dr2sb_ll_export=new("dr2sb_ll_export",this);
dr2sb_tl_export=new("dr2sb_tl_export",this);

mon2sb_ll_export=new("mon2sb_ll_export",this);
mon2sb_tl_export=new("mon2sb_tl_export",this);

dr2sb_ll_dut_export=new("dr2sb_ll_dut_export",this);
dr2sb_tl_dut_export=new("dr2sb_tl_dut_export",this);


mon2sb_ll_dut_export=new("mon2sb_ll_dut_export",this);
mon2sb_tl_dut_export=new("mon2sb_tl_dut_export",this);

endfunction : new


//LOGIC LAYER  WRITE RECEIVED PACKET
virtual function void write_rcvd_ll_dut_pkt(input rapidio_ll_sequence_item ll_pkt);
rapidio_ll_sequence_item exp_ll_pkt;
ll_pkt.print();
//uvm_report_info(get_type_name(),$psprintf("\t LL PACKET SIZE =%d \n\n",exp_ll_pkt_q.size()),UVM_LOW);
if(exp_ll_pkt_q.size())
begin
  exp_ll_pkt=exp_ll_pkt_q.pop_front();
  exp_ll_pkt.print();
  if(ll_pkt.compare(exp_ll_pkt))
     uvm_report_info(get_type_name(),$psprintf("\t  LL PACKET FIELDS MATCH\n\n"),UVM_LOW);
  else
     uvm_report_error(get_type_name(),$psprintf("\t  LL PACKET FIELDS MISMATCH\n\n"),UVM_LOW);
end
else 
uvm_report_error(get_type_name(),$psprintf("\t No PACKETs FOR COMPARING \n\n"),UVM_LOW);
endfunction : write_rcvd_ll_dut_pkt


//LOGIC LAYER  WRITE SENT PACKET
virtual function void write_sent_ll_pkt(input rapidio_ll_sequence_item ll_pkt);
  exp_ll_pkt_q.push_back(ll_pkt);
 // uvm_report_info(get_type_name(),$psprintf("\t EXPETCED LL PACKET COUNT =%d \n\n",exp_ll_pkt_q.size()),UVM_LOW);
 // uvm_report_info(get_type_name(),$psprintf("\t EXPETCED LL PACKET  \n\n"),UVM_LOW);
 // ll_pkt.print(); 
endfunction : write_sent_ll_pkt

//LOGIC LAYER REPORT
virtual function void ll_report();
uvm_report_info(get_type_name(),$psprintf("\t SCOREBOARD REPORT\n\n",this.sprint()),UVM_LOW);
endfunction : ll_report






//LOGIC LAYER B2B  WRITE RECEIVED PACKET
virtual function void write_rcvd_ll_pkt(input rapidio_ll_sequence_item ll_pkt);
rapidio_ll_sequence_item exp_ll_dut_pkt;
ll_pkt.print();
if(exp_ll_dut_pkt_q.size())
begin
exp_ll_dut_pkt=exp_ll_dut_pkt_q.pop_front();
exp_ll_dut_pkt.print();
if(ll_pkt.compare(exp_ll_dut_pkt))
  uvm_report_info(get_type_name(),$psprintf("\t  LL PACKET FIELDS MATCH\n\n"),UVM_LOW);
else
  uvm_report_error(get_type_name(),$psprintf("\t  LL PACKET FIELDS MISMATCH\n\n"),UVM_LOW);
end
else 
uvm_report_error(get_type_name(),$psprintf("\t No PACKETs FOR COMPARING \n\n"),UVM_LOW);
endfunction : write_rcvd_ll_pkt

//LOGIC LAYER B2B  WRITE SENT PACKET
virtual function void write_sent_ll_dut_pkt(input rapidio_ll_sequence_item ll_dut_pkt);
  exp_ll_dut_pkt_q.push_back(ll_dut_pkt);
//  uvm_report_info(get_type_name(),$psprintf("\t EXPETCED LL PACKET  \n\n"),UVM_LOW);
//  ll_dut_pkt.print(); 
endfunction : write_sent_ll_dut_pkt

//LOGIC LAYER B2B REPORT
virtual function void ll_dut_report();
uvm_report_info(get_type_name(),$psprintf("\t SCOREBOARD REPORT\n\n",this.sprint()),UVM_LOW);
endfunction : ll_dut_report



//DATALINK LAYER  WRITE RECEIVED PACKET
virtual function void write_rcvd_tl_dut_pkt(input rapidio_tl_sequence_item tl_pkt);
rapidio_tl_sequence_item exp_tl_pkt;
tl_pkt.print();
if(exp_tl_pkt_q.size())
begin
  exp_tl_pkt=exp_tl_pkt_q.pop_front();
  exp_tl_pkt.print();
  if(tl_pkt.compare(exp_tl_pkt))
    uvm_report_info(get_type_name(),$psprintf("\t  TL PACKET FIELDS MATCH\n\n"),UVM_LOW);
  else
    uvm_report_error(get_type_name(),$psprintf("\t  TL PACKET FIELDS MISMATCH\n\n"),UVM_LOW);
  end
else 
uvm_report_error(get_type_name(),$psprintf("\t No PACKETs FOR COMPARING \n\n"),UVM_LOW);
endfunction : write_rcvd_tl_dut_pkt

//DATALINK LAYER  WRITE SENT PACKET
virtual function void write_sent_tl_pkt(input rapidio_tl_sequence_item tl_pkt);
  exp_tl_pkt_q.push_back(tl_pkt);
endfunction : write_sent_tl_pkt

//DATALINK LAYER REPORT
virtual function void tl_report();
uvm_report_info(get_type_name(),$psprintf("\t SCOREBOARD REPORT\n\n",this.sprint()),UVM_LOW);
endfunction : tl_report


//DATALINK LAYER B2B WRITE RECEIVED PACKET
virtual function void write_rcvd_tl_pkt(input rapidio_tl_sequence_item tl_dut_pkt);
rapidio_tl_sequence_item exp_tl_dut_pkt;
tl_dut_pkt.print();
if(exp_tl_dut_pkt_q.size())
begin
  exp_tl_dut_pkt=exp_tl_dut_pkt_q.pop_front();
  tl_dut_pkt.print();
  if(exp_tl_dut_pkt.compare(tl_dut_pkt))
    uvm_report_info(get_type_name(),$psprintf("\t  TL PACKET FIELDS MATCH\n\n"),UVM_LOW);
  else
    uvm_report_error(get_type_name(),$psprintf("\t  TL PACKET FIELDS MISMATCH\n\n"),UVM_LOW);
  end
else 
uvm_report_error(get_type_name(),$psprintf("\t No PACKETs FOR COMPARING \n\n"),UVM_LOW);
endfunction : write_rcvd_tl_pkt

//DATALINK LAYER B2B WRITE SENT PACKET
virtual function void write_sent_tl_dut_pkt(input rapidio_tl_sequence_item tl_dut_pkt);
exp_tl_dut_pkt_q.push_back(tl_dut_pkt);
endfunction : write_sent_tl_dut_pkt

//DATALINK LAYER B2B REPORT
virtual function void tl_dut_report();
uvm_report_info(get_type_name(),$psprintf("\t SCOREBOARD REPORT\n\n",this.sprint()),UVM_LOW);
endfunction : tl_dut_report

endclass : rapidio_scoreboard

