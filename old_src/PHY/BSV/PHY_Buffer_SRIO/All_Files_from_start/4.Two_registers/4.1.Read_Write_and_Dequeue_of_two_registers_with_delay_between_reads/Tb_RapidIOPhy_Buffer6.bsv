package Tb_RapidIOPhy_Buffer6;

import RapidIO_DTypes ::*;
import RapidIOPhy_Buffer6 ::*;

(*synthesize*)
(*always_enabled*)
(*always_ready*)

module mkTb_RapidIOPhy_Buffer6(Empty);

Ifc_RapidIOPhy_Buffer6 bfr6 <- mkRapidIOPhy_Buffer6;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 55 )
		$finish (0);
endrule

rule r10(reg_ref_clk == 0);
bfr6._tx_rg12(2'b10);
endrule

rule r1_1(reg_ref_clk == 1);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hF52AD3E66EFC11375A8D9C8EFC0ACEA0);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_2(reg_ref_clk <= 17);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3(reg_ref_clk == 18);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b1111);
bfr6._tx_crf(2'b11);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_1(reg_ref_clk == 19);
//bfr6._tx_vld_n(False);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_11(reg_ref_clk == 20);
//bfr6._tx_vld_n(False);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_2(reg_ref_clk == 22);
bfr6._tx_read(1'b1);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_3(reg_ref_clk >= 23 && reg_ref_clk<=39);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_4(reg_ref_clk == 40);
bfr6._tx_read(1'b0);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_5(reg_ref_clk == 41);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
endrule

rule r1_3_6(reg_ref_clk == 42);
$display("sof is %b",bfr6.lnk_tsof_n_());
$display("vld is %b",bfr6.lnk_tvld_n_());
$display("eof is %b",bfr6.lnk_teof_n_());
$display("DATA is %h",bfr6.lnk_td_());
$display("Rem is %b",bfr6.lnk_trem_());
$display("CRF is %b",bfr6.lnk_tcrf_());
bfr6._tx_deq(1'b1);
endrule

rule r1_3_7(reg_ref_clk == 43);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_3_8(reg_ref_clk == 44);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_4_8(reg_ref_clk == 45);
bfr6._tx_deq(1'b0);
endrule

rule r11(reg_ref_clk == 46);
bfr6._tx_rg12(2'b11);
endrule

rule r1_1_1(reg_ref_clk == 47);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hF62AD3E66EFC11376A8D9C8EFC0ACEA0);
bfr6._tx_rem(4'b1111);
bfr6._tx_crf(2'b11);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_1_2(reg_ref_clk == 48);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r1_1_21(reg_ref_clk == 49);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_1(reg_ref_clk == 50);
bfr6._tx_sof_n(False);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h11375A8D9C8EFC0ACEA0F52AD3E66EFC);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_2(reg_ref_clk == 51);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(True);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'hD3E66EFC11375A8D9C8EFC0ACEA0F52A);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

//rule r2_4_8(reg_ref_clk == 50);
//$display("Regstr value is %h",bfr6.buf_out_());
//endrule

rule r2_3(reg_ref_clk == 52);
bfr6._tx_sof_n(True);
bfr6._tx_eof_n(False);
bfr6._tx_vld_n(False);
bfr6._tx_data(128'h6EFC11376A8D9C8EFC0ACEA0F62AD3E6);
bfr6._tx_rem(4'b0111);
bfr6._tx_crf(2'b00);
$display("Regstr value is %h",bfr6.buf_out_());
endrule

rule r2_3_81(reg_ref_clk == 53);
$display("Regstr value is %h",bfr6.buf_out_());
endrule 

rule r2_3_82(reg_ref_clk == 54);
$display("Regstr value is %h",bfr6.buf_out_());
endrule 

rule r2_3_83(reg_ref_clk == 55);
$display("Regstr value is %h",bfr6.buf_out_());
endrule 
endmodule:mkTb_RapidIOPhy_Buffer6
endpackage:Tb_RapidIOPhy_Buffer6
