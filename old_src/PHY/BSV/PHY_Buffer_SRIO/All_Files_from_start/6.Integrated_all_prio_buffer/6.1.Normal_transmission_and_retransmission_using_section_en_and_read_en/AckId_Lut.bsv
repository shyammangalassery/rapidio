package AckId_Lut;

import Vector::*;


interface Ifc_AckId_Lut;
method Action _identify(Bit#(1) value);
method Action _store(Bit#(1) value);
method Action _clear(Bit#(1) value);
method Action _ackid_store(Bit#(6) value);
method Action _ackid_retrans(Bit#(6) value);
method Action _ackid_valid(Bit#(6) value);
method Action _ackid_clear(Bit#(6) value);
method Action _prio_in(Bit#(2) value);
method Action _rd_ptr_in(Bit#(4) value);


method Bit#(2) prio_out_();
method Bit#(2) prio_out_valid_();
method Bit#(1) valid_out_();
method Bit#(4) read_ptr_out_();
//method Bit#(6) ackid_out_();
endinterface:Ifc_AckId_Lut

(* synthesize *)
//(* always_enabled *)
//(* always_ready *)

module mkAckId_Lut(Ifc_AckId_Lut);


//[1-bit valid bit(whether there is valid entry or not),6-bit ackid,2-bit prio,4-bit pointr postn]
Vector#(64,Reg#(Bit#(13)))  rg_vect_lut <- replicateM(mkReg(0));

//ackid to be stored while reading
Wire#(Bit#(6)) wr_ack_id_store <- mkDWire(0);
//ackid to be retransmitted
Wire#(Bit#(6)) wr_ack_id_retrans <- mkDWire(0);
//to reload read pointer if there is valid entry
Wire#(Bit#(6)) wr_ack_id_valid <- mkDWire(0);
Wire#(Bit#(6)) wr_ack_id_clear <- mkDWire(0);
Wire#(Bit#(4)) wr_rd_ptr_in <- mkDWire(0);
Wire#(Bit#(2)) wr_prio <- mkDWire(0);
//to indicate whether updation or reading of lut is required
Wire#(Bit#(1)) id <- mkDWire(0);
Wire#(Bit#(1)) wr_store <- mkDWire(0);
Wire#(Bit#(1)) wr_clear <- mkDWire(0);

Reg#(Bit#(4)) rg_read_ptr_out <- mkReg(0);
Reg#(Bit#(2)) rg_prio_out <- mkReg(0);
Reg#(Bit#(2)) rg_prio_out_valid <- mkReg(0);
Reg#(Bit#(6)) rg_ack_id_retrans <- mkReg(0);
Reg#(Bit#(1)) rg_valid <- mkReg(0);




rule store(wr_store == 1);
let lv_id = wr_ack_id_store;
//vector of registers indexed by ackid
rg_vect_lut[lv_id] <= {1'b1, (wr_ack_id_store == 6'b0 ? 6'b111111 :(wr_ack_id_store - 6'b000001)), wr_prio, wr_rd_ptr_in};
$display("LUT entry:Valid bit - 1, Ackid - %b, Priority - %b, Pointer - %b",wr_ack_id_store, wr_prio, wr_rd_ptr_in);
endrule

rule clear_lut(wr_clear == 1);//put some other input like wr_clear for identifying its a clear operation and at the same time retreiving position is also possible  
let lv_id = wr_ack_id_clear;
rg_vect_lut[lv_id] <= {13'b0};
endrule

//reading from lut
rule out_retrans(id == 1);
let lv_id = wr_ack_id_retrans;
rg_prio_out <= rg_vect_lut[lv_id][5:4];
//rg_ack_id_retrans <= wr_ack_id_retrans;

endrule

rule valid_check;
let lv_id = wr_ack_id_valid;
rg_valid <= rg_vect_lut[lv_id][12];
rg_read_ptr_out <= rg_vect_lut[lv_id][3:0];
rg_prio_out_valid <= rg_vect_lut[lv_id][5:4];
endrule

//ackid to be stored
method Action _ackid_store(Bit#(6) value);
       wr_ack_id_store <= value;
endmethod

//ackid to be identified from lut for retransmission
method Action _ackid_retrans(Bit#(6) value);
       wr_ack_id_retrans <= value;
endmethod

method Action _ackid_valid(Bit#(6) value);
	wr_ack_id_valid <= value;
endmethod

method Action _ackid_clear(Bit#(6) value);
	wr_ack_id_clear <= value;
endmethod

method Action _rd_ptr_in(Bit#(4) value);
       wr_rd_ptr_in <= value;
endmethod

method Action _prio_in(Bit#(2) value);
	wr_prio <= value;
endmethod
	
method Action _identify(Bit#(1) value);
       id <= value;
endmethod

method Action _store(Bit#(1) value);
       wr_store <= value;
endmethod

method Action _clear(Bit#(1) value);
	wr_clear <= value;
endmethod

method Bit#(4) read_ptr_out_();
       return rg_read_ptr_out;
endmethod

method Bit#(2) prio_out_();
	return rg_prio_out;
endmethod

method Bit#(2) prio_out_valid_();
	return rg_prio_out_valid;
endmethod
/*
method Bit#(6) ackid_out_();
	return rg_ack_id_retrans;
endmethod
*/
method Bit#(1) valid_out_();
	return rg_valid;
endmethod


endmodule:mkAckId_Lut
endpackage:AckId_Lut
