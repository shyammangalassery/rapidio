Integrated transmit buffer with ackid generator,lut,4 sections of buffers etc. Txmtng of first cycle and ackid assignment happens in same cycle using CReg(both writing and reading possible in same cycle). Thus continuous transmission of data takes place without any delay. Thus early enabling of the next section to be read is not required now. 

Thus writing to the buffer according to priority and normal transmission according to priority after assigning ackid and writing of the same to the lut(ackid-prio-pointer(location)) is implemented.

Works pending - retransmission and dequeuing.
