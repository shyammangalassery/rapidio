/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Single register Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1.This module is developed for the Physical layer 
-- 2.A Single Register of 2310 bits is created to hold the incoming packet.
-- 3.This module behaves the base for the set of each buffers(group of registers)
-- 4.In this module, both read and write is carried out by position shifting using for loop for bit by bit updation.  
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIOPhy_Buffer2;

import RapidIO_DTypes ::*;
import DefaultValue ::*;

interface Ifc_RapidIOPhy_Buffer2;
	method Action _tx_sof_n(Bool value);
	method Action _tx_eof_n(Bool value);
	method Action _tx_vld_n(Bool value);
	method Action _tx_data(DataPkt value);
	method Action _tx_rem(Bit#(4) value);
	method Action _tx_crf_n(Bit#(2) value);
	method Action _tx_deq(Bit#(1) value);
	method Action _tx_read(Bit#(1) value);
	method Action _tx_retransmsn(Bit#(1) value);//enables retransmission if equal to 1,i.e.,when RFR or a link request is sent by transmitter
	method Action _tx_ack(Bit#(6) value);
   
	method Bool lnk_tsof_n_();
	method Bool lnk_teof_n_();
	method DataPkt lnk_td_();
	method Bool lnk_tvld_n_();
	method Bit#(4) lnk_trem_();
	method Bit#(2) lnk_tcrf_();
	method Bit#(1) read_eof_();
	method Bit#(1) valid_entry_();//indicates whether there is valid data
	method BufferData buf_out_();//displays contents of register
endinterface:Ifc_RapidIOPhy_Buffer2


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer2(Ifc_RapidIOPhy_Buffer2);

Wire#(Bool) tx_sof_n <- mkDWire(True);
Wire#(Bool) tx_eof_n <- mkDWire(True);
Wire#(Bool) tx_vld_n <- mkDWire(True);
Wire#(DataPkt) tx_data <- mkDWire(0);
Wire#(Bit#(4)) tx_rem <- mkDWire(0);
Wire#(Bit#(2)) tx_crf <- mkDWire(0);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4)) wr_lnk_trem <- mkDWire(0);
Wire#(Bit#(2)) wr_lnk_tcrf <- mkDWire(0);
Wire#(Bit#(1)) wr_eof <- mkDWire(0);
Wire#(Bit#(6)) wr_tx_ack <- mkDWire(0);                
Wire#(Bit#(1)) wr_read_en <- mkDWire(0);
Wire#(Bit#(1)) wr_deq <- mkDWire(0);                 
Wire#(Bit#(1)) wr_retrans <- mkDWire(0);                   

Reg#(BufferData) rg_buf[2] <- mkCReg(2,defaultValue);//basic register to which data is written.total register size = 2310 bits.CReg is used, so that both read and write of ackid in the register is possible in one cycle itself.
Reg#(Bit#(5)) rg_cnt_read <- mkReg(0);//read counter in a single register.
Reg#(Bit#(5)) rg_cnt_write <- mkReg(0);//write counter indicating the position of each cycle of data in the register. 
Reg#(Bit#(5)) rg_cnt_wr_retrnsmsn <- mkReg(0);//to update write count register to initial value in case of retransmission.
Reg#(Bit#(12)) rg_pos <- mkReg(0);//for storing position count value
Reg#(Bit#(12)) rg_pos_read <- mkReg(0);//for storing position count value for reading


//writing into a register              
rule r1_write(tx_vld_n == False || wr_deq == 1);                            
  
 	if(wr_deq == 1)
		rg_buf[2311:0] <= 2312'b0;
	else if(tx_sof_n == False && tx_eof_n == False)
		begin            
		rg_buf[2311:0] <= {1'b1,tx_crf,tx_rem,1'b1,2176'b0,tx_data};  //only one cycle(128 bits) of data; [2311:2304] stores eof crf rem and sof in 8 bits
		rg_cnt_wr <= rg_cnt_wr +1;
		//rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 1;		
		rg_cnt_rd <= 0; 
		end

	else if(tx_sof_n == False)
		begin            
		rg_buf[127:0] <= tx_data;                                      //first cycle always goes to position [127:0]
		rg_cnt_wr <= rg_cnt_wr + 4;             //pointer incremented by four in the first cycle in case of multiple cycles of data.
		rg_pos <= rg_pos + 128;
		//rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 4;		
		end

	else if(tx_eof_n == False)
		begin


		let lv_rg_intrmediate = rg_buf[2175:128];
		rg_buf[(2311):(128)] <= {1'b1,tx_crf,tx_rem,1'b1,tx_data,lv_rg_intrmediate};     //last cycle always goes to position [2303:2176]; [2311:2304] stores eof crf rem and sof in 8 bits
		rg_cnt_rd <= 0; 
		end 

	else 
		begin

		let lv_pos = rg_pos;
		Bit#(2312) lv_rg_buf = rg_buf;
		for(Integer i=0; i<128; i=i+1)
			begin
			let lv_poss = lv_pos + fromInteger(i);
			lv_rg_buf[lv_poss] = tx_data[i];
			end
		rg_pos <= lv_pos + 128;
		rg_buf <= lv_rg_buf;
		rg_cnt_wr <= rg_cnt_wr +1;
		//rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 1;		
		end

      

endrule

/*
rule retxmsn(wr_retrans == 1);
	rg_cnt_wr <= rg_cnt_wr_retrnsmsn;
	rg_cnt_rd <= 0;
endrule
*/

rule r1_read(rg_read == 1);                 //reading enabled by rg_read
			
                                            //pointer = 1 implies single cycle of data
	if(rg_cnt_wr == 1) 
		begin
		if(rg_cnt_rd == 0)
			begin
			rg_buf[5:0] <= wr_tx_ack;        //ackid assignment
			//$display("ackid in basic =%b",wr_tx_ack);
			$display("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
			rg_cnt_rd <= rg_cnt_rd + 1;
			end
		else if(rg_cnt_rd == 1)
			begin
			wr_lnk_tsof_n <= False;
			wr_lnk_teof_n <= False;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[127:0];
			wr_lnk_trem <= rg_buf[2308:2305];
			wr_lnk_tcrf <= rg_buf[2310:2309];
			wr_eof <= 1'b1;                 //to facilitate read pointer incrementing
			rg_cnt_rd <= 0;
			rg_cnt_wr <= 0;
			end
		end


	else if(rg_cnt_wr != 0 && rg_cnt_wr != 1 && rg_cnt_wr != 2)

		begin
		if(rg_cnt_rd == 0)
			begin
			rg_buf[5:0] <= wr_tx_ack;         //ackid assignment
			$display("ackid in basic =%b",wr_tx_ack);
			$display("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			rg_cnt_wr <= rg_cnt_wr-1;
			rg_cnt_rd <= rg_cnt_rd + 1;
			end



		else if(rg_cnt_rd == 1)
			begin
			//$display("data1..........................");
			wr_lnk_tsof_n <= False;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[127:0];
			rg_pos_read <= rg_pos_read + 128;
			rg_cnt_wr <= rg_cnt_wr-1;
			rg_cnt_rd <= rg_cnt_rd + 1;
			//wr_lnk_trem <= rg_buf[2308:2305];
			//wr_lnk_tcrf <= rg_buf[2310:2309];
			//wr_eof <= 1'b1;
			end

		else
			begin
			//$display("rg_cnt_wr and rg_cnt_rd = %b,%b",rg_cnt_wr,rg_cnt_rd);
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			Bit#(128) lv_lnk_data = 0;
			let lv_pos = rg_pos_read;
			Bit#(2312) lv_rg_buf = rg_buf;
			for(Integer i=0; i<128; i=i+1)
				begin
				let lv_poss = lv_pos + fromInteger(i);
				lv_lnk_data[i] = lv_rg_buf[lv_poss];
				end
			wr_lnk_td <= lv_lnk_data;
			rg_pos_read <= lv_pos + 128;
			rg_cnt_wr <= rg_cnt_wr - 1;
			rg_cnt_rd <= rg_cnt_rd + 1;
			end
		end
		else if(rg_cnt_wr == 2 && rg_cnt_rd != 0)
			begin
			wr_lnk_teof_n <= False;
			wr_lnk_tsof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buf[2303:2176];
			wr_eof <= 1'b1;                                //facilitating read pointer incrementing
			wr_lnk_trem <= rg_buf[2308:2305];
			wr_lnk_tcrf <= rg_buf[2310:2309];
			rg_cnt_rd <= 0;
			end

		

		




		

		


	
endrule



method Action _tx_ack(Bit#(6) value);
	wr_tx_ack <= value;
endmethod

method Action _tx_sof_n(Bool value);
     tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     tx_eof_n <= value;
endmethod

method Action _tx_crf_n(Bit#(2) value);
     tx_crf <= value;
endmethod

method Action _tx_vld_n(Bool value);
     tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     tx_rem <= value;
endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod
/*
method Action _tx_retransmsn(Bit#(1) value);
	wr_retrans <= value;
endmethod
*/


method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return wr_lnk_trem;
endmethod

method Bit#(2) lnk_tcrf_();
      return wr_lnk_tcrf;
endmethod

method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method Bit#(1) read_eof_();
	return wr_eof;
endmethod

method RegBuf buf_out_();
      return rg_buf;
endmethod

endmodule:mkRapidIOPhy_Buffer2
endpackage:RapidIOPhy_Buffer2
