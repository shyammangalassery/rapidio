/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- 1. It is used to test the generated CRC-24 code.
-- 2. The control symbol packet (38 bits) plus the CRC 24 code(24 bits) is used as input data. 
-- 3. Initially CRC-24 check sum value is assigned to all 1's. 
-- 4. This is connected with the CRC24 checker module. 
--
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_PhyCRC24Checker;

import RapidIO_PhyCRC24Checker::*;

(*synthesize*)
(*always_enabled*)
(*always_ready*)


module mkTb_RapidIO_PhyCRC24Checker(Empty);

Ifc_RapidIO_PhyCRC24Checker crc24_check <- mkRapidIO_PhyCRC24Checker;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);
Reg#(Bit#(24)) out_check <- mkReg (0);

/*
-- Following rule, it is used to generate reference clock 
*/
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 10)
		$finish (0);
endrule

rule r2(reg_ref_clk ==1); 
crc24_check._inputs_ControlSymbolDatacrc_in (64'b1110101010111010000101101001110111000000001000001100110000110111);//data+code+2bits for alignment present in packet format
crc24_check._inputs_rx_vld (False);
$display("Input 1== 38'b11101010101110100001011010011101110000");
$display("CRC Code == 24'b000010000011001100001101");
endrule:r2

rule r3(reg_ref_clk == 2); 
crc24_check._inputs_ControlSymbolDatacrc_in (64'b0111010101011101000010110100111011100010111011000010001010001011);
crc24_check._inputs_rx_vld (False);
$display("Input 2== 38'b01110101010111010000101101001110111000");
$display("CRC Code == 24'b101110110000100010100010");
endrule:r3

rule r3_1(reg_ref_clk == 3); 
crc24_check._inputs_ControlSymbolDatacrc_in (64'b0011101010101110100001011010011101110001001100001000111001000011);
crc24_check._inputs_rx_vld (False);
$display("Input 3== 38'b00111010101011101000010110100111011100");
$display("CRC Code == 24'b010011000010001110010000");
endrule:r3_1

rule r3_2(reg_ref_clk == 4); 
crc24_check._inputs_ControlSymbolDatacrc_in (64'b0001110101010111010000101101001110111000110111101101100000100111);
crc24_check._inputs_rx_vld (False);
$display("Input 4== 38'b00011101010101110100001011010011101110");
$display("CRC Code == 24'b001101111011011000001001");
endrule:r3_2

rule r3_3(reg_ref_clk == 5); 
crc24_check._inputs_ControlSymbolDatacrc_in (64'b1000111010101011101000010110100111011110100100110010100010000011);//wrong input therefore crc24 not equal to zero
crc24_check._inputs_rx_vld (False);
$display("Input 5== 38'b00001110101010111010000101101001110111");
$display("CRC Code == 24'b101001001100101000100000");
endrule:r3_3

rule r1_1;
$display("CRC Check=%b",crc24_check.outputchk_CRC24_ ());
endrule:r1_1

endmodule:mkTb_RapidIO_PhyCRC24Checker
endpackage:Tb_RapidIO_PhyCRC24Checker
