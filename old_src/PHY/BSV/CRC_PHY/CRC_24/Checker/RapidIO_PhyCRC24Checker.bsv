/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- 1. It is used to check the generated CRC-24 code.
-- 2. Design is implemented as mentioned in the RapidIO specification 3.1. 
-- 3. The Control symbol packet format 64 require to generate CRC24.
-- 4. The control symbol packet (38 bits) plus the CRC 24 code(24 bits) is used as input data. 
-- 5. Initially CRC-24 check sum value is assigned to all 1's. 
-- 6. Function is developed to perform the CRC-24 checking.
--
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_PhyCRC24Checker;

// The Endian format is changed from Big Endian to little Endian

function Bit#(24) fn_CRC24Checker (Bit#(62) datain);

//Equation network to perform CRC24 checking

    Bit#(1) cksum0 = datain[0] ^ datain[2] ^ datain[5] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[13] ^ datain[14] ^ datain[21] ^ datain[23] ^ datain[24] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[34] ^ datain[36] ^ datain[38] ^ datain[41] ^ datain[43] ^ datain[48] ^ datain[52] ^ datain[55];

    Bit#(1) cksum1 = datain[0] ^ datain[1] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[9] ^ datain[12] ^ datain[13] ^ datain[15] ^ datain[21] ^ datain[22] ^ datain[23] ^ datain[25] ^ datain[26] ^ datain[30] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37] ^ datain[38] ^ datain[39] ^ datain[41] ^ datain[42] ^ datain[43] ^ datain[44] ^ datain[48] ^ datain[49] ^ datain[52] ^ datain[53] ^ datain[55] ^ datain[56];

    Bit#(1) cksum2 = ~datain[1] ^ datain[2] ^ datain[3] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[10] ^ datain[13] ^ datain[14] ^ datain[16] ^ datain[22] ^ datain[23] ^ datain[24] ^ datain[26] ^ datain[27] ^ datain[31] ^ datain[35] ^ datain[36] ^ datain[37] ^ datain[38] ^ datain[39] ^ datain[40] ^ datain[42] ^ datain[43] ^ datain[44] ^ datain[45] ^ datain[49] ^ datain[50] ^ datain[53] ^ datain[54] ^ datain[56] ^ datain[57];

    Bit#(1) cksum3 = datain[0] ^ datain[3] ^ datain[4] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[13] ^ datain[15] ^ datain[17] ^ datain[21] ^ datain[25] ^ datain[26] ^ datain[29] ^ datain[32] ^ datain[34] ^ datain[37] ^ datain[39] ^ datain[40] ^ datain[44] ^ datain[45] ^ datain[46] ^ datain[48] ^ datain[50] ^ datain[51] ^ datain[52] ^ datain[54] ^ datain[57] ^ datain[58];

    Bit#(1) cksum4 = ~datain[1] ^ datain[4] ^ datain[5] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[14] ^ datain[16] ^ datain[18] ^ datain[22] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[33] ^ datain[35] ^ datain[38] ^ datain[40] ^ datain[41] ^ datain[45] ^ datain[46] ^ datain[47] ^ datain[49] ^ datain[51] ^ datain[52] ^ datain[53] ^ datain[55] ^ datain[58] ^ datain[59];

    Bit#(1) cksum5 = ~datain[2] ^ datain[5] ^ datain[6] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[15] ^ datain[17] ^ datain[19] ^ datain[23] ^ datain[27] ^ datain[28] ^ datain[31] ^ datain[34] ^ datain[36] ^ datain[39] ^ datain[41] ^ datain[42] ^ datain[46] ^ datain[47] ^ datain[48] ^ datain[50] ^ datain[52] ^ datain[53] ^ datain[54] ^ datain[56] ^ datain[59] ^ datain[60];

    Bit#(1) cksum6 = ~datain[0] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[9] ^ datain[12] ^ datain[14] ^ datain[16] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[23] ^ datain[26] ^ datain[27] ^ datain[32] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37] ^ datain[38] ^ datain[40] ^ datain[41] ^ datain[42] ^ datain[47] ^ datain[49] ^ datain[51] ^ datain[52] ^ datain[53] ^ datain[54] ^ datain[57] ^ datain[60] ^ datain[61];

    Bit#(1) cksum7 = ~datain[0] ^ datain[1] ^ datain[2] ^ datain[3] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[11] ^ datain[14] ^ datain[15] ^ datain[17] ^ datain[19] ^ datain[22] ^ datain[23] ^ datain[26] ^ datain[29] ^ datain[33] ^ datain[34] ^ datain[35] ^ datain[37] ^ datain[39] ^ datain[42] ^ datain[50] ^ datain[53] ^ datain[54] ^ datain[58] ^ datain[61];

    Bit#(1) cksum8 = ~datain[0] ^ datain[1] ^ datain[3] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[15] ^ datain[16] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[26] ^ datain[28] ^ datain[29] ^ datain[30] ^ datain[35] ^ datain[40] ^ datain[41] ^ datain[48] ^ datain[51] ^ datain[52] ^ datain[54] ^ datain[59];

    Bit#(1) cksum9 = ~datain[1] ^ datain[2] ^ datain[4] ^ datain[5] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[15] ^ datain[16] ^ datain[17] ^ datain[19] ^ datain[21] ^ datain[22] ^ datain[27] ^ datain[29] ^ datain[30] ^ datain[31] ^ datain[36] ^ datain[41] ^ datain[42] ^ datain[49] ^ datain[52] ^ datain[53] ^ datain[55] ^ datain[60];

    Bit#(1) cksum10 = ~datain[0] ^ datain[3] ^ datain[6] ^ datain[8] ^ datain[11] ^ datain[15] ^ datain[16] ^ datain[17] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[22] ^ datain[24] ^ datain[26] ^ datain[27] ^ datain[29] ^ datain[30] ^ datain[31] ^ datain[32] ^ datain[34] ^ datain[36] ^ datain[37] ^ datain[38] ^ datain[41] ^ datain[42] ^ datain[48] ^ datain[50] ^ datain[52] ^ datain[53] ^ datain[54] ^ datain[55] ^ datain[56] ^ datain[61];

    Bit#(1) cksum11 = ~datain[0] ^ datain[1] ^ datain[2] ^ datain[4] ^ datain[5] ^ datain[7] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[16] ^ datain[17] ^ datain[18] ^ datain[19] ^ datain[22] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[29] ^ datain[30] ^ datain[31] ^ datain[32] ^ datain[33] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37] ^ datain[39] ^ datain[41] ^ datain[42] ^ datain[48] ^ datain[49] ^ datain[51] ^ datain[52] ^ datain[53] ^ datain[54] ^ datain[56] ^ datain[57];

    Bit#(1) cksum12 = datain[1] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[8] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[14] ^ datain[15] ^ datain[17] ^ datain[18] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[31] ^ datain[32] ^ datain[33] ^ datain[34] ^ datain[35] ^ datain[36] ^ datain[37] ^ datain[38] ^ datain[40] ^ datain[42] ^ datain[43] ^ datain[49] ^ datain[50] ^ datain[52] ^ datain[53] ^ datain[54] ^ datain[55] ^ datain[57] ^ datain[58];

    Bit#(1) cksum13 = ~datain[0] ^ datain[3] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[15] ^ datain[16] ^ datain[18] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[29] ^ datain[31] ^ datain[32] ^ datain[33] ^ datain[35] ^ datain[37] ^ datain[39] ^ datain[44] ^ datain[48] ^ datain[50] ^ datain[51] ^ datain[52] ^ datain[53] ^ datain[54] ^ datain[56] ^ datain[58] ^ datain[59];

    Bit#(1) cksum14 = datain[0] ^ datain[1] ^ datain[2] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[12] ^ datain[14] ^ datain[16] ^ datain[17] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[30] ^ datain[32] ^ datain[33] ^ datain[40] ^ datain[41] ^ datain[43] ^ datain[45] ^ datain[48] ^ datain[49] ^ datain[51] ^ datain[53] ^ datain[54] ^ datain[57] ^ datain[59] ^ datain[60];

    Bit#(1) cksum15 = datain[1] ^ datain[2] ^ datain[3] ^ datain[5] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[11] ^ datain[13] ^ datain[15] ^ datain[17] ^ datain[18] ^ datain[20] ^ datain[21] ^ datain[24] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[30] ^ datain[31] ^ datain[33] ^ datain[34] ^ datain[41] ^ datain[42] ^ datain[44] ^ datain[46] ^ datain[49] ^ datain[50] ^ datain[52] ^ datain[54] ^ datain[55] ^ datain[58] ^ datain[60] ^ datain[61];

    Bit#(1) cksum16 = ~datain[0] ^ datain[3] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[8] ^ datain[12] ^ datain[13] ^ datain[16] ^ datain[18] ^ datain[19] ^ datain[22] ^ datain[23] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[31] ^ datain[32] ^ datain[35] ^ datain[36] ^ datain[38] ^ datain[41] ^ datain[42] ^ datain[45] ^ datain[47] ^ datain[48] ^ datain[50] ^ datain[51] ^ datain[52] ^ datain[53] ^ datain[56] ^ datain[59] ^ datain[61];

    Bit#(1) cksum17 = datain[1] ^ datain[4] ^ datain[5] ^ datain[6] ^ datain[7] ^ datain[9] ^ datain[13] ^ datain[14] ^ datain[17] ^ datain[19] ^ datain[20] ^ datain[23] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[31] ^ datain[32] ^ datain[33] ^ datain[36] ^ datain[37] ^ datain[39] ^ datain[42] ^ datain[43] ^ datain[46] ^ datain[48] ^ datain[49] ^ datain[51] ^ datain[52] ^ datain[53] ^ datain[54] ^ datain[57] ^ datain[60];

    Bit#(1) cksum18 = ~datain[0] ^ datain[6] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[11] ^ datain[13] ^ datain[15] ^ datain[18] ^ datain[20] ^ datain[23] ^ datain[25] ^ datain[32] ^ datain[33] ^ datain[36] ^ datain[37] ^ datain[40] ^ datain[41] ^ datain[44] ^ datain[47] ^ datain[48] ^ datain[49] ^ datain[50] ^ datain[53] ^ datain[54] ^ datain[58] ^ datain[61];

    Bit#(1) cksum19 = ~datain[0] ^ datain[1] ^ datain[2] ^ datain[5] ^ datain[7] ^ datain[8] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[16] ^ datain[19] ^ datain[23] ^ datain[27] ^ datain[28] ^ datain[29] ^ datain[33] ^ datain[36] ^ datain[37] ^ datain[42] ^ datain[43] ^ datain[45] ^ datain[49] ^ datain[50] ^ datain[51] ^ datain[52] ^ datain[54] ^ datain[59];

    Bit#(1) cksum20 = datain[0] ^ datain[1] ^ datain[3] ^ datain[5] ^ datain[6] ^ datain[8] ^ datain[10] ^ datain[11] ^ datain[12] ^ datain[17] ^ datain[20] ^ datain[21] ^ datain[23] ^ datain[26] ^ datain[27] ^ datain[30] ^ datain[36] ^ datain[37] ^ datain[41] ^ datain[44] ^ datain[46] ^ datain[48] ^ datain[50] ^ datain[51] ^ datain[53] ^ datain[60];

    Bit#(1) cksum21 = ~datain[1] ^ datain[2] ^ datain[4] ^ datain[6] ^ datain[7] ^ datain[9] ^ datain[11] ^ datain[12] ^ datain[13] ^ datain[18] ^ datain[21] ^ datain[22] ^ datain[24] ^ datain[27] ^ datain[28] ^ datain[31] ^ datain[37] ^ datain[38] ^ datain[42] ^ datain[45] ^ datain[47] ^ datain[49] ^ datain[51] ^ datain[52] ^ datain[54] ^ datain[61];

    Bit#(1) cksum22 = ~datain[0] ^ datain[3] ^ datain[7] ^ datain[8] ^ datain[9] ^ datain[11] ^ datain[12] ^ datain[19] ^ datain[21] ^ datain[22] ^ datain[24] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[32] ^ datain[34] ^ datain[36] ^ datain[39] ^ datain[41] ^ datain[46] ^ datain[50] ^ datain[53];

    Bit#(1) cksum23 = ~datain[1] ^ datain[4] ^ datain[8] ^ datain[9] ^ datain[10] ^ datain[12] ^ datain[13] ^ datain[20] ^ datain[22] ^ datain[23] ^ datain[25] ^ datain[26] ^ datain[27] ^ datain[28] ^ datain[33] ^ datain[35] ^ datain[37] ^ datain[40] ^ datain[42] ^ datain[47] ^ datain[51] ^ datain[54];

    return {cksum23, cksum22, cksum21, cksum20, cksum19, cksum18, cksum17, cksum16, cksum15, cksum14, cksum13, cksum12, cksum11, cksum10, cksum9, cksum8,cksum7, cksum6, cksum5, cksum4, cksum3, cksum2, cksum1,cksum0};

endfunction


interface Ifc_RapidIO_PhyCRC24Checker;
 
//input methods and output methods
	method Action _inputs_ControlSymbolDatacrc_in (Bit#(64) value);
	method Action _inputs_rx_vld (Bool value);

	method Bit#(24) outputchk_CRC24_ ();//output crc 24 checksum returning method

endinterface : Ifc_RapidIO_PhyCRC24Checker


(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIO_PhyCRC24Checker (Ifc_RapidIO_PhyCRC24Checker);

// Input Signals as Wires
Wire#(Bit#(62)) wr_data_in <- mkDWire (0);//38 bits input data + 24 bits CRC code
Wire#(Bool) wr_rx_vld <- mkDWire (True);


method Action _inputs_ControlSymbolDatacrc_in (Bit#(64) value);
    wr_data_in <= value[63:2];
endmethod

method Action _inputs_rx_vld (Bool value);
    wr_rx_vld <= value;
endmethod

method Bit#(24) outputchk_CRC24_ ();
 
    if (wr_rx_vld == False) 
        return fn_CRC24Checker (wr_data_in);
    else    
        return 24'hffffff; // By default, as per specification the initial value of CRC is 24'hffffff
endmethod


endmodule : mkRapidIO_PhyCRC24Checker
endpackage : RapidIO_PhyCRC24Checker
