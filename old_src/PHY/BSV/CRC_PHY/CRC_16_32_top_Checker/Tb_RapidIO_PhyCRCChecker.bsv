/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- This Module is a test module to check the entire CRC operation developed, which will be used in the PHY top module
-- It is connected to the top module for checking. 
-- 
--To be done
-- 1.If total length of packet is more than 80 bytes (Two CRC16 codes are appended and both cases have to be checked)
-- 2.If CRC16 or CRC32 code appears in the next cycle after last data cycle,i.e when tx_rem == 101,110 & 111.
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_PhyCRCChecker;

import RapidIO_PhyCRCChecker ::*;


(*synthesize*)
(*always_enabled*)
(*always_ready*)

module mkTb_RapidIO_PhyCRCChecker(Empty);

Ifc_RapidIO_PhyCRCChecker crc_check <- mkRapidIO_PhyCRCChecker;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

/*
-- Following rule, it is used to generate reference clock 
*/
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 10 )
		$finish (0);
endrule

/*
rule r2(reg_ref_clk == 0); 
crc_check._link_tx_sof_n (False);
crc_check._link_tx_vld_n (False);
crc_check._link_tx_data (128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5);
crc_check._link_tx_eof_n (False);
crc_check._link_tx_rem (3'b000);
$display("Input == 128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5");
$display("REM value == 3'b000");
endrule:r2
*/

rule r2(reg_ref_clk == 1); 
crc_check._link_tx_sof_n (False);
crc_check._link_tx_vld_n (False);
crc_check._link_tx_data (128'hCEA0F52ABD080000DB4CF9EC00000000);
crc_check._link_tx_eof_n (False);
crc_check._link_tx_rem (3'b001);
$display("Input == 128'hCEA0F52ABD080000DB4CF9ED00000000");
$display("REM value == 3'b001");
endrule

rule r3(reg_ref_clk == 2); 
crc_check._link_tx_sof_n (False);
crc_check._link_tx_vld_n (False);
crc_check._link_tx_data (128'hCEA0F52AD3E634E5D5776F9A00000000);
crc_check._link_tx_eof_n (False);
crc_check._link_tx_rem (3'b010);
$display("Input == 128'hCEA0F52AD3E634E5D5776F9A00000000");
$display("REM value == 3'b010");
endrule

rule r4(reg_ref_clk == 3); 
crc_check._link_tx_sof_n (False);
crc_check._link_tx_vld_n (False);
crc_check._link_tx_data (128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5);
crc_check._link_tx_eof_n (True);
crc_check._link_tx_rem (3'b111);
$display("Input == 128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5");
$display("REM value == 3'b111");
endrule

rule r5(reg_ref_clk == 4); 
crc_check._link_tx_sof_n (True);
crc_check._link_tx_vld_n (False);
crc_check._link_tx_data (128'hCEA0F52AD3E65F3A051284A700000000);
crc_check._link_tx_eof_n (False);
crc_check._link_tx_rem (3'b010);
$display("Input == 128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5");
$display("REM value == 3'b010");
endrule

/*
rule r2_1(reg_ref_clk == 3); 
crc_check._link_tx_sof_n (False);
crc_check._link_tx_vld_n (False);
crc_check._link_tx_data (128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5);
crc_check._link_tx_eof_n (True);
crc_check._link_tx_rem (3'b111);
$display("Input == 128'hCEA0F52AD3E6EFC11375A8D9C8EFC0A5");
$display("REM value == 3'b111");
endrule:r2_1

rule r2_2(reg_ref_clk == 4); 
crc_check._link_tx_sof_n (True);
crc_check._link_tx_vld_n (False);
crc_check._link_tx_data (128'hcea0f52aa0f70000718e0df300000000);
crc_check._link_tx_eof_n (False);
crc_check._link_tx_rem (3'b001);
$display("Input == 128'hcea0f52aa0f70000718e0df300000000");
$display("REM value == 3'b001");
endrule:r2_2*/

rule r9;
$display("crc16 check out = %b",crc_check.outputs_CRC16_());
$display("crc32 check out = %b",crc_check.outputs_CRC32_());
endrule

endmodule:mkTb_RapidIO_PhyCRCChecker
endpackage:Tb_RapidIO_PhyCRCChecker
