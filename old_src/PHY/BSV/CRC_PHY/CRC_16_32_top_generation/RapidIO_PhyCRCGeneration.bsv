/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
--Description
-- This Module is a test module to check the entire CRC operation developed, which will be used in the PHY top module 
-- 1.crc16 is generated and appended to the data and then data is given to crc32 module.this data includes crc16 and zero padding if 
--   required.
-- 2.the input is delayed and given to crc32 module and crc32 is also generated  and appended.
-- 3.Registers are used in crc16 and crc32 individual modules instead of wires so that proper timing is maintained(to be changed in future   --   if possible)
--
--To be done
-- 1.If total length of packet is more than 80 bytes (Two CRC16 codes are appended and both cases have to be checked)
-- 2.If CRC16 or CRC32 code appears in the next cycle after last data cycle,i.e when tx_rem == 101,110 & 111.
-- 
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_PhyCRCGeneration;

import RapidIO_PhyCRC16Generation::*;
import RapidIO_PhyCRC32Generation::*;

interface Ifc_RapidIO_PhyCRCGeneration;

//input methods and output methods
method Action _link_tx_sof_n (Bool value);
method Action _link_tx_eof_n (Bool value);
method Action _link_tx_vld_n (Bool value);
method Action _link_tx_data (Bit#(128) value);
method Action _link_tx_rem (Bit#(3) value);
 
method Bool out_sof_();
method Bool out_eof_();
method Bool out_vld_();
method Bit#(3) out_rem_();

//method Bit#(1) out_en_32_();
method Bit#(128) outputs_DataCRC_();

endinterface : Ifc_RapidIO_PhyCRCGeneration

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIO_PhyCRCGeneration(Ifc_RapidIO_PhyCRCGeneration);

Ifc_RapidIO_PhyCRC16Generation crc16 <- mkRapidIO_PhyCRC16Generation;
Ifc_RapidIO_PhyCRC32Generation crc32 <- mkRapidIO_PhyCRC32Generation;

//Input signals as wires
Wire#(Bool) wr_tx_sof <- mkDWire (True);
Wire#(Bool) wr_tx_eof <- mkDWire (True);
Wire#(Bool) wr_tx_vld <- mkDWire(True);
Wire#(Bit#(128)) wr_tx_data <- mkDWire (0);
Wire#(Bit#(128)) wr_tx_datacode <- mkDWire (0);
Wire#(Bit#(3)) wr_tx_rem <- mkDWire (0);

//Intermediate registers for providing required delays when appending crc16 and also when giving to crc32 module.
Reg#(Bool) rg_out_sof <- mkReg (True);
Reg#(Bool) rg_out_eof <- mkReg (True);
Reg#(Bool) rg_out_vld <- mkReg (True);
Reg#(Bit#(128)) rg_out_data <- mkReg (0);
Reg#(Bit#(3)) rg_out_rem <- mkReg (0);

Reg#(Bool) rg_in_sof <- mkReg (True);
Reg#(Bool) rg_in_eof <- mkReg (True);
Reg#(Bool) rg_in_vld <- mkReg (True);
Reg#(Bit#(128)) rg_in_data <- mkReg (0);
Reg#(Bit#(3)) rg_in_rem <- mkReg (0);
//Reg#(Bit#(1)) rg_en_32 <- mkReg (0);*/


//output signals as wires
Wire#(Bool) wr_out_sof <- mkDWire (True);
Wire#(Bool) wr_out_eof <- mkDWire (True);
Wire#(Bool) wr_out_vld <- mkDWire (True);
Wire#(Bit#(128)) wr_out_data <- mkDWire (0);
Wire#(Bit#(128)) wr_out_datacode <- mkDWire (0);
Wire#(Bit#(3)) wr_out_rem <- mkDWire (0);


			
//input to crc16 module

rule r1_crc16;

crc16._link_tx_sof_n(wr_tx_sof);
crc16._link_tx_eof_n(wr_tx_eof);
crc16._link_tx_vld_n(wr_tx_vld);
crc16._link_tx_data(wr_tx_data);
crc16._link_tx_rem(wr_tx_rem);

//delay provided for crc16 appending
rg_out_data <= wr_tx_data ;
rg_out_sof <= wr_tx_sof;
//if(wr_tx_rem == 111)
rg_out_eof <= wr_tx_eof;
rg_out_vld <= wr_tx_vld;
rg_out_rem <= wr_tx_rem;

endrule


//delay provided for crc32 module
rule delay_crc16;

rg_in_data <=  rg_out_eof ? rg_out_data : wr_tx_datacode ;//if eof is false,appended data is taken,otherwise original data is taken
rg_in_sof <= rg_out_sof;
rg_in_eof <= rg_out_eof;
rg_in_vld <= rg_out_vld;
rg_in_rem <= rg_out_rem;

endrule

//crc16 appending
rule r1_crc16_append;

$display("rem value for appending crc16 = %b",rg_out_rem);
$display("crc16 = %h",crc16.outputs_CRC16_ ());

case(rg_out_rem)

3'b111:wr_tx_datacode <= {crc16.outputs_CRC16_ (),112'b0};
3'b110:wr_tx_datacode <= {rg_out_data[127:16],crc16.outputs_CRC16_ ()};
3'b101:wr_tx_datacode <= {rg_out_data[127:32],crc16.outputs_CRC16_ (),16'b0};
3'b100:wr_tx_datacode <= {rg_out_data[127:48],crc16.outputs_CRC16_ (),32'b0};
3'b011:wr_tx_datacode <= {rg_out_data[127:64],crc16.outputs_CRC16_ (),48'b0};
3'b010:wr_tx_datacode <= {rg_out_data[127:80],crc16.outputs_CRC16_ (),64'b0};
3'b001:wr_tx_datacode <= {rg_out_data[127:96],crc16.outputs_CRC16_ (),80'b0};
3'b000:wr_tx_datacode <= {rg_out_data[127:112],crc16.outputs_CRC16_ (),96'b0};

endcase

endrule


//intermediate signal -- data along with crc16 which is given as input to crc32 module
rule r1_crc16_out;
$display("data appended with crc16 and given to crc32 module = %h",wr_tx_datacode);
endrule

//crc32 module
rule r2_crc32;

crc32._link_tx_sof_n(rg_out_sof);
crc32._link_tx_eof_n(rg_out_eof);
crc32._link_tx_vld_n(rg_out_vld);
crc32._link_tx_data((rg_out_eof ? rg_out_data : wr_tx_datacode));
crc32._link_tx_rem(rg_out_rem);

endrule



//crc32 appending
rule r2_crc32_out;

$display("crc32=%h",crc32.output_CRC32_());
$display("rem value for appending crc32 = %b",rg_in_rem);

case(rg_in_rem)

3'b111:wr_out_datacode <= {rg_in_data[127:112],16'b0,crc32.output_CRC32_(),64'b0};
3'b110:wr_out_datacode <= {crc32.output_CRC32_(),96'b0};
3'b101:wr_out_datacode <= {crc32.output_CRC32_(),96'b0};
3'b100:wr_out_datacode <= {rg_in_data[127:32],crc32.output_CRC32_()};
3'b011:wr_out_datacode <= {rg_in_data[127:48],16'b0,crc32.output_CRC32_()};
3'b010:wr_out_datacode <= {rg_in_data[127:64],crc32.output_CRC32_(),32'b0};
3'b001:wr_out_datacode <= {rg_in_data[127:80],16'b0,crc32.output_CRC32_(),32'b0};
3'b000:wr_out_datacode <= {rg_in_data[127:96],crc32.output_CRC32_(),64'b0};

endcase

endrule


//output of crc module
rule r2_crc_out;

wr_out_sof <= rg_in_sof;
wr_out_eof <= rg_in_eof;
wr_out_vld <= rg_in_vld;
wr_out_data <= rg_in_eof ? rg_in_data : wr_out_datacode;//if eof is false,appended data is taken,otherwise original data is taken.
wr_out_rem <= rg_in_rem;

endrule

//input and output method definitions

method Action _link_tx_sof_n (Bool value);
	wr_tx_sof <= value;
endmethod

method Action _link_tx_eof_n (Bool value);
	wr_tx_eof <= value;
endmethod
 
method Action _link_tx_vld_n (Bool value);
	wr_tx_vld <= value;
endmethod

method Action _link_tx_data (Bit#(128) value);
	wr_tx_data <= value;
endmethod

method Action _link_tx_rem (Bit#(3) value);
	wr_tx_rem <= value;
endmethod
 
method Bool out_sof_();
	return wr_out_sof;
endmethod

method Bool out_eof_();
	return wr_out_eof;
endmethod

method Bool out_vld_();
	return wr_out_vld;
endmethod

method Bit#(3) out_rem_();
	return wr_out_rem;
endmethod

method Bit#(128) outputs_DataCRC_();
	return wr_out_data;
endmethod

endmodule:mkRapidIO_PhyCRCGeneration
endpackage:RapidIO_PhyCRCGeneration
