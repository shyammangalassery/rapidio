/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Verification (Looping the Transmit and Receive signals) IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1. This module connects the Transmit and Receive signals respectively. 
-- 2. Verifies the Deadlocks present in the RapidIO.
-- 3. Used to verify the logic functions
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_LoopVerification;

import RapidIO_DTypes ::* ;

interface Ifc_RapidIO_LoopVerification;
 // Input Methods - Transmit Signals
 //-- Control Signals
 method Action _intx_sof_n (Bool value);
 method Action _intx_eof_n (Bool value);
 method Action _intx_vld_n (Bool value);
 method Action _intx_dsc_n (Bool value);
 method Bool outtx_rdy_n_ ();

 //-- Data Signals
 method Action _intx_data (Data value);
 method Action _intx_rem (Bit#(3) value);
 method Action _intx_crf (Bool value);
 method Bool outtx_master_enable_ (); 

 // Output Methods - Receive Signals
 //-- Control Signals
 method Bool out_rx_sof_n_ ();
 method Bool out_rx_eof_n_ ();
 method Bool out_rx_vld_n_ ();
 method Action in_rx_rdy_n (Bool value);

 //-- Data Signals
 method Data out_rx_data_ ();
 method Bit#(3) out_rx_rem_ ();
 method Bool out_rx_crf_ ();

endinterface : Ifc_RapidIO_LoopVerification


(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_LoopVerification (Ifc_RapidIO_LoopVerification); 
 Wire#(Bool) wr_tx_sof <- mkDWire(True);
 Wire#(Bool) wr_tx_eof <- mkDWire (True);
 Wire#(Bool) wr_tx_vld <- mkDWire (True);
 Wire#(Bool) wr_tx_dsc <- mkDWire (True);
 Wire#(Bool) wr_tx_rdy <- mkDWire (True);

 //-- Data Signals
 Wire#(Data) wr_tx_data <- mkDWire (0);
 Wire#(Bit#(3)) wr_tx_rem <- mkDWire (0);
 Wire#(Bool) wr_tx_crf <- mkDWire (True);
 Wire#(Bool) wr_tx_master_enable <- mkDWire (False); 

 // Output Methods - Receive Signals
 //-- Control Signals
 Wire#(Bool) wr_rx_sof <- mkDWire (True);
 Wire#(Bool) wr_rx_eof <- mkDWire (True);
 Wire#(Bool) wr_rx_vld <- mkDWire (True);
 Wire#(Bool) wr_rx_rdy <- mkDWire (True);

 //-- Data Signals
 Wire#(Data) wr_rx_data <- mkDWire (0);
 Wire#(Bit#(3)) wr_rx_rem <- mkDWire (0);
 Wire#(Bool) wr_rx_crf <- mkDWire (True);

// Method Definitions 
// Input Methods 
 //-- Control Methods 
 method Action _intx_sof_n (Bool value);
    wr_tx_sof <= value; 
 endmethod 
 method Action _intx_eof_n (Bool value);
    wr_tx_eof <= value; 
 endmethod 
 method Action _intx_vld_n (Bool value);
    wr_tx_vld <= value; 
 endmethod
 method Action _intx_dsc_n (Bool value);
    wr_tx_dsc <= value; 
 endmethod
 method Bool outtx_rdy_n_ ();
    return wr_rx_rdy;
 endmethod 

 //-- Data Signals
 method Action _intx_data (Data value);
    wr_tx_data <= value; 
 endmethod
 method Action _intx_rem (Bit#(3) value);
    wr_tx_rem <= value; 
 endmethod
 method Action _intx_crf (Bool value);
    wr_tx_crf <= value;
 endmethod 
 method Bool outtx_master_enable_ ();
    return False; 
 endmethod

// Output Methods
 //-- Control Signals
 method Bool out_rx_sof_n_ ();
    return wr_tx_sof;
 endmethod 
 method Bool out_rx_eof_n_ ();
    return wr_tx_eof;
 endmethod
 method Bool out_rx_vld_n_ ();
    return wr_tx_vld;
 endmethod 
 method Action in_rx_rdy_n (Bool value);
    wr_rx_rdy <= value; 
 endmethod 

 //-- Data Signals
 method Data out_rx_data_ ();
    return wr_tx_data; 
 endmethod 
 method Bit#(3) out_rx_rem_ ();
    return wr_tx_rem; 
 endmethod 
 method Bool out_rx_crf_ ();
    return wr_tx_crf; 
 endmethod 


endmodule : mkRapidIO_LoopVerification
endpackage : RapidIO_LoopVerification



