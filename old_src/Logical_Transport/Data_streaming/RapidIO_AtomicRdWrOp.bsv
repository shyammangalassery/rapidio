/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Atomic Read/Write Operations Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This module is designed to perform Atomic Read and Write Operations for 
-- Ftype 2 (Request Class) and Ftype 5 (Write Class.)
-- 1. Atomic Request Class Operations:
--    1. Finite State Machine is used to perform atomic operations. It consists
--       of 5 States (Idle, Read, Rd_Modify, Wr_Modify, Update).
--    2. Idle State is default state. In the Read state, the data from the Memory 
--       is read and state is updated to next state, Modify in following clock cycle.
--    3. In Modify state, the data from the memory is modified using the ttype (different 
--       atomic operations) and updated the data to memory. 
--    4. In Update state, the result of the Modify state data is updated in the memory. 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package RapidIO_AtomicRdWrOp;

import RegFile ::* ;
import RapidIO_DTypes ::* ;
import RapidIO_FTypeDfns ::* ;

/*
-- To Perform Atomic Operations, the Target Request Interface signals are used. 
-- Since Message Passing signals are not required for atomic operations, those signals are 
-- remained unused. Only Control and Data signlas are used.
*/
interface Ifc_RapidIO_AtomicRdWrOp;

// Input Methods
 //-- Control Signal Interface
 method Action _inputs_treq_sof (Bool value);
 method Action _inputs_treq_eof (Bool value);
 method Action _inputs_treq_vld (Bool value);
// method Action _treq_rdy_n (Bool value); // Output

 //-- Data Signal Interface
 method Action _inputs_treq_tt (TT value);
 method Action _inputs_treq_data (Data value);
 method Action _inputs_treq_crf (Bool value);
 method Action _inputs_treq_prio (Prio value);
 method Action _inputs_treq_ftype (Type value);
 method Action _inputs_treq_dest_id (DestId value);
 method Action _inputs_treq_source_id (SourceId value);
 method Action _inputs_treq_tid (TranId value);
 method Action _inputs_treq_ttype (Type value);
 method Action _inputs_treq_addr (Addr value);
 method Action _inputs_treq_byte_count (ByteCount value);
 method Action _inputs_treq_byte_en (ByteEn value);

// Output Methods 
 // -- Control Signal Interface 
 method Bool outputs_tresp_sof_ ();
 method Bool outputs_tresp_eof_ (); 
 method Bool outputs_tresp_vld_ ();
// method Action tresp_rdy_n_ (Bool value);

 // -- Data Signal Interface
 method TT outputs_tresp_tt_ ();
 method Data outputs_tresp_data_ ();
 method Bool outputs_tresp_crf_ ();
 method Prio outputs_tresp_prio_ ();
 method Type outputs_tresp_ftype_ ();
 method SourceId outputs_tresp_src_id_ ();
 method DestId outputs_tresp_dest_id_ ();
 method Status outputs_tresp_status_ ();
 method TranId outputs_tresp_tid_ ();
 method Type outputs_tresp_ttype_ ();

endinterface : Ifc_RapidIO_AtomicRdWrOp

/*
-- Enumeration type is defined with the name State_At_Rd which has 4 states: State_Idle, State_Read, State_RdModify, State_WrModify and State_Update.
*/
typedef enum {State_Idle, State_Read, State_RdModify, State_WrModify, State_Update} State_At_Rd deriving (Bits, Eq);

/*
-- Following function is used to perform Atomic operations. 
-- Since according to the RapidIO specification, Byte Count and Byte Enable signals are used to modify the 
-- 1-byte, 2-byte and 4-byte data.
-- It takes two datas: one from the memory and modified data (using atomic operations)  
*/

function Bit#(65) fn_AtomicRdModify (Data datafrommem, Data dataatrd, ByteCount bytecnt, ByteEn byteen);
    /*
    -- Byte Enable determines the byte of the 64 bit data. 
    */
    if (bytecnt == 9'd1) begin // One Byte Read
	if (byteen == 8'b10000000)
		return {1'b0, dataatrd[63:56], datafrommem[55:0]};
	else if (byteen == 8'b01000000)
		return {1'b0, datafrommem[63:56], dataatrd[55:48], datafrommem[47:0]};
	else if (byteen == 8'b00100000)
		return {1'b0, datafrommem[63:48], dataatrd[47:40], datafrommem[39:0]};
	else if (byteen == 8'b00010000)
		return {1'b0, datafrommem[63:40], dataatrd[39:32], datafrommem[31:0]};
	else if (byteen == 8'b00001000)
		return {1'b0, datafrommem[63:32], dataatrd[31:24], datafrommem[23:0]};
	else if (byteen == 8'b00000100)
		return {1'b0, datafrommem[63:24], dataatrd[23:16], datafrommem[15:0]};
	else if (byteen == 8'b00000010)
		return {1'b0, datafrommem[63:16], dataatrd[15:8], datafrommem[7:0]};
	else if (byteen == 8'b00000001)
		return {1'b0, datafrommem[63:8], dataatrd[7:0]};
	else 
		return {1'b1, datafrommem};
    end
    else if (bytecnt == 9'd2) begin // Two byte Read 
	if (byteen == 8'b11000000)
		return {1'b0, dataatrd[63:48], datafrommem[47:0]};
	else if (byteen == 8'b00110000)
		return {1'b0, datafrommem[63:48], dataatrd[47:32], datafrommem[31:0]};
	else if (byteen == 8'b00001100)
		return {1'b0, datafrommem[63:32], dataatrd[31:16], datafrommem[15:0]};
	else if (byteen == 8'b00000011)
		return {1'b0, datafrommem[63:16], dataatrd[15:0]};
	else 
		return {1'b1, datafrommem}; 
    end 
    else if (bytecnt == 9'd3) begin // Three bytes Read 
	if (byteen == 8'b11100000)
		return {1'b0, dataatrd[63:40], datafrommem[39:0]};
	else if (byteen == 8'b00000111)
		return {1'b0, datafrommem[63:24], dataatrd[23:0]};
	else 
		return {1'b1, datafrommem}; 
    end 
    else if (bytecnt == 9'd4) begin // Four bytes Read 
	if (byteen == 8'b11110000)
		return {1'b0, dataatrd[63:32], datafrommem[31:0]};
	else if (byteen == 8'b00001111)
		return {1'b0, datafrommem[63:32], dataatrd[31:0]};
	else 
		return {1'b1, datafrommem}; 
    end 
    else if (bytecnt == 9'd5) begin // Five bytes Read 
	if (byteen == 8'b11111000)
		return {1'b0, dataatrd[63:24], datafrommem[23:0]}; 
	else if (byteen == 8'b00011111)
		return {1'b0, datafrommem[63:40], dataatrd[39:0]};
	else 
		return {1'b1, datafrommem};
    end 
    else if (bytecnt == 9'd6) begin // 6 bytes Read 
	if (byteen == 8'b11111100)
		return {1'b0, dataatrd[63:16], datafrommem[15:0]};
	else if (byteen == 8'b00111111)
		return {1'b0, datafrommem[63:48], dataatrd[47:0]};
	else
		return {1'b1, datafrommem};
    end 
    else if (bytecnt == 9'd7) begin // 7 bytes Read 
	if (byteen == 8'b11111110)
		return {1'b0, dataatrd[63:8], datafrommem[7:0]};
	else if (byteen == 8'b01111111)
		return {1'b0, datafrommem[63:56], dataatrd[55:0]};
	else
		return {1'b1, datafrommem};
    end 
    else if (bytecnt == 9'd8) begin // 8 bytes Read 
	if (byteen == 8'b11111111)
	    	return {1'b0, dataatrd}; 
        else 
		return {1'b1, datafrommem};
    end 
    else // Otherwise generates Error Response 
	return {1'b1, datafrommem};
endfunction 

/*
-- Following function is used to perform atomic read operations.
-- It uses the data from the memory and perform atomic operations and result
-- is sent to another function to modify based on 1-byte, 2-byte and 4-byte 
*/
function Bit#(64) fn_AtomicReadOPn (Type ttype, Data datain);
    case (ttype) matches
	4'b0100: return datain; // NREAD
	4'b1100: begin // Atomic Incr
		    Bit#(65) lv_atincr = {1'b0, datain} + 'b1; 
		    return lv_atincr[63:0];
		end 
	4'b1101: begin // Atomic Decr 
		   Bit#(65) lv_atdecr = {1'b0, datain} - 'b1; 
		   return lv_atdecr[63:0];
		end 
	4'b1110: return 64'hffffffffffffffff; // Atomic Set
	4'b1111: return 64'b0; // Atomic Clear 
	default : return datain; 
    endcase
endfunction 

/*
-- Following rule is used to perform atomic write operations.
-- Implemented all the atomic write but Compare and swap yet to implement 
*/
function Bit#(65) fn_AtomicWriteOPn (Type ttype, Data datafrommem, Data supplieddata);
   case (ttype) matches 
	4'b0100: return {1'b0, supplieddata}; // NWRITE Transaction 
	4'b0101: return {1'b0, supplieddata}; // NWRITE_R Transaction 
	4'b1100: return {1'b0, supplieddata}; // Atomic Swap (Unconditionally swap the supplied data with the data from the memory)
// 	4'b1101: // Yet to Add Atomic Compare and Swap 
	4'b1110: begin
		if (datafrommem == 0) 
			return {1'b0, supplieddata}; 
		else 
			return {1'b0, datafrommem}; 
		end 
	default : return {1'b1, datafrommem};
   endcase 
endfunction 

(* synthesize *)
//(* always_enabled *)
//(* always_ready *)
module mkRapidIO_AtomicRdWrOp (Ifc_RapidIO_AtomicRdWrOp);
// Inputs Methods as Wires
 //-- Control Signal Interface
Wire#(Bool) wr_treq_sof <- mkDWire(False);
Wire#(Bool) wr_treq_eof <- mkDWire (False);
Wire#(Bool) wr_treq_vld <- mkDWire (False);
// method Action _treq_rdy_n (Bool value);

 //-- Data Signal Interface
Wire#(TT) wr_treq_tt <- mkDWire (0); 
Wire#(Data) wr_treq_data <- mkDWire (0);
Wire#(Bool) wr_treq_crf <- mkDWire (False);
Wire#(Prio) wr_treq_prio <- mkDWire (0);
Wire#(Type) wr_treq_ftype <- mkDWire (0);
Wire#(DestId) wr_treq_destid <- mkDWire (0);
Wire#(SourceId) wr_treq_srcid <- mkDWire (0);
Wire#(TranId) wr_treq_tid <- mkDWire (0);
Wire#(Type) wr_treq_ttype <- mkDWire (0);
Wire#(Addr) wr_treq_addr <- mkDWire (0);
Wire#(ByteCount) wr_treq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_treq_byte_en <- mkDWire (0);

// Internal Wires and Registers
Reg#(State_At_Rd) rg_AtomicState <- mkReg (State_Idle);
Reg#(Data) rg_DataInput <- mkReg (0);
Reg#(Type) rg_ftype <- mkReg (0);
Reg#(Type) rg_ttype <- mkReg (0);
Reg#(DestId) rg_destid <- mkReg (0); 
Reg#(SourceId) rg_srcid <- mkReg (0);
Reg#(TranId) rg_TID <- mkReg (0);
Reg#(Prio) rg_prio <- mkReg (0); 
Reg#(Addr) rg_Addr <- mkReg (0); 
Reg#(ByteCount) rg_ByteCount <- mkReg (0);
Reg#(ByteEn) rg_ByteEn <- mkReg (0);
Reg#(Bool) rg_Busy <- mkReg (False);
Reg#(Bit#(1)) rg_AtomicOPStatus <- mkReg (0); 
Reg#(Type) rg_ttype_resp <- mkReg (0); 
Reg#(TT) rg_tt <- mkReg (0); 

// Target Response Signals as Wires 
Wire#(Bool) wr_tresp_SOF <- mkDWire (False); 
Wire#(Bool) wr_tresp_EOF <- mkDWire (False); 
Wire#(Bool) wr_tresp_Vld <- mkDWire (False); 

Wire#(TT) wr_tresp_tt <- mkDWire (0); 
Wire#(Data) wr_tresp_Data <- mkDWire (0);
Wire#(Type) wr_tresp_ftype <- mkDWire (0);
Wire#(Type) wr_tresp_ttype <- mkDWire (0);
Wire#(Prio) wr_tresp_prio <- mkDWire (0);
Wire#(Status) wr_tresp_status <- mkDWire (0);
Wire#(Status) wr_AtOpstatus <- mkDWire (0); 
Wire#(TranId) wr_tresp_TID <- mkDWire (0); 
Wire#(SourceId)	wr_tresp_srcid <- mkDWire (0);
Wire#(DestId) wr_tresp_destid <- mkDWire (0); 


Reg#(Data) rg_Data_From_Mem <- mkReg (0); // Used to store the Data from the Memory 
Reg#(Maybe#(Data)) rg_AtomicModifiedData <- mkReg (tagged Invalid); // Used to store the modified data (after atomic operation performed)

/*
-- RegFileLoad is used as the Memory (as far this module concerned). This is just to verify ing the module 
-- It can be changed according to the RapidIO application 
-- NOTE: Create Memory.hex file to duplicate the memory 
*/
RegFile#(Bit#(5), Bit#(64)) memory <- mkRegFileLoad ("Memory.hex", 0, 31); 


/*
-- Finite State Machine to Perform Atomic Read and Write Operation 
-- 1. State Idle 
*/
rule rl_Validating_Atomic_Read_Signals (rg_AtomicState == State_Idle);
    if (wr_treq_sof == True) begin // When SOF is valid, the input is stored and state machine is changed to next state
        DestId lv_destid = (wr_treq_tt == 2'b00) ? {wr_treq_destid[31:24], 24'd0} : 
                    (wr_treq_tt == 2'b01) ? {wr_treq_destid[31:16], 16'd0} : 
                    (wr_treq_tt == 2'b10) ? wr_treq_destid : 0;

        SourceId lv_srcid = (wr_treq_tt == 2'b00) ? {wr_treq_srcid[31:24], 24'd0} : 
                    (wr_treq_tt == 2'b01) ? {wr_treq_srcid[31:16], 16'd0} : 
                    (wr_treq_tt == 2'b10) ? wr_treq_srcid : 0;


 
	rg_DataInput <= (wr_treq_ftype == 'd5) ? wr_treq_data : 0; 
	rg_tt <= wr_treq_tt; 
	rg_ftype <= wr_treq_ftype;
	rg_ttype <= wr_treq_ttype;
	rg_destid <= lv_destid; 
	rg_srcid <= lv_srcid;
	rg_TID <= wr_treq_tid; 
	rg_prio <= wr_treq_prio; 
	rg_Addr <= wr_treq_addr; 
	rg_ByteCount <= wr_treq_byte_count;
	rg_ByteEn <= wr_treq_byte_en;
	rg_AtomicState <= State_Read;
	rg_Busy <= True; 
	$display ("\n Atomic Operations : State Machine Started ! ! !");
    end 
    else if (rg_Busy == False) begin
	rg_DataInput <= 0; 
	rg_tt <= 0; 
	rg_ftype <= 0;
	rg_ttype <= 0;
	rg_destid <= 0; 
	rg_srcid <= 0;
	rg_TID <= 0; 
	rg_prio <= 0; 
	rg_Addr <= 0; 
	rg_ByteCount <= 0;
	rg_ByteEn <= 0;
	rg_AtomicModifiedData <= tagged Invalid; 
	rg_Data_From_Mem <= 0;
	rg_AtomicState <= State_Idle;  
	rg_AtomicOPStatus <= 0;
	$display ("\n No Valid Inputs Current State : IDLE ");
    end

endrule  

/*
-- Finite State Machine 
-- 2. State Read  
-- Determines whether Atomic Read or Atomic Write depend on the ftype input 
-- Neither case, state machine changes to default Idle State. 
-- Reads the Data from the Memory and stored it in a Register 
*/
rule rl_Read_From_Memory (rg_AtomicState == State_Read); 
    if (rg_ftype == 'd2) begin // To determine Read Operation 
	rg_Data_From_Mem <= memory.sub (rg_Addr[7:3]);
	rg_AtomicState <= State_RdModify;
	$display ("\n Current State Read \n Moved to Next State : State Read Modify ");
    end
    else if (rg_ftype == 'd5) begin // To determine Write Operation 
	rg_Data_From_Mem <= memory.sub (rg_Addr[7:3]);
	rg_AtomicState <= State_WrModify;
	$display ("\n Current State Read \n Moved to Next State : State Write Modify ");
    end  
    else begin
	rg_Data_From_Mem <= 0;
    	rg_AtomicState <= State_Idle;
    end 
endrule 

/*
-- Finite State Machine 
-- 3.1 State Read Modify (Depend on the Incoming Ftype value, Read or Write is determined)
-- Performs Atomic Read operation. 
-- Modifies the Output data based on the Byte count and Byte Enable 
-- Also determines the Status of the operation 
-- After the operation is completed, State machine changes to Update 
*/
rule rl_Perform_Atomic_Read_Opn (rg_AtomicState == State_RdModify);
    Data lv_AtomicOpnData =  fn_AtomicReadOPn (rg_ttype, rg_Data_From_Mem);
    Data lv_DataFrmMem = (rg_ttype == 'd4) ? 0 : rg_Data_From_Mem;
    Bit#(65) lv_fn_AtomicRdModify = fn_AtomicRdModify (lv_DataFrmMem, lv_AtomicOpnData, rg_ByteCount, rg_ByteEn);
    rg_AtomicModifiedData <= tagged Valid lv_fn_AtomicRdModify[63:0];
    rg_AtomicOPStatus <= lv_fn_AtomicRdModify[64]; 
    rg_ttype_resp <= 4'b1000; 
    rg_AtomicState <= State_Update;
    $display ("\n Atomic Read Operation is completed \n Next State : Update");
endrule 

/*
-- Finite State Machine 
-- 3.2 State Write Modify (Depend on the Incoming Ftype value, Read or Write is determined) 
-- Performs the Atomic Write Operations 
-- Generate the Status of the Atomic Operation 
-- Generate the Ttype for the Response signals 
-- After the operation is completed, State machine changes to Update 
*/
rule rl_Perform_Atomic_Write_Opn (rg_AtomicState == State_WrModify);
    Bit#(65) lv_fn_AtomicWriteOPn = fn_AtomicWriteOPn (rg_ttype, rg_Data_From_Mem, rg_DataInput);
    rg_AtomicModifiedData <= tagged Valid lv_fn_AtomicWriteOPn[63:0];
    rg_AtomicOPStatus <= lv_fn_AtomicWriteOPn[64]; 
    rg_ttype_resp <= (rg_ttype == 4'b0101) ? 4'b0000 : 
		     (rg_ttype == 4'b1100) ? 4'b1000 : 
		     (rg_ttype == 4'b1101) ? 4'b1000 : 4'b0000;
    rg_AtomicState <= State_Update;
    $display ("\n Atomic Write Operation is completed \n Next State : Update");
endrule 

/*
-- Finite State Machine 
-- 4. State Update 
-- During READ, Data is not updated and for atomic operation, data is updated in the memory 
-- During Write, Data is updated in the memory 
-- Once the atomic operation completed, State machine changes to dafault IDLE state and Busy signal is set to False.
*/
rule rl_Update_Memory (rg_AtomicState == State_Update);
    if ((rg_ftype == 'd2) && (rg_ttype != 'd4)) begin // For NREAD, the modified data should not updated. Only for Atomic operations, memory is updated 
	let lv_AtModifiedData = fromMaybe (0, rg_AtomicModifiedData);
    	memory.upd (rg_Addr[7:3], lv_AtModifiedData);
	$display ("\n Atomic Read : Memory is Updated");
    end
    else if (rg_ftype == 4'd5) begin
	let lv_AtModifiedData = fromMaybe (0, rg_AtomicModifiedData);
    	memory.upd (rg_Addr[7:3], lv_AtModifiedData);
	$display ("\n Atomic Write : Memory is Updated");
    end 
    rg_AtomicState <= State_Idle;
    rg_Busy <= False; 
    $display ("\n Atomic Operations Completed ");
endrule

/*
-- Following rule, it is used to generate the Target Response signals (for Read Operation).
-- During the Atomic Update state, the Target Response signals are generated and sent as output. 
*/
rule rl_Generate_Ftype2Response ((rg_ftype == 'd2) && (rg_AtomicState == State_Update)); 
    if (rg_AtomicModifiedData matches tagged Valid .data) begin  
//   	let lv_AtModifiedData = fromMaybe (0, x);
	wr_tresp_SOF <= True; 
	wr_tresp_EOF <= True; 
	wr_tresp_Vld <= True; 
	wr_tresp_tt <= rg_tt;
	wr_tresp_prio <= rg_prio + 1; // Always Prio field for response is incremented by 1. 
	wr_tresp_Data <= ((rg_AtomicOPStatus == 0) ? ((rg_ttype_resp == 4'b1000) ? data : 0) : 0);
	wr_tresp_ftype <= 'd13; 
	wr_tresp_ttype <= rg_ttype_resp;
	wr_tresp_status <= (rg_AtomicOPStatus == 1) ? 4'b0111 : 4'b0000; 
	wr_tresp_TID <= rg_TID; 
	wr_tresp_srcid <= rg_destid;
	wr_tresp_destid <= rg_srcid; 
    end 
endrule 

/*
-- Following rule, Used to generate Target Response signals (for Write operation)
-- for Ttype == 4 (NWRITE), the response is not generated  
*/
rule rl_Generate_Ftype5Response ((rg_ftype == 'd5) && (rg_AtomicState == State_Update) && (rg_ttype != 'd4));
    if (rg_AtomicModifiedData matches tagged Valid .data) begin
	wr_tresp_SOF <= True; 
	wr_tresp_EOF <= True; 
	wr_tresp_Vld <= True;
 	wr_tresp_tt <= rg_tt;
	wr_tresp_prio <= rg_prio + 1; // Always Prio field for response is incremented by 1. 
	wr_tresp_Data <= ((rg_AtomicOPStatus == 0) ? ((rg_ttype_resp == 4'b1000) ? data : 0) : 0);
	wr_tresp_ftype <= 'd13; 
	wr_tresp_ttype <= rg_ttype_resp;
	wr_tresp_status <= (rg_AtomicOPStatus == 1) ? 4'b0111 : 4'b0000; 
	wr_tresp_TID <= rg_TID; 
	wr_tresp_srcid <= rg_destid;
	wr_tresp_destid <= rg_srcid; 
    end 
endrule 

// Input and Output Methods Definitions 
// -- Input Methods
 // -- Control Signal Interface
 method Action _inputs_treq_sof (Bool value);
	wr_treq_sof <= value; 
 endmethod 
 method Action _inputs_treq_eof (Bool value);
	wr_treq_eof <= value;
 endmethod 
 method Action _inputs_treq_vld (Bool value);
	wr_treq_vld <= value; 
 endmethod 
// method Action _treq_rdy_n (Bool value); // Output

 //-- Data Signal Interface
 method Action _inputs_treq_tt (TT value);
	wr_treq_tt <= value; 
 endmethod 
 method Action _inputs_treq_data (Data value);
	wr_treq_data <= value; 
 endmethod 
 method Action _inputs_treq_crf (Bool value);
	wr_treq_crf <= value; 
 endmethod 
 method Action _inputs_treq_prio (Prio value);
	wr_treq_prio <= value; 
 endmethod 
 method Action _inputs_treq_ftype (Type value);
	wr_treq_ftype <= value; 
 endmethod 
 method Action _inputs_treq_dest_id (DestId value);
	wr_treq_destid <= value; 
 endmethod 
 method Action _inputs_treq_source_id (SourceId value);
	wr_treq_srcid <= value; 
 endmethod 
 method Action _inputs_treq_tid (TranId value);
	wr_treq_tid <= value; 
 endmethod 
 method Action _inputs_treq_ttype (Type value);
	wr_treq_ttype <= value; 
 endmethod 
 method Action _inputs_treq_addr (Addr value);
	wr_treq_addr <= value; 
 endmethod 
 method Action _inputs_treq_byte_count (ByteCount value);
	wr_treq_byte_count <= value; 
 endmethod 
 method Action _inputs_treq_byte_en (ByteEn value);
	wr_treq_byte_en <= value; 
 endmethod  

// Output Methods 
 // -- Control Signal Interface 
 method Bool outputs_tresp_sof_ ();
	return wr_tresp_SOF; 
 endmethod 
 method Bool outputs_tresp_eof_ (); 
	return wr_tresp_EOF;
 endmethod 
 method Bool outputs_tresp_vld_ ();
	return wr_tresp_Vld; 
 endmethod 
// method Action tresp_rdy_n_ (Bool value);

 // -- Data Signal Interface
 method TT outputs_tresp_tt_ ();
	return wr_tresp_tt; 
 endmethod 
 method Data outputs_tresp_data_ ();
	return wr_tresp_Data; 
 endmethod 
 method Bool outputs_tresp_crf_ ();
	return False; 
 endmethod
 method Prio outputs_tresp_prio_ ();
	return wr_tresp_prio; 
 endmethod 
 method Type outputs_tresp_ftype_ ();
	return wr_tresp_ftype; 
 endmethod 
 method SourceId outputs_tresp_src_id_ ();
	return wr_tresp_srcid; 
 endmethod 
 method DestId outputs_tresp_dest_id_ ();
	return wr_tresp_destid; 
 endmethod 
 method Status outputs_tresp_status_ ();
	return wr_tresp_status; 
 endmethod 
 method TranId outputs_tresp_tid_ ();
	return wr_tresp_TID;
 endmethod 
 method Type outputs_tresp_ttype_ ();
	return wr_tresp_ttype; 
 endmethod 
endmodule : mkRapidIO_AtomicRdWrOp

endpackage : RapidIO_AtomicRdWrOp
