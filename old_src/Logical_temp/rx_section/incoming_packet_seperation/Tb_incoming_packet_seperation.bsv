package Tb_incoming_packet_seperation;

import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_InComingPkt_Separation ::*;

module mkTb_incoming_packet_seperation(Empty);

	Ifc_RapidIO_InComingPkt_Separation  incoming_packet_seperation <- mkRapidIO_InComingPkt_Separation;

Reg#(Bit#(4)) reg_ref_clk <- mkReg (0);

	rule rl_ref_clk_disp;
		reg_ref_clk <= reg_ref_clk + 1;
		$display (" \n----------------- CLOCK == %d ----------------------", reg_ref_clk);
		if (reg_ref_clk == 8)
		$finish (0);
	endrule


/*
rule r1(reg_ref_clk==0);
	incoming_packet_seperation._inputs_SOF(True);
	incoming_packet_seperation._inputs_EOF(False);
	incoming_packet_seperation._inputs_VLD(True);
	incoming_packet_seperation._inputs_DataPkt(128'h004232004ff200000000000c00000000);
endrule
*/
rule r1(reg_ref_clk==0);
	incoming_packet_seperation._inputs_SOF(True);
	incoming_packet_seperation._inputs_EOF(False);
	incoming_packet_seperation._inputs_VLD(True);
	incoming_packet_seperation._inputs_DataPkt(128'h005683990000000000000008ffffffff);
endrule

rule r12(reg_ref_clk==1);
	incoming_packet_seperation._inputs_SOF(False);
	incoming_packet_seperation._inputs_EOF(False);
	incoming_packet_seperation._inputs_VLD(True);
	incoming_packet_seperation._inputs_DataPkt(128'h11111111111111111111111111111111);
endrule

rule r13(reg_ref_clk==2);
	incoming_packet_seperation._inputs_SOF(False);
	incoming_packet_seperation._inputs_EOF(False);
	incoming_packet_seperation._inputs_VLD(True);
	incoming_packet_seperation._inputs_DataPkt(128'h22222222222222222222222222222222);
endrule

rule r14(reg_ref_clk==3);
	incoming_packet_seperation._inputs_SOF(False);
	incoming_packet_seperation._inputs_EOF(False);
	incoming_packet_seperation._inputs_VLD(True);
	incoming_packet_seperation._inputs_DataPkt(128'h33333333333333333333333333333333);
endrule

rule r15(reg_ref_clk==4);
	incoming_packet_seperation._inputs_SOF(False);
	incoming_packet_seperation._inputs_EOF(True);
	incoming_packet_seperation._inputs_VLD(True);
	incoming_packet_seperation._inputs_DataPkt(128'h44444444444444444444444400000000);
endrule

rule r2;
	$display("output header packet == %h",incoming_packet_seperation.outputs_HeaderPkt_ ());
	$display("output data packet == %h",incoming_packet_seperation.outputs_DataPkt_ ());
	$display("output packet count == %h",incoming_packet_seperation.outputs_PktCount_ ());
	$display("output last packet == %h",incoming_packet_seperation.outputs_LastPkt_ ());
	$display("output maximum packet count == %h",incoming_packet_seperation.outputs_MaxPktCount_ ());
endrule





endmodule
endpackage
