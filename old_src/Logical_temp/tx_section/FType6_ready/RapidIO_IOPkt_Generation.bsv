/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Generation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. To generate the Ftype Packets. 
-- 2. It invokes TxFtypeFunctions Package to generate ftype Header and Data packets
-- 3. The packet is generated with the latency of 1 cycle. 
-- 4. Ftype Packet Header information is stored until the transaction is completed. 
-- 5. Used temporary registers to store the data for packet generation
-- 6. The Packet is generated using the Ftype field and SOF determines the Header packet.
-- 7. Output SOF, EOF, Vld are determined.  
-- 8. The Packet information is shown below. 
-- 9. Bus Interface Ready-Valid operation is implemented. 
--
-- To do's
-- 1. GSM Implementation
-- 2. Flow Control 
-- 3. Data Streaming
-- 4. Message Passing
--
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------


	-- Packet Information -- 

1. Request Class  (Atomic or NREAD Request)
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Read Size[3:0], Source TranID[7:0], Address[28:0], WdPtr, xamsbs[1:0], Resv[55:0]}

2. Write Request  (Atomic Write or NWRITE Request)
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Write Size[3:0], Source TranID[7:0], Address[28:0], WdPtr, xamsbs[1:0], Data[63:8]}
	Data_1 -> { Data[7:0], Resv[55:0]}

3. Stream Write Class 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Address[28:0], Resv, Xamsbs[1;0], Data1, 8'h00 }
	Data_2 -> { Data2, 64'h0 }
	Data_3 -> { Data3, 64'h0 } 	// For 3 Stream Data, But the Data Stream can increase 

4. Maintenance Read Request 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Read Size[3:0], Target TranID[7:0], Hop_Count[7:0], Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Write Size[3:0], Target TranID[7:0], Hop_Count[7:0], Offset[20:0], WdPtr, Resv, Data[63:8] }
	Data_1 -> { Data[7:0] }

6. Maintenance Read Response 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0], Hop_Count[7:0], Resv[15:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

7. Maintenance Write Response 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0], Hop_Count[7:0], Resv[15:0] }

8. Response With Data
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0], Data[63:0] }

9. Response Without Data
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0] }
*/
package RapidIO_IOPkt_Generation;

import FIFO ::*;
import FIFOF ::*;
import SpecialFIFOs ::*;
import DefaultValue ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_TxFTypeFunctionsDev8 ::*;
import RapidIO_TxFTypeFunctionsDev16 ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_IOPkt_Concatenation ::*;
`include "RapidIO.defines"

interface Ifc_RapidIO_IOPkt_Generation;
 method Action _inputs_Ftype6IOStreamClass (FType6_StreamWrClass pkt); 
 method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt ireqpkt);
 method Action _inputs_InitReqDataCount (InitReqDataInput value);
 method Bool pkgen_sof_n_ ();
 method Bool pkgen_eof_n_ (); 
 method Bool pkgen_vld_n_ (); 
 method Bool pkgen_dsc_n_ ();
 method Action pkgen_rdy_n (Bool value);
 method DataPkt pkgen_data_ ();
 method Bit#(4) pkgen_tx_rem_ ();
 method Bool pkgen_crf_ ();
 method Bool outputs_RxRdy_From_Dest_();
endinterface : Ifc_RapidIO_IOPkt_Generation

typedef struct {
	Bool sof; 
	Bool eof; 
	Bool vld;
	DataPkt data;
	Bit#(4) txrem;
	Bool crf; 
} Transmit_Pkt deriving (Bits, Eq);

instance DefaultValue#(Transmit_Pkt);
	defaultValue = Transmit_Pkt {sof: False, eof: False, vld: False, data: 0, txrem: 0, crf: False};
endinstance 

(* synthesize *)

module mkRapidIO_IOPkt_Generation (Ifc_RapidIO_IOPkt_Generation);
	Wire#(FType6_StreamWrClass) wr_Ftype6WrStreamInput <- mkDWire (defaultValue);

Wire#(FType6_StreamWrClass) rg_FType62Delay <- mkReg (defaultValue);

	Wire#(InitiatorReqIfcPkt) wr_InitReqInput <- mkDWire (defaultValue);
	Wire#(InitReqDataInput) wr_InitReqDataCount <- mkDWire (defaultValue);
	Reg#(InitReqDataInput) rg_InitReqDataCount <- mkReg (defaultValue);
	Wire#(Bool) wr_RxReady_In <- mkDWire (False);
	Wire#(Bit#(`RIO_DATA_16)) wr_x   <- mkDWire (0);
	Wire#(Bit#(`RIO_DATA_48)) wr_y   <- mkDWire (0);
	Wire#(Bit#(`RIO_DATA_32)) wr_z   <- mkDWire (0);
	Wire#(Bit#(`RIO_DATA_64)) wr_d_x   <- mkDWire (0);
	Wire#(Bit#(`RIO_DATA_80)) wr_r_x   <- mkDWire (0);

Reg#(Bool) pkgen_SOF_n <- mkReg (False);
Reg#(Bool) pkgen_EOF_n <- mkReg (False);
Reg#(Bool) pkgen_DSC_n <- mkReg (False);
Reg#(Bool) pkgen_VLD_n <- mkReg (False); 
Reg#(Bit#(3)) rg_TxRem <- mkReg (0);

Reg#(Data) rg_InitReqInputData <- mkReg (0);

Reg#(FType6_StreamWrClass) rg_Ftype6WrStreamInputDelay <- mkReg (defaultValue);

Reg#(InitiatorReqIfcPkt) rg_InitReqInput1Delay <- mkReg (defaultValue); 
Reg#(InitiatorReqIfcPkt) rg_InitReqInput2Delay <- mkReg (defaultValue); 

Wire#(DestId) rg_DestId <- mkDWire (0);
Wire#(SourceId) rg_SourceId <- mkDWire (0);

Reg#(DataPkt) rg_OutputDataPkt <- mkReg (0); 
Wire#(DataPkt) wr_OutputFtype6DataPkt <- mkDWire (0);
Wire#(Bool) wr_OutputFtype6Vld <- mkDWire (False);
Wire#(Bool) wr_SOF_Ftype6Vld <- mkDWire (False);
Wire#(Bool) wr_EOF_Ftype6Vld <- mkDWire (False);
Wire#(Bit#(4)) wr_TxRem_Ftype6 <- mkDWire (0);

Reg#(Data) rg_datanew <- mkReg (0);
Reg#(FType6_StreamWrClass) rg_new <- mkReg (defaultValue);


Reg#(ByteCount) rg_InitReqByteCount <- mkReg (9'h1ff); 
Reg#(ByteCount) rg_CurrentInitReqByteCount <- mkReg (0); 
Reg#(ByteCount) rg_CurrentTgtRespByteCount <- mkReg (0); 
Reg#(ByteCount) rg_CurrentMaintainRespByteCount <- mkReg (0);
Reg#(Bool) rg_Ftype6HeaderNotComplete <- mkReg (False); 
Reg#(Bool) rg_Ftype6HeaderNotComplete1 <- mkReg (False);
Reg#(Data) rg_Ftype6DataValidDelayed <- mkReg (0);
Wire#(Transmit_Pkt) wr_PktGenTranmitIfcFirst <- mkDWire (defaultValue); 
Reg#(FType6_StreamWrClass) rg_Ftype6InputValid <- mkReg (defaultValue);
FIFOF#(Transmit_Pkt) ff_TransmitPktIfcFIFO <- mkSizedBypassFIFOF (8); 

rule rl_InitReqInterfaceInput2Delay;
    rg_InitReqInput1Delay <= wr_InitReqInput;	
    rg_InitReqInput2Delay <= rg_InitReqInput1Delay;	
	rg_FType62Delay <= wr_Ftype6WrStreamInput;
endrule


Reg#(Data) rg_Ftype6DataValid <- mkReg (0);
Reg#(Data) rg_Ftype6DataInput <- mkReg (0);
Reg#(Bool) rg_Ftype6LastData2Delay <- mkReg (False);

/*
rule rl_Ftype6InputDelay_1Clk; 
$display("wire****************** == %b",wr_Ftype6WrStreamInput);
$display("register @@@@@@@@@@@@@@@@@ == %h",rg_new);
$display("delayed register @@@@@@@@@@@@@@@@@ == %h",rg_Ftype6WrStreamInputDelay);
$display("FType 6 data input %h",rg_datanew);
$display("original FType 6 data input %h",rg_Ftype6DataInput);
  


if (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True && wr_InitReqInput.ireqcntrl.ireq_sof == True ) begin
$display("1111111111111111111111111111111");
//rg_Ftype6WrStreamInputDelay <= rg_Ftype6WrStreamInputDelay;
//rg_Ftype6DataInput1 <= wr_InitReqInput.ireqdata.ireq_data;
rg_new <= wr_Ftype6WrStreamInput;
end


else if (rg_InitReqInput1Delay.ireqcntrl.ireq_sof == True && rg_InitReqInput2Delay.ireqcntrl.ireq_eof == True) begin
$display("22222222222222222222222222222222");
//rg_Ftype6WrStreamInputDelay <= wr_Ftype6WrStreamInput;
//rg_Ftype6DataInput <= rg_Ftype6DataInput1;
rg_Ftype6WrStreamInputDelay <= rg_new;
end

else if (wr_InitReqInput.ireqcntrl.ireq_sof == True ) begin
$display("3333333333333333333333333333333");
rg_Ftype6WrStreamInputDelay <= wr_Ftype6WrStreamInput;
end


if (rg_InitReqInput1Delay.ireqcntrl.ireq_sof == True && rg_InitReqInput2Delay.ireqcntrl.ireq_eof == True) begin 
$display("555555555555555555555555555555555555555555555555");
//rg_Ftype6DataInput <= rg_Ftype6DataInput1;
rg_Ftype6DataInput <= rg_datanew ;
end

else if (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True && wr_InitReqInput.ireqcntrl.ireq_sof == True) begin
$display("444444444444444444444444444444");
//rg_Ftype6DataInput1 <= wr_InitReqInput.ireqdata.ireq_data;
rg_datanew <= wr_InitReqInput.ireqdata.ireq_data;
end

 
   else if (wr_InitReqInput.ireqcntrl.ireq_vld == True) begin
$display("666666666666666666666666666666666666666666");
 	rg_Ftype6DataInput <= wr_InitReqInput.ireqdata.ireq_data;	
end
 
else 
begin
$display("777777777777777777777777777777777777777");
//rg_Ftype6WrStreamInputDelay <= rg_Ftype6WrStreamInputDelay;
rg_Ftype6DataInput <= 0;
end 


    else 
	rg_Ftype6DataInput <= 0; 

endrule
*/

rule rl_Ftype6InputDelay_1Clk; // Ftype6 Input Delayed 1 Clock until EOF enabled (But EOF is delayed 1 clock cycle)
//$display("register @@@@@@@@@@@@@@@@@ == %b",rg_Ftype6WrStreamInputDelay);
//$display("wire****************** == %b",wr_Ftype6WrStreamInput);

    rg_Ftype6LastData2Delay <= rg_InitReqDataCount.lastdata;

    if (wr_InitReqInput.ireqcntrl.ireq_sof == True) begin
	rg_Ftype6WrStreamInputDelay <= wr_Ftype6WrStreamInput;
	end

    if (wr_InitReqInput.ireqcntrl.ireq_vld == True)
 	rg_Ftype6DataInput <= wr_InitReqInput.ireqdata.ireq_data;	
    else 
	rg_Ftype6DataInput <= 0;
endrule

rule rl_Ftype6InputValid; 
    rg_Ftype6InputValid <= rg_Ftype6WrStreamInputDelay;
    rg_Ftype6DataValid <= rg_Ftype6DataInput;
endrule

Reg#(Bool) rg_Ftype6LastData3Delay <- mkReg (False);
Reg#(Data) rg_Ftype6DataValid1Delayed <- mkReg (0);
Reg#(Bool) rg_Ftype6LastData4Delay <- mkReg (False);

rule rl_Ftype6Generation (rg_Ftype6WrStreamInputDelay.ftype == `RIO_FTYPE6_STREAM_WR); 
case (rg_Ftype6WrStreamInputDelay.tt ) matches
'b00 : begin 

 Data lv_Ftype6DataValid = rg_Ftype6DataInput;
    rg_Ftype6DataValidDelayed <= rg_Ftype6DataInput;
    rg_Ftype6LastData3Delay <= rg_InitReqDataCount.lastdata; 

	if (rg_InitReqInput1Delay.ireqcntrl.ireq_sof == True && rg_InitReqInput1Delay.ireqcntrl.ireq_vld == True && rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True)
	begin
	wr_OutputFtype6DataPkt <= fn_Dev8Ftype6SWriteHdrPktCreation (rg_Ftype6WrStreamInputDelay, rg_InitReqInput1Delay.ireqdata.ireq_prio, 2'b00, rg_InitReqInput1Delay.ireqdata.ireq_destid[31:24], 8'h00, 1'b0,  lv_Ftype6DataValid);
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= True;
        rg_Ftype6HeaderNotComplete <= True;
end
	else if (rg_Ftype6HeaderNotComplete == True) 
	begin
	wr_OutputFtype6DataPkt <= fn_Dev8Ftype6SWriteDataPktCreation (rg_Ftype6WrStreamInputDelay, rg_Ftype6DataValid[(`RIO_DATA - 49):0], wr_y);
rg_Ftype6HeaderNotComplete <= False;
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= False;
        wr_TxRem_Ftype6 <= (rg_InitReqInput2Delay.ireqcntrl.ireq_eof == True) ? 4'b1000 : 0;
     	wr_EOF_Ftype6Vld <= (rg_InitReqInput2Delay.ireqcntrl.ireq_eof == True) ? True : False;
end

// Data available for more than one cycle
	else if (rg_InitReqInput1Delay.ireqcntrl.ireq_sof == True && rg_InitReqInput1Delay.ireqcntrl.ireq_vld == True && rg_InitReqInput1Delay.ireqcntrl.ireq_eof == False)
	begin
	wr_OutputFtype6DataPkt <= fn_Dev8Ftype6SWriteHdrPktCreation (rg_Ftype6WrStreamInputDelay, rg_InitReqInput1Delay.ireqdata.ireq_prio, 2'b00, rg_InitReqInput1Delay.ireqdata.ireq_destid[31:24], 8'h00, 1'b0,  lv_Ftype6DataValid);
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= True;
	wr_EOF_Ftype6Vld <= False;
        rg_Ftype6HeaderNotComplete1 <= True;
	end
	else if (rg_Ftype6HeaderNotComplete1 == True )
	begin
wr_OutputFtype6DataPkt <= fn_Dev8Ftype6SWriteDataPktCreation (rg_Ftype6WrStreamInputDelay, rg_Ftype6DataValidDelayed[(`RIO_DATA - 49):0], lv_Ftype6DataValid[(`RIO_DATA - 1):(`RIO_DATA - 48)]);
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= False;
     	wr_EOF_Ftype6Vld <= False;
	if (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True) begin
	rg_Ftype6HeaderNotComplete1 <= False;
	end
	end
else
begin
	wr_OutputFtype6DataPkt <= fn_Dev8Ftype6SWriteDataPktCreation (rg_Ftype6WrStreamInputDelay, rg_Ftype6DataValidDelayed[(`RIO_DATA - 49):0], wr_y);
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= False;
        wr_TxRem_Ftype6 <= (rg_InitReqInput2Delay.ireqcntrl.ireq_eof == True) ? 4'b1000 : 0;
     	wr_EOF_Ftype6Vld <= (rg_InitReqInput2Delay.ireqcntrl.ireq_eof == True) ? True : False;
	end
end
'b01 : begin
	if (rg_Ftype6WrStreamInputDelay.ftype == `RIO_FTYPE6_STREAM_WR) begin 
	Data lv_Ftype6DataValid = rg_Ftype6DataInput;
	rg_Ftype6DataValidDelayed <= rg_Ftype6DataInput;
	rg_Ftype6DataValid1Delayed <= rg_Ftype6DataValidDelayed; 
	rg_Ftype6LastData3Delay <= rg_InitReqDataCount.lastdata;
	rg_Ftype6LastData4Delay <= rg_Ftype6LastData3Delay;

	if (rg_InitReqInput1Delay.ireqcntrl.ireq_sof == True && rg_InitReqInput1Delay.ireqcntrl.ireq_vld == True && rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True)
	begin
	wr_OutputFtype6DataPkt <= fn_Dev16Ftype6SWriteHdrPktCreation (rg_Ftype6WrStreamInputDelay, rg_InitReqInput1Delay.ireqdata.ireq_prio, rg_InitReqInput1Delay.ireqdata.ireq_tt, rg_InitReqInput1Delay.ireqdata.ireq_destid[31:16], 16'h00, 8'h00, lv_Ftype6DataValid);
	rg_Ftype6HeaderNotComplete <= True; 
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= True;
	end
	else if (rg_Ftype6HeaderNotComplete == True)
	begin 
	wr_OutputFtype6DataPkt <= fn_Dev16Ftype6SWriteDataPktCreation (rg_Ftype6WrStreamInputDelay, rg_Ftype6DataValidDelayed[(`RIO_DATA - 33):0], wr_z);
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= False;
        wr_TxRem_Ftype6 <= (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True) ? 4'b1000 : 0;
     	wr_EOF_Ftype6Vld <= (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True) ? True : False;
	end
	else if (rg_InitReqInput1Delay.ireqcntrl.ireq_sof == True && rg_InitReqInput1Delay.ireqcntrl.ireq_vld == True && rg_InitReqInput1Delay.ireqcntrl.ireq_eof == False)
	begin
	wr_OutputFtype6DataPkt <= fn_Dev16Ftype6SWriteHdrPktCreation (rg_Ftype6WrStreamInputDelay, rg_InitReqInput1Delay.ireqdata.ireq_prio, rg_InitReqInput1Delay.ireqdata.ireq_tt, rg_InitReqInput1Delay.ireqdata.ireq_destid[31:16], 16'h00, 8'h00, lv_Ftype6DataValid);
	rg_Ftype6HeaderNotComplete1 <= True; 
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= True;
     	wr_EOF_Ftype6Vld <= False;
	end
	else if (rg_Ftype6HeaderNotComplete1 == True)
	begin
        wr_OutputFtype6DataPkt <= fn_Dev16Ftype6SWriteDataPktCreation (rg_Ftype6WrStreamInputDelay, rg_Ftype6DataValidDelayed[(`RIO_DATA - 33) :0], lv_Ftype6DataValid[(`RIO_DATA - 1):(`RIO_DATA - 32)]);
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= False;
     	wr_EOF_Ftype6Vld <= False;
	if (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True) begin
        rg_Ftype6HeaderNotComplete1 <= False;
	end
    	end 
	else if (rg_InitReqInput2Delay.ireqcntrl.ireq_eof == True) 
begin
	wr_OutputFtype6DataPkt <= fn_Dev16Ftype6SWriteDataPktCreation (rg_Ftype6WrStreamInputDelay, rg_Ftype6DataValid1Delayed[(`RIO_DATA - 33):0], wr_z);
	wr_OutputFtype6Vld <= True;
	wr_SOF_Ftype6Vld <= False;
        wr_TxRem_Ftype6 <= (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True) ? 4'b1000 : 0;
     	wr_EOF_Ftype6Vld <= (rg_InitReqInput1Delay.ireqcntrl.ireq_eof == True) ? True : False;
	end
	end
	end
endcase
endrule

rule rl_DataPkt_Valid;
    DataPkt lv_OutputDataPkt = wr_OutputFtype6DataPkt;
    Bool lv_pkgen_vld = wr_OutputFtype6Vld ;
    Bool lv_pkgen_sof = wr_SOF_Ftype6Vld;
    Bool lv_pkgen_eof =  wr_EOF_Ftype6Vld ;
    Bit#(4) lv_txrem =  wr_TxRem_Ftype6 ;

    if (lv_pkgen_vld == True) begin
        Transmit_Pkt lv_Transmit_Pkt = Transmit_Pkt {sof: lv_pkgen_sof, eof: lv_pkgen_eof, vld: lv_pkgen_vld, data: lv_OutputDataPkt, txrem: lv_txrem, crf: False};
	ff_TransmitPktIfcFIFO.enq(lv_Transmit_Pkt);
    end 
endrule 

rule rl_CalculateDSC (rg_InitReqInput2Delay.ireqcntrl.ireq_dsc == True );
	pkgen_DSC_n <= True;
	ff_TransmitPktIfcFIFO.clear();
endrule

rule rl_FIFOF_Dequeue ((ff_TransmitPktIfcFIFO.first.vld == True) && (wr_RxReady_In == False)); 
	ff_TransmitPktIfcFIFO.deq();
endrule 

rule rl_FIFOF_First;
	wr_PktGenTranmitIfcFirst <= ff_TransmitPktIfcFIFO.first();
endrule 

rule disp;
    $display ("\n\tThe IOGen Transmit Packet FIFO Output == %b", wr_PktGenTranmitIfcFirst);
endrule

 method Action _inputs_Ftype6IOStreamClass (FType6_StreamWrClass pkt);
	wr_Ftype6WrStreamInput <= pkt;
 endmethod

 method Action _inputs_InitReqIfcPkt (InitiatorReqIfcPkt ireqpkt);
	wr_InitReqInput <= ireqpkt;
	rg_InitReqInputData <= ireqpkt.ireqdata.ireq_data;
 endmethod

 method Action _inputs_InitReqDataCount (InitReqDataInput value);
	wr_InitReqDataCount <= value; 
	rg_InitReqDataCount <= value;
 endmethod

 method Bool pkgen_sof_n_ ();
	return !(wr_PktGenTranmitIfcFirst.sof); 
 endmethod

 method Bool pkgen_eof_n_ ();
	return !(wr_PktGenTranmitIfcFirst.eof);
 endmethod	

 method Bool pkgen_vld_n_ ();
	return !(wr_PktGenTranmitIfcFirst.vld);
 endmethod

 method Bool pkgen_dsc_n_ ();
	return (!pkgen_DSC_n);
 endmethod

 method Action pkgen_rdy_n (Bool value); 
	wr_RxReady_In <= value;
 endmethod

 method DataPkt pkgen_data_();
	return wr_PktGenTranmitIfcFirst.data; 
 endmethod

 method Bit#(4) pkgen_tx_rem_ ();
	return wr_PktGenTranmitIfcFirst.txrem; 
 endmethod

 method Bool pkgen_crf_ ();
	return False;
 endmethod

 method Bool outputs_RxRdy_From_Dest_();
	return wr_RxReady_In; 
 endmethod 

endmodule : mkRapidIO_IOPkt_Generation
endpackage : RapidIO_IOPkt_Generation

